import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class ServerAphorism implements Runnable {
   File aphorisms=new File("aforizms.txt");
   static private Socket connection;
   static private ObjectOutputStream output;
   static private ObjectInputStream input;
   static private ServerSocket server;
   final int max = 10; 
   int rnd=0;
   int countString=0;
   
   @Override
   public void run() {
      try {
         server=new ServerSocket(2505,100);
         while(true){
            connection= server.accept();
            output=new ObjectOutputStream(connection.getOutputStream());
            input=new ObjectInputStream(connection.getInputStream());
            input.readObject();
            BufferedReader breader=new BufferedReader(
                  new InputStreamReader(
                  new FileInputStream (aphorisms),"UTF8"));
            String line;
            rnd = rnd(max);
            while((line=breader.readLine())!=null){
               countString++;
               if(countString==rnd){
                  output.writeObject(line);
               }
            }
            breader.close();
            server.close();
            connection.close();
         }
      }
      catch (HeadlessException e) {
         e.printStackTrace();
      } 
      catch (ClassNotFoundException e) {
         e.printStackTrace();
      }
      catch (UnknownHostException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   public static int rnd(int max)
   {
      return (int) (Math.random() * ++max);
   }
}
 class Me{
   
}