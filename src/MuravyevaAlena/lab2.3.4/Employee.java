package staffDemo;

class Employee implements WorkTime{
   StaffDemo astaffDemo=new StaffDemo();
   public int id;
   public String name;
   public int worktime;
   public double payment;
   public int base;
   public String position;
   
   public Employee(int aid,String aname,String aposition,int aworktime,int abase){
      id=aid;
      name=aname;
      position=aposition;
      worktime=aworktime;
      base=abase;
   }
   public Employee(int aid,String aname,String aposition,int aworktime){
      id=aid;
      name=aname;
      position=aposition;
      worktime=aworktime;
   }
   public Employee(double apayment){
      payment=apayment;
   }
   public int getWorktime() {
      return worktime;
   }
   public void printEmployee(){
      System.out.printf("%d %s %s %d %10.2f\n",id,name,position,worktime,payment);
   }
   
   public void calculationPayment(){
      payment= payment();
   }
   public double payment() {
      return base*(astaffDemo.getNormalTime()/worktime);
   }
}