package automata;

enum menuIndex{TEA,ESPRESSO, AMERICANA,LATTE,MOCHA,HOT_CHACOLATE};

public class AutomataDemo {

   public static void main(String[] args) {
      Automata av=new Automata();
      av.on();
      av.coin(25);
      boolean result = av.choice(menuIndex.TEA);
      
      if(result == true)
      {
         av.off();
      }
      else
      {
         av.coin(10);
         av.choice(menuIndex.TEA);
         av.finish();
      }
   }
}
class Automata{
   int cash=0;
   String[]menu={"tea","espresso","americana","latte","mocha","hot chocolate"};
   int[] price={15,25,30,35,25,30};
   menuIndex userChoise;
   enum States{ON,OFF, WAIT,ACCEPT,CHEK,COOK};
   States state = States.OFF;
   
   public Automata(){
   }
   public void on(){
      state=States.WAIT;
      printMenu();
   }
   public void off(){
      state=States.OFF;
   }
   public void coin(int coin){
      if(state==States.WAIT||state==States.ACCEPT){
         state=States.ACCEPT;
         cash=+coin;
      }
   }
   public void printMenu(){
      for(String n:menu){
         System.out.println(n);
      }
   }
   public States printState(){
      return state;
   }
   public boolean choice(menuIndex  index){
      userChoise=index;
      return check();
   }
   public boolean check(){
      boolean retVal = false;
      if(cash<price[userChoise.ordinal()]){
         cancel();
      }
      else if(cash>=price[userChoise.ordinal()]){
         state=States.COOK;
         cook();
         retVal = true;
      }
      
      return retVal;
   }
   public void cancel(){
      state=States.WAIT;
   }
   public void cook(){
      finish();
   }
   public int returnMoney(){
      int delivery=0;
      if(state!=States.COOK){
         delivery=cash;
         cash=0;
         state=States.WAIT;
      }
      return delivery;
   }
   public void finish(){
      state=States.WAIT;
      System.out.printf("Take %s",userChoise);
   }
}