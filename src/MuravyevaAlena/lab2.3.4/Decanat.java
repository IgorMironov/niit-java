package decanat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Decanat{
   static final int MAX_GROUPS_IN_DECANAT = 3;
   static final int MAX_STUDENTS_IN_DECANAT = 33;
   static final int MAX_LETTER_ID_GROUP = 5;
   static final int VARIABLE= 1;
   static final int MAX_LETER_NAME = 50;
   static final int FROM=0;
   static final int TO=10;
   static final int MARKS_PERFORMANSE=3;
   Student [] students=new Student [MAX_STUDENTS_IN_DECANAT];
   Group [] groups= new Group[MAX_GROUPS_IN_DECANAT];
   char [] nm=new char[MAX_LETTER_ID_GROUP];
   char [] name=new char [MAX_LETER_NAME];
   char [] number=new char [MAX_LETTER_ID_GROUP];
   private int spase=0;
   
   public void openFile(){
      try
      {
         File file1 = new File("date/groups.txt");
         File file2 = new File("date/student1.txt");
         
         FileReader fr = new FileReader(file1);
         BufferedReader reader = new BufferedReader(fr);
         FileReader fr2 = new FileReader(file2);
         BufferedReader reader2 = new BufferedReader(fr2);
         String line;
         int counterGroup=0;
         while ( (line=reader.readLine()) != null) {
            groups[counterGroup] =new Group(line);
            counterGroup++;
         }
         reader.close();
         int studentIndex=0;
         int indexArrNumber=0;
         int indexArrName=0;
         int indexArrNm=0;
         while((line=reader2.readLine())!=null){
            char[] arr= line.toCharArray();
            for(int j=0;j<arr.length;j++){
               if(arr [j]!= ' ' && spase>=1 && spase<=3){
                  name[indexArrName]=arr[j];
                  indexArrName++;
               }
               if(arr [j]!= ' ' && spase==0){
                  number[indexArrNumber]=arr[j];
                  indexArrNumber++;
               }
               if(arr [j]==' ') spase++;
               if(arr [j]==' '&& spase>1 && spase<=3){
                  name[indexArrName]=' ';
                  indexArrName++;
               }
               if(arr [j]!= ' ' && spase==4){
                  nm[indexArrNm]=arr[j];
                  indexArrNm++;
               }
            }
            students[studentIndex]=new Student(number,name,nm);
            enrollmentStudent(nm,studentIndex);
            generating(studentIndex);
            studentIndex++;
            indexArrNumber=0;
            indexArrName=0;
            indexArrNm=0;
            Arrays.fill(name,'\0');
            Arrays.fill(number,'\0');
            Arrays.fill(nm,'\0');
            spase=0;
            
         }
         reader2.close();
      }
      catch(FileNotFoundException e){
         e.printStackTrace();
      }
      catch(IOException ex){
         System.out.println(ex.getMessage());
      }
   }
   public void enrollmentStudent(char [] nm,int studentIndex){
      String strNumGr2=new String(nm);
      for(int i=0;i<=MAX_GROUPS_IN_DECANAT-VARIABLE;i++){
         if(strNumGr2.compareTo(groups[i].getName())==0){
            students[studentIndex].addToGroup(groups[i]);
            groups[i].addStudent(students [studentIndex]);
         }
      }
   }
   public void generating(int studentIndex){
      int [] arrNumber = new int[TO+VARIABLE];
      for(int k=0;k<=TO;k++){
         int randomNumber= FROM+(int) (Math.random()*TO);
         arrNumber[k]=randomNumber;
      }
      students[studentIndex].addToMarks(arrNumber);
      students[studentIndex].calculationAverage();
   }
   public void statisticaGroupsAndStudents(){
      for(int i=0;i<MARKS_PERFORMANSE;i++){
         if(groups[i].getTotalAverage()<MARKS_PERFORMANSE){
            groups[i].performance=Performance.BAD;
         }
         for(int j=0;j<groups[i].getNum();j++){
            if(students[j].getAverage()<MARKS_PERFORMANSE){
               students[i].performance=Performance.BAD;
            }
         }
         groups[i].deletionStudent();
      }
   }
   public void relocationStudents(){
      int to1Gr=groups[FROM].getNum();
      int randomStudent1=0;
      Student tmpStudent;
      
      randomStudent1= FROM+(int) (Math.random()*to1Gr);
      tmpStudent=groups[FROM].getStudentByIndex(randomStudent1);
      groups[FROM].delStudent(randomStudent1);
      groups[VARIABLE].addStudent(tmpStudent);
      tmpStudent.changeGroup(groups[VARIABLE]);
   }
}
