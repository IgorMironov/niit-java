package decanat;

public class Student{
   Group gruppa;
   static final int MAX_LETTER_ID_GROUP = 5;
   static final int MAX_LETER_NAME = 50;
   static final int MAX_MARKS=10;
   public Performance performance=Performance.GOOD;
   private char[] id=new char [MAX_LETTER_ID_GROUP];
   private char [] fio = new char[MAX_LETER_NAME];
   private int[] marks;
   private char[] num=new char[MAX_LETTER_ID_GROUP];
   private int average;
   
   public Student(char[] i,char[] f, char[] nm){
      for(int k=0; k<i.length; k++)
      {
         id[k]=i[k];
      }
      for(int a=0; a<f.length; a++)
      {
         fio[a]=f[a];
      }
      for(int m=0; m<nm.length; m++)
      {
         num[m]=nm[m];
      }
   }
   
   public char[] getFio(){
      char[]student=new char [getCounter()];
      for(int i=0;i<=fio.length;i++){
         if(fio[i]=='\0'){
            return student;
         }
         student[i]=fio[i];
      }
      return student;
   }
   public int getCounter(){
      int counterSymbol=0;
      for(int i=0;i<fio.length;i++){
         if(fio[i]=='\0'){
            return counterSymbol;
         }
         counterSymbol++;
      }
      return counterSymbol;
   }
   public char[] getId(){
      return id;
   }
   public Group getGroup(){
      return gruppa;
   }
   public int getAverage(){
      return average;
   }
   public void addToGroup(Group gr){
      gruppa =gr;
   }
   public void addToMarks(int []arrNumber){
      marks=arrNumber;
   }
   public void calculationAverage(){
      for(int i=0;i<=MAX_MARKS;i++){
         average=average+marks[i];
      }
      average= average/MAX_MARKS;
   }
   public void changeGroup(Group groups){
      gruppa=groups;
   }
   public void printStudent(){
      String name = new String(fio);
      String num = new String(id);
      System.out.println("Name:"+name+"id"+num);
   }
}