package staffDemo;

import java.io.*;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class StaffDemo {
   public int counterEngineer=0;
   public int counterProgrammer = 0;
   public int counterTester = 0;
   public int counterTeamLeader = 0;
   public int counterProjectManager = 0;
   public int counterManager = 0;
   public int counterSeniorManager = 0;
   public int headProjectManager=0;
   public int headTeamLeader=0;
   public int headSeniorManager=0;
   public int normalTime = 175;
   final int MAX_EMPLOYEES=35;
   public final int budgetProject=500000;
   public double coefficientEngineer=0;
   public double coefficientProgrammer = 0;
   public double coefficientTester = 0;
   public double coefficientTeamLeader = 0;
   public double coefficientManager = 0;
   public double coefficientProjectManager = 0;
   public double coefficientSeniorManager = 0;
   public double sizeBonusAllEngineer=0.1;
   public double sizeBonusAllProgrammer=0.07;
   public double sizeBonusAllTester=0.05;
   public double sizeBonusAllTeamLeader=0.15;
   public double sizeBonusAllManager=0.25;
   public double sizeBonusAllProjectManager=0.15;
   public double sizeBonusAllSeniorManager=0.23;
   Employee[] employees = new Employee[MAX_EMPLOYEES];

   public static void main(String[] args) {
      StaffDemo astafDemo = new StaffDemo();
      astafDemo.openFile();
      astafDemo.calculationAllPayment();
   }

public void openFile() {
   try{
      File file = new File("employee/emptxt.txt");
      FileReader fr = new FileReader(file);
      BufferedReader reader = new BufferedReader(fr);
      int idPeople;
      String namePeaple;
      int worktimePeople;
      int basePeople;
      String position;
      String line;
      int currentNum=0;

      while ((line = reader.readLine()) != null) {
         String[] words = line.split("\t");
         idPeople = Integer.parseInt(words[0]);
         namePeaple = words[1];
         position = words[2];
         worktimePeople = Integer.parseInt(words[3]);
         basePeople = Integer.parseInt(words[4]);
         if (position.equals("разнорабочий")) {
            employees[currentNum] = new Personal(idPeople, namePeaple,position, worktimePeople, basePeople);
            currentNum++;
         }
         else if (position.equals("уборщица")) {
            employees[currentNum] = new Cleaner(idPeople, namePeaple,position, worktimePeople, basePeople);
            currentNum++;
         }
         else if (position.equals("водитель")) {
            employees[currentNum] = new Driver(idPeople, namePeaple,position, worktimePeople, basePeople);
            currentNum++;
         }
         else if (position.equals("инженер")) {
            employees[currentNum] = new Engineer(idPeople, namePeaple,position, worktimePeople, basePeople);
            counterEngineer++;
            currentNum++;
         }
         else if (position.equals("инженер-программист")) {
            employees[currentNum] = new Programmer(idPeople, namePeaple,position, worktimePeople, basePeople);
            counterProgrammer++;
            currentNum++;
         }
         else if (position.equals("инженер по тестированию")) {
            employees[currentNum] = new Tester(idPeople, namePeaple,position, worktimePeople, basePeople);
            counterTester++;
            currentNum++;
         }
         else if (position.equals("ведущий программист")) {
            employees[currentNum] = new TeamLeader(idPeople, namePeaple,position, worktimePeople, basePeople);
            counterTeamLeader++;
            currentNum++;
         }
         else if (position.equals("менеджер")) {
            employees[currentNum] = new Manager(idPeople, namePeaple,position, worktimePeople);
            counterManager++;
            currentNum++;
         }
         else if (position.equals("проектный менеджер")) {
            employees[currentNum] = new ProjectManager(idPeople, namePeaple,position, worktimePeople);
            counterProjectManager++;
            currentNum++;
         }
         else if (position.equals("руководитель направления")) {
            employees[currentNum] = new SeniorManager(idPeople, namePeaple,position, worktimePeople);
            counterSeniorManager++;
            currentNum++;
         }
      }
      reader.close();
   } 
   catch (FileNotFoundException e) {
      e.printStackTrace();
   } catch (IOException ex) {
      System.out.println(ex.getMessage());
   }
}
public void calculationAllPayment(){
   coefficientEngineer=sizeBonusAllEngineer/counterEngineer;
   coefficientProgrammer=sizeBonusAllProgrammer/counterProgrammer;
   coefficientTester=sizeBonusAllTester/counterTester;
   coefficientTeamLeader=sizeBonusAllTeamLeader/counterTeamLeader;
   coefficientManager=sizeBonusAllManager/counterManager;
   coefficientProjectManager=sizeBonusAllProjectManager/counterProjectManager;
   coefficientSeniorManager=sizeBonusAllSeniorManager/counterSeniorManager;
   headTeamLeader=counterEngineer+counterProgrammer+counterTester;
   headProjectManager=counterManager;
   headSeniorManager=counterManager+counterProjectManager;
   
   for(Employee emp: employees){
      if(emp instanceof Engineer){
         Engineer pers = (Engineer)emp;
         pers.bonus(budgetProject, coefficientEngineer);
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof Programmer){
         Programmer pers = (Programmer)emp;
         pers.bonus(budgetProject, coefficientProgrammer);
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof Tester){
         Tester pers = (Tester)emp;
         pers.bonus(budgetProject, coefficientTester);
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof TeamLeader){
         TeamLeader pers = (TeamLeader)emp;
         pers.bonus(budgetProject, coefficientTeamLeader);
         pers.paymentForPeople(headTeamLeader);
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof Manager){
         Manager pers = (Manager)emp;
         pers.bonus(budgetProject, coefficientManager);
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof ProjectManager){
         ProjectManager pers = (ProjectManager)emp;
         pers.bonus(budgetProject, coefficientProjectManager);
         pers.paymentForPeople(headProjectManager);
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof SeniorManager){
         SeniorManager pers = (SeniorManager)emp;
         pers.bonus(budgetProject, coefficientSeniorManager);
         pers.paymentForPeople(headSeniorManager);
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof Cleaner){
         Cleaner pers = (Cleaner)emp;
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof Driver){
         Driver pers = (Driver)emp;
         pers.calculationPayment();
         pers.printEmployee();
      }
      else if(emp instanceof Personal){
         Personal pers = (Personal)emp;
         pers.calculationPayment();
         pers.printEmployee();
      }
   }
}
public int getNormalTime(){
   return normalTime;
}
}