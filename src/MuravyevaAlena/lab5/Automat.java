package automatCUIdemo;

public class Automat {
    States state = States.OFF;
    int[] price={15,25,30,35,25,30};
    menuIndex userChoice;
    int cash=0;

    public States getState(){return state;}
    public Automat(){off();}
    public void on(){state=States.WAIT;}
    public void off(){state=States.OFF;}
    public void coin(int coin){
        if(state==States.WAIT||state==States.ACCEPT){
            state=States.ACCEPT;
            cash=+coin;
        }
    }
    public boolean choice(menuIndex  index){
        userChoice =index;
        return check();
    }
    public boolean check(){
        if(cash<price[userChoice.ordinal()]){
            return false;
        }
        else if(cash>=price[userChoice.ordinal()]){
            state=States.COOK;
            return true;
        }
        return true;
    }
    public void cancel(){state=States.WAIT;}
    public void cook(){finish();}
    public void finish(){state=States.WAIT;}
    public int getCash(){return cash;}
}
