package automatCUIdemo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
enum menuIndex{TEA,ESPRESSO, AMERICANA,LATTE,MOCHA, HOT_CHOCOLATE};
enum States{OFF, WAIT,ACCEPT,COOK};

public class AutomatGUIdemo {
    private JPanel panel;
    private JButton onOff;
    public JTextField balance;
    private DrinkButton teaCost;
    private DrinkButton espressoCost;
    private DrinkButton americanaCost;
    private DrinkButton latteCost;
    private DrinkButton mochaCost;
    private DrinkButton hotChocolateCost;
    public JTextField info;
    public JProgressBar progressBar1;
    public int delivery=0;
    public int  cashUser=0;
    boolean result=false;
    String cash;
    int[] price={15,25,30,35,25,30};
    Automat av = new Automat();

    public AutomatGUIdemo() {

        teaCost.setDrinkIndex(menuIndex.TEA);
        espressoCost.setDrinkIndex(menuIndex.ESPRESSO);
        americanaCost.setDrinkIndex(menuIndex.AMERICANA);
        latteCost.setDrinkIndex(menuIndex.LATTE);
        mochaCost.setDrinkIndex(menuIndex.MOCHA);
        hotChocolateCost.setDrinkIndex(menuIndex.HOT_CHOCOLATE);
        info.setEditable(false);

        onOff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (av.getState() ==States.OFF) {
                    av.on();
                    onOff.setBackground(Color.red);
                    info.setText("On");
                    info.update(info.getGraphics());
                    try{
                        Thread.sleep(1000);
                    }
                    catch(InterruptedException t){
                    }
                    info.setText("Insert coins");
                    balance.setEnabled(true);
                }
                else if (av.getState() ==States.WAIT)
                {
                    av.off();
                    info.setText("Off");
                    onOff.setBackground(Color.blue);
                    balance.setEnabled(false);
                }

            }
        });

        balance.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cash= balance.getText().trim();
                try {
                    cashUser = Integer.parseInt(cash);
                    av.coin(cashUser);
                    info.setText("Please select product");
                } catch (NumberFormatException c) {
                    info.setText("Insert coins");
                }
            }
        });

        teaCost.addActionListener(menuListener);
        espressoCost.addActionListener(menuListener);
        americanaCost.addActionListener(menuListener);
        latteCost.addActionListener(menuListener);
        mochaCost.addActionListener(menuListener);
        hotChocolateCost.addActionListener(menuListener);
    }
    public static void main(String[] args) {
        JFrame jframe = new JFrame("Coffee machine");
        jframe.setContentPane(new AutomatGUIdemo().panel);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.pack();
    }

    ActionListener menuListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            DrinkButton menu = (DrinkButton) e.getSource();
            if (av.getState() != States.COOK) {
                result = av.choice(menu.drink);
                if (result == true && av.getState() == States.COOK) {
                    delivery = av.getCash() - price[av.userChoice.ordinal()];
                    info.setText("Is cooking");
                    info.update(info.getGraphics());
                    av.cook();
                    ThreadPB tr = new ThreadPB();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException n) {
                    }
                    tr.start();
                    try {
                        tr.join();
                        info.setText("Drink ready. Take the delivery");
                        info.update(info.getGraphics());
                        balance.setText(Integer.toString(delivery));
                        balance.update(balance.getGraphics());
                        Thread.sleep(5000);
                    } catch (InterruptedException n) {
                    }
                    clean();
                    balance.update(balance.getGraphics());
                    info.setText("Insert coins");
                    info.update(info.getGraphics());
                }
            } else if (result == false) {
                av.cancel();
                info.setText("Insert coins");
            }
        }
        void clean() {
            balance.setText("0");
            delivery = 0;
            cashUser = 0;
            av.cash = 0;
        }
    };
    class ThreadPB extends Thread {
        @Override
        public void run() {
            progressBar1.setStringPainted(true);
            progressBar1.setMinimum(0);
            progressBar1.setMaximum(100);

            for (int i = progressBar1.getMinimum(); progressBar1.getValue() < progressBar1.getMaximum(); i += 10) {
                try {
                    Thread.sleep(1000);
                    progressBar1.setValue(i);
                    progressBar1.update(progressBar1.getGraphics());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            progressBar1.setValue(0);
            Thread.interrupted();
        }
    }
}



