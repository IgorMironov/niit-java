package sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Rotate;

public class Arrows extends Pane {
    private ImageView imageView;
    private final double angle;


    public Arrows(Image image,double angle){

        this.angle=angle;
        this.imageView=new ImageView(image);
        imageView.setFitHeight(230);
        imageView.setFitWidth(45);
        imageView.setTranslateX(250);//смещение на центр
        imageView.setTranslateY(95);

        Rotate rotate=new Rotate();
        rotate.setPivotX(22.5);//ось (гвоздик)
        rotate.setPivotY(180);//ось (гвоздик)
        rotate.setAngle(angle);
        imageView.getTransforms().addAll(rotate);
       getChildren().addAll(imageView);
    }

}



