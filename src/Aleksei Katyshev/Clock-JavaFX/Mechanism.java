package sample;

import javafx.animation.Animation;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

    class Mechanism extends Pane {
    private Image image=new Image(getClass().getResourceAsStream("4.png"));
    private ImageView mechanismView=new ImageView(image);
    private final int frames=3;
    private final int width=330;
    private final int height=330;
    Mechanism(){
        mechanismView.setViewport(new Rectangle2D(0,0,width,height));
        SpriteAnimation mechanismSprite = new SpriteAnimation(
                mechanismView, Duration.seconds(3),frames,width,height);
        mechanismSprite.setCycleCount(Animation.INDEFINITE);
        mechanismSprite.play();
        getChildren().add(mechanismView);
        setTranslateX(105);
        setTranslateY(105);
    }


}
