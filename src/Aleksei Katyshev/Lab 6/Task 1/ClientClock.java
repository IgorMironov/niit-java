package MyNet;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.WindowEvent;

public class ClientClock extends Application implements Runnable{
    /**var*/
    Thread t;
    final private String ADRESS="127.0.0.1";
    final private int PORT=1234;
    static private Socket connection;
    private String dateS,timeS;
    private TextField time=new TextField("TIME:");
    private TextField today=new TextField("TODAY:");
    private TextField date=new TextField();
    private TextField hourMin=new TextField();
    /**main*/
    public static void main(String[] args){
        launch(args);
         new ClientClock();
    }
    /**cons*/
    public ClientClock(){
        t=new Thread(this,"Clock");
        t.start();
    }
    /**meth*/
    private boolean status(String str){
        if (str.equalsIgnoreCase("close"))
            return true;
        else if (str.equalsIgnoreCase("exit"))
            return true;
        else
            return false;
    }
    private void print(String s){  System.out.println(s);  }
    public void start(Stage primaryStage) throws NullPointerException{
        GridPane root=new GridPane();
        root.setHgap(5);
        root.setVgap(15);
        time.setEditable(false);
        today.setEditable(false);
        date.setEditable(false);
        hourMin.setEditable(false);
        time.setAlignment(Pos.CENTER);
        today.setAlignment(Pos.CENTER);
        date.setAlignment(Pos.CENTER);
        hourMin.setAlignment(Pos.CENTER);
        date.setPrefSize(500,10);
        GridPane.setConstraints(today,0,1);
        GridPane.setConstraints(time,0,0);
        GridPane.setConstraints(date,1,1);
        GridPane.setConstraints(hourMin,1,0);
        root.getChildren().addAll(today,time,date,hourMin);
        Scene scene=new Scene(root);
        primaryStage.setTitle("clock & date");
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent event) {
               t.interrupt();
            }
        });
        primaryStage.show();
    }
    public void run(){
        try {
            connection = new Socket(InetAddress.getByName(ADRESS), PORT);
            print("Подключение к серверу: "+ADRESS);

            BufferedReader in=new BufferedReader(new InputStreamReader(connection.getInputStream()));
            PrintWriter out=new PrintWriter(connection.getOutputStream(),true);
            /**infinite cycle*/
            while(true){
                t.sleep(1000);
                out.println("dateS");
                if (status(dateS=in.readLine())) break;
                date.setText(dateS);
                out.println("timeS");
                if (status(timeS=in.readLine())) break;
                hourMin.setText(timeS);
            }
            out.close();
            in.close();
            connection.close();
        } catch (IOException e) {
            print("IOException");
        }
        catch (InterruptedException e) {
            return;
        }
    }
}
