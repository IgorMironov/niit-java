
import javafx.application.*;
import javafx.event.EventHandler;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class Main extends Application implements Runnable{
    final private String ADRESS="127.0.0.1";
    final private int PORT=1234;
    static private Socket connection;
    private String text="этого невидно совсем";
    private Pane root=new Pane();
    private Label label=new Label(text);
    Thread t;
    public Main(){
        t=new Thread(this);
        t.start();
    }

    public static void main(String[] args){
        launch(args);
    }
    public void start(Stage primaryStage){
        root=new Pane();
        label=new Label(text);
        label.setFont(new Font("Comic Sans MS",getSize(text.length())));
        label.setStyle("-fx-padding: 5;-fx-border-color: black");
        label.setTranslateX(100);
        label.setTranslateY(100);
        label.setWrapText(true);
        label.setTextAlignment(TextAlignment.CENTER);
        label.setPrefSize(500,300);
        root.getChildren().addAll(label);
        primaryStage.setScene(new Scene(root,700,500));
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                t.interrupt();
            }
        });
    }


    private int getSize(int length){
        if(length<25)             return 50;
        else if (length<50)       return 40;
        else if (length<100)      return 35;
        else if (length<150)      return 30;
        else if (length<200)      return 25;
        else if(length<300)       return 20;
        else                      return 15;
    }

    public void run(){
        try{
            connection=new Socket(InetAddress.getByName(ADRESS),PORT);
            print("подключение: "+ADRESS);
            BufferedReader in= new BufferedReader(new InputStreamReader(connection.getInputStream()));
            PrintWriter out=new PrintWriter(connection.getOutputStream(),true);
            while(true) {

                print("send");
                out.println("ready");
                text=in.readLine();
                    Platform.runLater(() -> {
                        label.setFont(new Font("Comic Sans MS", getSize(text.length())));
                        label.setText(text);
                        root.requestLayout();
                    });

                t.sleep(1000);



            }
        } catch (InterruptedException e) { return;
        } catch (IOException io){print("io");return;}

    }

    private boolean status(String s){
        if(s.equals("exit"))
            return true;
        else if(s.equals("close"))
            return true;
        else return false;
    }
    private void print(String s){
        System.out.println(s);
    }
}
