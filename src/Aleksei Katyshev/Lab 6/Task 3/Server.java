


import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;



class ServerOne extends Thread{
    private Socket socket=new Socket();
    private BufferedReader in;
    private PrintWriter out;
    private HashMap<String,String> myMap;
    SimpleDateFormat timeNow=new SimpleDateFormat("ss");

    public ServerOne(Socket s,HashMap<String,String> map) throws IOException{

        socket =s;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(
                new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);
        myMap=map;
        start();

    }

    public void run(){
        try{


            while(true){
                if(myMap==null){
                    System.out.println("NO MSG FOR YOU! sry =(");
                    break;
                }



                String output="";
                String timeServer;


                while(true) {
                    Date date=new Date();
                    timeServer=timeNow.format(date).toString();
                   // System.out.print(" S: "+timeServer);

                    for(Map.Entry<String,String> buf: myMap.entrySet()){
                        if(timeServer.toString().equals(buf.getKey())){
                           // System.out.println(buf.getValue());
                            if(!output.equals(buf.getValue())){
                                output=buf.getValue();
                                System.out.println(output);
                                out.println(output);
                            }
                        }
                    }
                sleep(500);
                }
            }
            print("disconnect");
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        } finally{
            try{
                socket.close();
            }
            catch(IOException io){
                print("socket closed");
            }
        }
    }
    private void print(String s){
        System.out.println(s);
    }
}

public class Server{
    static final int PORT = 1234;
    public static void main(String[] args)throws IOException{


        ServerSocket s= new ServerSocket(PORT);
        Collections collections=new Collections();
        print("Start server");
        try{
            while(true){
                Socket socket=s.accept();
                try {
                    print("Connect compleat");
                    new ServerOne(socket,collections.getMyMap(socket.getInetAddress().toString()));
                }
                catch (IOException io){
                    socket.close();
                }
            }


        }
        finally {
            s.close();
        }
    }

    private static void print(String s){
        System.out.println(s);
    }
}

class Collections{
    BufferedReader inf;
    HashMap<String,HashMap> map=new HashMap<>();
    HashMap<String,String> timeMSG =new HashMap<>();
    ArrayList<InetAddress> ipAddresses=new ArrayList<>();

    Collections() throws IOException {
        String bufStr;
        String[] splitStr;
        inf = new BufferedReader(new FileReader(new File("timeMSG.txt")));
        BufferedReader bufferedLine = new BufferedReader(inf);
        while ((bufStr = bufferedLine.readLine()) != null) {
            if (!bufStr.isEmpty()) {
                    splitStr = bufStr.split("\t", 3);
                    timeMSG.put(splitStr[1],splitStr[2]);
                    map.put(splitStr[0],timeMSG);
            }
        }

    }

    HashMap<String,String> getMyMap(String ip){
        HashMap<String,String> result=null;
        for(Map.Entry<String,HashMap> buf:map.entrySet()){
            if (buf.getKey().toString().equals(ip)){
                result=buf.getValue();
            }
        }
        return result;
    }
    void addIP(InetAddress ip){
        ipAddresses.add(ip);
        System.out.print(ip);
    }
}