package qqq;


import java.io.*;
import java.net.*;


class ServerOne extends Thread{
    private Socket socket=new Socket();
    private BufferedReader in;
    private PrintWriter out;
    private BufferedReader inf;

    public ServerOne(Socket s) throws IOException{
        socket =s;
         in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             out = new PrintWriter(
                    new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);
        inf = new BufferedReader(new FileReader(new File("aphorisms.txt")));
        start();
    }

    public void run(){
        try{
            while(true){
                String input;
                String output="";
                input = in.readLine();
                print("получил "+input);
                while(output.length()<10) {
                 output = inf.readLine();
                }

               if(input.equals("END")) break;
                print("выход: "+output);
               out.println(output);
            }
            print("disconnect");
        }

        catch(IOException io){
            print("io");
        }
        finally{
            try{
                socket.close();
            }
            catch(IOException io){
                print("socket closed");
            }
        }
    }
    private void print(String s){
        System.out.println(s);
    }
}

public class Server{
    static final int PORT = 1234;

    public static void main(String[] args)throws IOException{


        ServerSocket s= new ServerSocket(PORT);
        print("Start server");
        try{
            while(true){
                Socket socket=s.accept();
                try {
                    print("Connect compleat");
                    print("Client: "+socket.getInetAddress());
                    new ServerOne(socket);
                }
                catch (IOException io){
                    socket.close();
                }
            }


        }
        finally {
            s.close();
        }
    }
    private static void print(String s){
       System.out.println(s);
    }
}