import java.math.BigDecimal;

class squareRoot{
   double delta=0.00000001;
   double arg;

   squareRoot(double arg) { 
      this.arg=arg;
   }
   
   
   double average(double x,double y) {
      return (x+y)/2.0;
   }
   
   boolean good(double guess,double x) {
      return Math.abs(guess*guess-x)<delta; //
   }
   
   double improve(double guess,double x) {
      return average(guess,x/guess);
   }
   
   double iter(double guess, double x) { 
      if(good(guess,x)) 
         return guess;
      else
         return iter(improve(guess,x),x);
   }
   
   public double calc() {
      return iter(1.0,arg);
   }
}



public class Lab1Task2{
   public static void main(String[] args)
   {
		double val=Double.parseDouble(args[0]);
		int accuracy=Integer.parseInt(args[1]);
		squareRoot sqrt=new squareRoot(val);
		double result=sqrt.calc();
		BigDecimal rounded=new BigDecimal(result);
		System.out.println("squareRoot of "+val+" it's "+rounded.setScale(accuracy,BigDecimal.ROUND_HALF_UP));
   }
}
