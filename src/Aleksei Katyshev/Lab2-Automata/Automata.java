package Automata;

public class Automata {
    public static void main(String[]args){
        CoffeeAutomat cof=new CoffeeAutomat();
        //while(true){
        if(cof.getState().equals("OFF")){
            cof.printState();
            cof.on();
        }
        if(cof.getState().equals("WAIT")){
            cof.printState();
            cof.printMenu();
            // cof.coin(50);
            // cof.cancel();
        }
        if(cof.getState().equals("ACCEPT")){
            cof.coin(50);
            cof.choice(1);
        }
        //}
    }


}


class CoffeeAutomat{
    private final String[] menu= new String[]{
            "Espresso",
            "Cappuccino",
            "Hot chocolate",
            "Latte macchiato",
            "Black tea",
            "Green tea",
            "Red tea",
            "Hot water"
    };
    private final int[] prices=new int[]{20,35,25,30,15,15,15,5};
    private int cash=0;
    enum allStates{
        OFF,WAIT,ACCEPT,COOK,CHANGE
    }
    private allStates state = allStates.OFF;
    /** Getters and Setters */
    int getCash() {
        return cash;
    }
    private void setCash(int cash){
        this.cash=cash;
    }
    private int getPrices(int i) {
        return prices[i];
    }
    private String getMenu(int i) {
        return menu[i];
    }
    String getState() {
        return state.toString();
    }
    public void setState(allStates state) {
        this.state = state;
    }
    /** Methods */
    void on(){
        if(state==allStates.OFF){
            setState(allStates.WAIT);
        }
    }
    protected void off(){
        if(!(state==allStates.OFF)){
            giveChange();
            setState(allStates.OFF);
        }
    }

    void coin(int money){
        setState(allStates.ACCEPT);
        setCash(getCash()+money);
    }


    private void giveChange(){
        if(getCash()>0) {
            print("Keep your change:" + getCash());
            setState(allStates.CHANGE);
            setCash(0);
        }
        setState(allStates.WAIT);
    }
    void printMenu(){
        setState(allStates.ACCEPT);
        for(int i=0;i<menu.length;i++){
            print( (i+1) + ". - " + getMenu(i)+" "+getPrices(i));
        }
    }
    void printState(){
        print(getState());
    }
    void choice(int choice){
        if(check(choice-1)){
            setState(allStates.COOK);
            setCash(getCash()-getPrices(choice-1));
            print("cooking...");
            finish();
        }
        else{
            print("Sorry, you don't have enough money. Insert more or keep money back.");
        }
    }
    private boolean check(int i) {
        return getPrices(i) < getCash();
    }
    void cancel(){
        giveChange();
        print("Goodbye");
    }
    private void finish(){
        giveChange();
        print("Thank You! Take your drink.");
        off();
    }
    /** Service methods*/
    private void print(String msg){
        System.out.println(msg);
    }
}