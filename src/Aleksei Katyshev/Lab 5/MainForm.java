import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainForm extends JFrame{
    Automata cofeman=new Automata();
    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JButton blackTea;
    private JButton redTea;
    private JButton espresso;
    private JButton hotChocolate;
    private JButton capuccino;
    private JButton latteMacchiato;
    private JButton greenTea;
    private JButton hotWater;
    private JComboBox comboBox1;
    private JButton coinButton;
    private JButton ON;
    private JTextField change;
    private JButton takeMoneyButton;
    private JProgressBar progressBar1;
    private JButton getBack;


    public MainForm() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        textField1.setEditable(false);
        textField1.setText(cofeman.getStatus());
        textField2.setEditable(false);
        change.setEditable(false);
        ON.setBackground(Color.red);



        setContentPane(panel1);
        setSize(500,300);
        setVisible(true);

        ON.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cofeman.getStatus()=="OFF"){
                    textField2.setText("Balance: 0");
                    cofeman.setStatus("WAIT");
                    textField1.setText("Incert money and choose drink");
                    ON.setBackground(Color.yellow);
                }
                else if(cofeman.getStatus()=="WAIT"){
                    cofeman.setStatus("OFF");
                    textField1.setText("OFF");
                    textField2.setText("");
                    ON.setBackground(Color.red);
                }
            }
        });

        coinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(cofeman.getStatus()=="WAIT") {
                    cofeman.setBalance((String) comboBox1.getSelectedItem());
                    textField2.setText("Balance:" + cofeman.getBalance());
                }
            }
        });


        espresso.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    double price = 2.5;
                    getDrink(price);
            }
        });

        capuccino.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double price = 3.5;
                getDrink(price);
            }
        });

        hotChocolate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double price = 2.0;
                getDrink(price);
            }
        });

        latteMacchiato.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double price = 4.0;
                getDrink(price);
            }
        });

        blackTea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double price = 1.5;
                getDrink(price);
            }
        });

        greenTea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double price = 1.5;
                getDrink(price);
            }
        });

        hotWater.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double price = 0.5;
                getDrink(price);
            }
        });



        takeMoneyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                change.setText("0");
            }
        });

        getBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(cofeman.getStatus()=="WAIT") {
                    change.setText(Double.toString(cofeman.giveChange()));
                    textField2.setText("Balance:" + cofeman.getBalance());
                    takeChange();
                }
            }
        });

    }
    private void takeChange(){
        if (change.getText() != ("0")) {
            textField1.setText("Take your change! Goodbye.");
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        textField1.setText("Incert money and choose drink");
        cofeman.setStatus("WAIT");

    }
    private void getDrink(double price){
        if(cofeman.getStatus()=="WAIT")
            if(cofeman.check(price)){
                cofeman.choice(price);
                getPB();
                change.setText(Double.toString(cofeman.giveChange()));
                textField2.setText("Balance:"+cofeman.getBalance());

            }
            else {
                textField1.setText("You don't have enough money. Add or get back money.");
            }
    }

    private void getPB(){
        new TheadCompleat();
    }

    private class TheadCompleat implements Runnable{
        TheadCompleat(){
            Thread cmplt=new Thread(this,"PB");
            cmplt.start();
        }


        @Override
        public void run() {
            progressBar1.setStringPainted(true);
            progressBar1.setMinimum(0);
            progressBar1.setMaximum(100);

            cofeman.setStatus("COOK");
            textField1.setText("Cooking...");
            for(int i=progressBar1.getMinimum();progressBar1.getValue()<progressBar1.getMaximum();i+=10){
                try {
                    Thread.sleep(1000);
                    progressBar1.setValue(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            textField1.setText("Get your drink. Thank you!");
            progressBar1.setValue(0);
            cofeman.setStatus("CHANGE");
            takeChange();
            Thread.interrupted();
        }
    }
}

