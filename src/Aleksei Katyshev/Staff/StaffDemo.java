import java.io.*;
import java.lang.String;

import java.util.ArrayList;
import java.util.List;

public class StaffDemo {
    public static void main(String args[]){
        MainMethod mainMeth=new MainMethod();
        mainMeth.fillingProject();
        mainMeth.payPersonal();
        mainMeth.printAll();
    }
}

class MainMethod{
    private ProjectName prj1;
    private ProjectName prj2;
    private CalcCon calcCon;
    private  List<Cleaners> cleaners = new ArrayList<Cleaners>();
    private List<Drivers> drivers = new ArrayList<Drivers>();
    private List<Managers> managers = new ArrayList<Managers>();
    private List<Programmers> programmers = new ArrayList<Programmers>();
    private  List<Testers> testers = new ArrayList<Testers>();
    private  List<ProjectManagers> projectManagers = new ArrayList<ProjectManagers>();
    private  List<SeniorManagers> seniorManagers = new ArrayList<SeniorManagers>();
    private List<TeamLeaders> teamLeaders = new ArrayList<TeamLeaders>();
    MainMethod(){
        prj1=new ProjectName("prj1",500000);
        prj2=new ProjectName("prj2",700000);
        readFile();
    }

    private void readFile() {
        String[] oneStr;
        File fl = new File("personal.txt");
        FileReader frdr;
        String bufStr;
        try {
            frdr = new FileReader(fl);
            BufferedReader buffered = new BufferedReader(frdr);
            for (; (bufStr = buffered.readLine()) != null; ) {
                if (!bufStr.isEmpty()) {
                    oneStr = bufStr.split("\t", 3);
                    creatProf(Integer.parseInt(oneStr[0]), oneStr);
                }
            }
            frdr.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void creatProf(int id, String[] strArr){
        if (strArr[2].equals("Tester")){
            testers.add(new Testers(id,strArr[1],strArr[2]));
        }
        if (strArr[2].equals("Programmer")){
            programmers.add(new Programmers(id,strArr[1],strArr[2]));
        }
        if (strArr[2].equals("Senior manager")){
            seniorManagers.add(new SeniorManagers(id,strArr[1],strArr[2]));
        }
        if (strArr[2].equals("Team leader")){
            teamLeaders.add(new TeamLeaders(id,strArr[1],strArr[2]));
        }
        if (strArr[2].equals("Project manager")){
            projectManagers.add(new ProjectManagers(id,strArr[1],strArr[2]));
        }
        if (strArr[2].equals("Manager")){
            managers.add(new Managers(id,strArr[1],strArr[2]));
        }
        if (strArr[2].equals("Cleaner")){
            cleaners.add(new Cleaners(id,strArr[1],strArr[2]));
        }
        if (strArr[2].equals("Driver")){
            drivers.add(new Drivers(id,strArr[1],strArr[2]));
        }
    }
    void fillingProject(){
        appointEmployees(prj1);
        appointEmployees(prj2);
    }
    private void appointEmployees(ProjectName project){
            getTesters(project);
            getProgrammers(project);
            getManagers(project);
            getTL(project);
            getPM(project);
            getSM(project);
    }
    private void getTesters(ProjectName project) {
        int i=0;
        for(Testers tester: testers){
            if(tester.getProject().equals("free")&& (i<project.needTesters)){
                i++;
                tester.setProject(project.name);
                calcCon = new CalcCon(project, tester);
                project.setTotalContribution(calcCon.newCon());
                project.setTotalPayment(calcCon.newTotal());
                tester.calcPaymentForWorkTime();
            }
        }
    }
    private void getProgrammers(ProjectName project){
        int i=0;
        for(Programmers proger : programmers){
            if(proger.getProject().equals("free")&& (i<project.needProgrammers)){
                i++;
                proger.setProject(project.name);
                calcCon = new CalcCon(project, proger);
                project.setTotalContribution(calcCon.newCon());
                project.setTotalPayment(calcCon.newTotal());
                proger.calcPaymentForWorkTime();
            }
        }
    }
    private void getManagers(ProjectName project){
        int i=0;
        for(Managers manager : managers){
            if(manager.getProject().equals("free") && i<project.needManagers){
                i++;
                manager.setProject(project.name);
                calcCon = new CalcCon(project, manager);
                project.setTotalContribution(calcCon.newCon());
                project.setTotalPayment(calcCon.newTotal());
            }
        }
    }
    private void getTL(ProjectName project){
        for (TeamLeaders teamLeader : teamLeaders) {
            if (teamLeader.getProject().equals("free")) {
                teamLeader.setProject(project.name);
                calcCon = new CalcCon(project, teamLeader);
                project.setTotalContribution(calcCon.newCon());
                project.setTotalPayment(calcCon.newTotal());
                teamLeader.calcPaymentForWorkTime();
                teamLeader.calcPaymentForHeading();
                break;
            }
        }
    }
    private void getPM(ProjectName project){
        for (ProjectManagers projectManager : projectManagers) {
            if (projectManager.getProject().equals("free")) {
                projectManager.setProject(project.name);
                projectManager.setContribution(project.getTotalContribution() / 2);//остаток пополам с сеньором
                project.setTotalPayment(projectManager.calcPaymentForContribution(project.buget, project.getTotalPayment()));
                projectManager.calcPaymentForHeading();
                break;
            }
        }
    }
    private void getSM(ProjectName project){
        for (SeniorManagers seniorManager : seniorManagers) {
            if (seniorManager.getProject().equals("free")) {
                seniorManager.setContribution(project.getTotalContribution()/2);//остаток контрибуции на сеньоре
                project.setTotalPayment(seniorManager.calcPaymentForContribution(project.buget, project.getTotalPayment()));
                seniorManager.calcPaymentForHeading();
                break;
            }
        }
    }
    void payPersonal(){
        for(Cleaners cleaner:cleaners)
            cleaner.calcPaymentForWorkTime();
        for(Drivers driver:drivers){
            driver.calcPaymentForWorkTime();
        }
    }
   void printAll(){
        for(SeniorManagers buf:seniorManagers) {
            System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
        }
       System.out.println("");
       for(ProjectManagers buf:projectManagers) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }
       System.out.println("");
       for(Managers buf:managers) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }
       System.out.println("");
       for(TeamLeaders buf:teamLeaders) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }
       System.out.println("");
       for(Testers buf:testers) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }
       System.out.println("");
       for(Programmers buf:programmers) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }
       System.out.println("");
       for(TeamLeaders buf:teamLeaders) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }
       System.out.println("");
       for(Cleaners buf:cleaners) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }
       System.out.println("");
       for(Drivers buf:drivers) {
           System.out.println(buf.fio + ": " + buf.prof + " на счету: " + (int) buf.payment + " руб.");
       }


   }
}


class CalcCon {
    private double total;
    private double contribution;
    private double newTotal;
    CalcCon(ProjectName prj, TeamLeaders employee) {
        this.total = prj.getTotalContribution();
        this.contribution = employee.getContribution();
        this.newTotal=employee.calcPaymentForContribution(prj.buget,prj.getTotalPayment());
    }

    CalcCon(ProjectName prj, Managers employee) {
        this.total = prj.getTotalContribution();
        this.contribution = employee.getContribution();
        this.newTotal=employee.calcPaymentForContribution(prj.buget,prj.getTotalPayment());
    }

    CalcCon(ProjectName prj, Testers employee) {
        this.total = prj.getTotalContribution();
        this.contribution = employee.getContribution();
        this.newTotal=employee.calcPaymentForContribution(prj.buget,prj.getTotalPayment());
    }

    CalcCon(ProjectName prj, Programmers employee) {
        this.total = prj.getTotalContribution();
        this.contribution = employee.getContribution();
        this.newTotal=employee.calcPaymentForContribution(prj.buget,prj.getTotalPayment());
    }

    double newCon() {
        return total - contribution;
    }

    double newTotal() {
        return newTotal;
    }
}
