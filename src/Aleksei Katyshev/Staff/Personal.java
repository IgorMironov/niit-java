
public class Personal extends Employee implements WorkTime {
    int hours;
    double payInHour;
    Personal(int id,String fio,String prof){
        super(id, fio, prof);
    }
    public void calcPaymentForWorkTime() {
        setPayment(hours*payInHour);
    }
}

class Cleaners extends Personal {

    Cleaners(int id, String fio,String prof) {
        super(id, fio, prof);
        this.payInHour=PAAYMENT_FOR_HOUR_CLEANER;
        this.hours=WORK_TIME_CLEANER;
    }
}
class Drivers extends Personal{
    Drivers(int id,String fio,String prof){
        super(id, fio, prof);
        this.payInHour=PAAYMENT_FOR_HOUR_DRIVER;
        this.hours=WORK_TIME_DRIVER;
    }
}