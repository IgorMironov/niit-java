
 public class ProjectName {
     final String name;
     double totalContribution=100;
     double totalPayment;
     final double buget;
     final int needTesters=5;
     final int needProgrammers=10;
     final int needManagers=2;


     ProjectName(String name,double totalPayment){
         this.name=name;
         this.totalPayment=totalPayment;
         this.buget=totalPayment;
     }

    public double getTotalContribution(){
        return totalContribution;
    }
    public  void setTotalContribution(double totalContribution){
        this.totalContribution=totalContribution;
    }

    public double getTotalPayment() {
        return totalPayment;
    }
    public void setTotalPayment(double totalPayment) {
        this.totalPayment = totalPayment;
    }
}
