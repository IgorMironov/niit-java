
public class Managers extends Employee implements Project {
    double contribution;
    Managers(int id,String fio,String prof){
        super(id,fio,prof);
        this.contribution=MAX_CON_MANAGER;
        this.project="free";
    }
    double getContribution(){
        return contribution;
    }
    public void setContribution(double contribution) {
        this.contribution = contribution;
    }
    public double calcPaymentForContribution(double buget,double summ) {
        setPayment(contribution/100*buget);
        return summ-((contribution/100)*buget);
    }
}


class ProjectManagers extends Managers implements Heading{
    ProjectManagers(int id,String fio,String prof){
        super(id, fio, prof);
    }
    public void setContribution(double contribution) {
        this.contribution = contribution;
    }
    public void calcPaymentForHeading() {
        setPayment(PAY_FOR_ONE_PM*LEADS_PM);
    }
}


class SeniorManagers extends ProjectManagers implements Heading{
    SeniorManagers(int id,String fio,String prof){
        super(id, fio, prof);
    }
    public void calcPaymentForHeading() {
        setPayment(PAY_FOR_ONE_SM*LEADS_SM);
    }
}