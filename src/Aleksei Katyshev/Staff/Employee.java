public class Employee {
    int id;
    String fio;
    String prof;
    double payment;
    String project;

    Employee(int id,String fio,String prof){
        this.id=id;
        this.fio=fio;
        this.prof=prof;
    }

    public void setProject(String project) {
        this.project = project;
    }
    public String getProject() {
        return project;
    }
    public void setPayment(double payment){
        this.payment+=payment;
    }
}
