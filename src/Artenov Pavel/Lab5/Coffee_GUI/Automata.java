package coffee_gui;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


class Automata {
    enum STATES {
        OFF,WAIT,ACCEPT,CHECK,COOK
    }
    private int cash,i,choice;
    private String[] menu;
    private int[] prices;
    STATES state=STATES.OFF;
    Automata(int cash,String[] menu,int[] prices){
        this.cash=cash;this.menu=menu;this.prices=prices;this.state=state;
    }
    private JProgressBar progBar = new JProgressBar();
    private JLabel status = new JLabel("Off");
    private JLabel cashLabel = new JLabel(Integer.toString(cash));

    void go (){

        JFrame automat = new JFrame("Coffee");

        final JRadioButton rbOne = new JRadioButton(menu[0]+"  "+prices[0]);
        final JRadioButton rbTwo = new JRadioButton(menu[1]+"  "+prices[1]);
        final JRadioButton rbThree = new JRadioButton(menu[2]+"  "+prices[2]);
        final JRadioButton rbFour = new JRadioButton(menu[3]+"  "+prices[3]);
        final JRadioButton rbFive = new JRadioButton(menu[4]+"  "+prices[4]);

        class rbListener implements ActionListener {
            public void actionPerformed(ActionEvent event) {
                if (event.getSource()==rbOne)
                    choice(0);
                else if (event.getSource()==rbTwo)
                    choice(1);
                else if (event.getSource()==rbThree)
                    choice(2);
                else if (event.getSource()==rbFour)
                    choice(3);
                else
                    choice(4);

            }
        }

        ButtonGroup group = new ButtonGroup();
        group.add(rbOne);group.add(rbTwo);group.add(rbThree);group.add(rbFour);group.add(rbFive);

        JPanel rbButtonPanel = new JPanel(new GridLayout(0,1,0,5));
        rbButtonPanel.setBorder(BorderFactory.createTitledBorder("Menu"));
        rbButtonPanel.add(rbOne);rbButtonPanel.add(rbTwo);rbButtonPanel.add(rbThree);rbButtonPanel.add(rbFour);
        rbButtonPanel.add(rbFive);

        status.setFont(new Font("Verdana", Font.BOLD, 20));
        progBar.setStringPainted(true);
        progBar.setMinimum(0);
        progBar.setMaximum(100);
        progBar.setValue(0);

        JPanel powerAndStatus = new JPanel();
        JButton btOn = new JButton("Power On");
        powerAndStatus.add(btOn);powerAndStatus.add(status);

        JPanel progBarAndStart = new JPanel();
        JButton btCook = new JButton("Cook");
        progBarAndStart.add(btCook);progBarAndStart.add(progBar);

        JPanel moneyButton = new JPanel();
        JButton btMoneyPlus = new JButton("add 5$");
        JButton btMoneyMinus = new JButton("subtract 5$");
        moneyButton.add(btMoneyPlus);moneyButton.add(btMoneyMinus);moneyButton.add(cashLabel);

        //слушаем радиобаттоны
        rbOne.addActionListener(new rbListener());
        rbTwo.addActionListener(new rbListener());
        rbThree.addActionListener(new rbListener());
        rbFour.addActionListener(new rbListener());
        rbFive.addActionListener(new rbListener());

        //обрабатываем события
        btOn.addActionListener((new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if(on())
                    status.setText("ON");
            }
        }));
        btMoneyPlus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                coin(5);
                cashLabel.setText(Integer.toString(cash));
            }
        });
        btMoneyMinus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if(cash>=5) {
                    coin(-5);
                }
                cashLabel.setText(Integer.toString(cash));
            }
        });
        btCook.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (cook()){
                    ThreadCl threadCl = new ThreadCl();
                    threadCl.start();
                    finish();
                }
            }
        });
        automat.add(BorderLayout.NORTH,powerAndStatus);
        automat.add(BorderLayout.SOUTH,progBarAndStart);
        automat.add(BorderLayout.CENTER,moneyButton);
        automat.add(BorderLayout.WEST,btOn);
        automat.add(BorderLayout.EAST,rbButtonPanel);
        automat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        automat.setSize(600,400);
        automat.setVisible(true);
    }
    public class ThreadCl extends Thread {
        public void run() {
            int i = 0;
            while (i <= 100) {
                progBar.setValue(i);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {}
                i=i+1;
            }
            JOptionPane.showMessageDialog(null,"Take your drink ","Finish",
                    JOptionPane.WARNING_MESSAGE);
            progBar.setValue(0);

        }
    }
    private boolean on(){
        if(state==STATES.OFF) {
            state = STATES.WAIT;
            return true;
        }
        else
            return false;
    }
    private void coin(int cash){
        if((state==STATES.WAIT) || (state==STATES.ACCEPT)||(state==STATES.CHECK)) {
            state = STATES.ACCEPT;
            this.cash = cash + this.cash;
            status.setText("ACCEPT");
        }
    }
    private void choice(int choice){
        if((state==STATES.ACCEPT) || (state==STATES.CHECK)){
            this.choice=choice;
            state=STATES.CHECK;
            JOptionPane.showMessageDialog(null,"You choice : "+menu[choice],"Your drink",
                    JOptionPane.WARNING_MESSAGE);
            status.setText("Choice");
            check();
        }
    }
    private void check(){
        if(cash>=prices[choice])
            state=STATES.COOK;
        else {
            JOptionPane.showMessageDialog(null, "Not enough money", "Error",
                    JOptionPane.WARNING_MESSAGE);
            status.setText("Accept");
        }
    }
    private boolean cook() {
        if (state == STATES.COOK) {
            status.setText("Cook");
            return true;
        }
        else
            return false;
    }
    private void finish(){
        state=STATES.WAIT;
        JOptionPane.showMessageDialog(null,"Take excess money  :"+Integer.toString(cash-prices[choice]),"Attention",
                JOptionPane.WARNING_MESSAGE);
        cash=0;
        cashLabel.setText(Integer.toString(cash));
        status.setText("Wait");
    }
}
