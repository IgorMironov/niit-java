/*Кофейный автомат с использованием графического пользовательского интерфейса*/
package coffee_gui;


public class App{

    public static void main( String[] args ){
        String[] menu=new String[]
                {"Americano","Cappuccino","Latte","Espresso","Irish"};
        int[] prices={10,20,30,40,50};

        Automata one=new Automata(0,menu,prices);
        one.go();
    }
}
