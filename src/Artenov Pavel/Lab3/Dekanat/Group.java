package Dekanat2;
/*
Разработать класс Group для хранения информации об учебной группе
Перечень полей:
Title-название группы
Students-массив из ссылок на студентов
Num-колличество студентов в группе
Head-ссылка на старосту(из членов группы)
Обеспечить класс следующими методами:
-создание группы с указанием названия
-добавление студента
-избрание старосты
-поиск студента по ФИО или ИД
-вычисление среднего балла в группе
-исключение студента из группы
 */

public class Group {
    private String title;
    private Student[] studentLinks = new Student[40];
    private int num;
    private Student head;

    //Создание группы с указанием названия
    Group(String title) {
        this.title = title;
    }
    public void createStudent(int index,Student student) {
        studentLinks[index] = student;
        num++;
    }
    //исключение студента из группы
    public void deleteStudent(Student student) {
        int i = 0;
        while (studentLinks[i] != student) {
            i++;
        }
        studentLinks[i] = null;
        int j=i;
        i++;
        while (studentLinks[i] != null) {
            i++;
        }
        studentLinks[j] = studentLinks[i-1];
        studentLinks[i-1]=null;
        num--;
    }


    //Добавление студента
    public void addStudent(Student student){
        int i = 0;
        while(studentLinks[i] != null)
            i++;
        studentLinks[i] = student;
        num++;
    }
    //Печать имени группы и студентов группы
    public void printStudentInGroup(){
        int i=0;
        System.out.println(getTitle());
        while(studentLinks[i] != null) {
            System.out.println(studentLinks[i].getName());
            i++;
        }
    }

    //Избрание старосты
    public void selectionHead(){
        int[]arr = new int[num];
        int i,temp,max=0,count=0,j,head_index=0;
        for(i=0;i<num;i++){
            arr[i]=(int)Math.random() * num;
        }
        for(i=0;i<num;i++){
            temp=arr[i];
            for(j=0;j<num;j++){
                if(temp==arr[j])
                    count++;
            }
            if(count>max){
                max=count;
                head_index=temp;
            }

        }
        head = studentLinks[head_index];
    }
    //поиск по фио или id
    public void findStudentInGroup(String name){
        boolean find=false;
        for(int i=0;i<num;i++){
            if(name.equals(studentLinks[i].getName())) {
                System.out.println("Student " + name + " is in "+title);
                find = true;
            }
        }
        if(!find)
            System.out.println("Student "+name+" not is in"+title);

    }
    public void findStudentInGroup(int id){
        boolean find=false;
        for(int i=0;i<num;i++){
            if(id==studentLinks[i].getId()) {
                System.out.println("Student id= " + id + " is in " + title);
                find=true;
            }
        }
        if(!find)
            System.out.println("Student id= "+" not is in "+ title );
    }
    //вычисление среднего балла в группе
    public void avgMarkInGroup(){
        double sum=0;
        for(int i=0;i<num;i++){
            sum=sum+studentLinks[i].getAvgMark();
        }
        System.out.println("Avg mark in group "+getTitle()+" = "+sum/num);
    }

    public String getTitle(){
        return title;
    }



}
