package Server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


    class Server extends Thread {
     private Socket socket;
     private PrintWriter printWriter;
     private BufferedReader bufferedReader;

    public Server(Socket socket) throws IOException{
        this.socket = socket;
        printWriter = new PrintWriter((socket.getOutputStream()), true);
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        start();
    }

    public void run(){

       Timer timer = new Timer(printWriter);
       Aphorism aphorism = new Aphorism(printWriter);
    }


}
public class App {
    static final int PORT = 9999;

    public static void main( String[] args ) {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);  //старт сервера
            while(true){
                Socket socket = serverSocket.accept();  //ждем подключения
                try {
                    System.out.println("Подключился клиент"+socket.getInetAddress());
                    new Server(socket);
                }
                catch (IOException ioex){
                    socket.close();
                }

            }
        }
        catch(IOException ioex){
            ioex.printStackTrace();
        }



    }
}
