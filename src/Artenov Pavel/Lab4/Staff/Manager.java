package Staff;

public class Manager extends Employee implements Project {
    protected double coefficientOfParticipation;
    protected int budjetProject;
    public Manager(int id,String name,int worktime,double coefficientOfParticipation,int budjetProject,String position){
        super(id,name,worktime,position);
        this.coefficientOfParticipation = coefficientOfParticipation;
        this.budjetProject = budjetProject;
    }
    public double calcPaymentProject() {
        return coefficientOfParticipation * budjetProject;
    }
    public int paymentCalc(){                                                //зп исходя из бюджета проекта
        return (int)calcPaymentProject();
    }
}
class ProjectManager extends Manager implements Heading {
    protected int countSubordinate;
    public ProjectManager(int id,String name,int worktime,double coefficientOfParticipation,int budjetProject,
                           int countSubordinate,String position){
        super(id,name,worktime,coefficientOfParticipation,budjetProject,position);
        this.countSubordinate = countSubordinate;
    }
    public int calcPaymentHeading(){
        return countSubordinate * ratePerSubordinate;
    }
    public int paymentCalc(){                                              //зп исходя из бюджета проекта и кол-ва подчиненных
        return (int)calcPaymentProject() + calcPaymentHeading();
    }
}
class SeniorManager extends ProjectManager {
    public SeniorManager (int id,String name,int worktime,double coefficientOfParticipation,int budjetProject,
                          int countSubordinate,String position){
        super(id,name,worktime,coefficientOfParticipation,budjetProject,countSubordinate,position);
    }
}