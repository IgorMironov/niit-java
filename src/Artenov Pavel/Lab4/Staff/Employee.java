package Staff;

public class Employee {
    protected int id;
    protected String name;
    protected int worktime;
    protected int payment;
    protected String position;

    public Employee(int id,String name,int worktime,String position){
        this.id=id;
        this.name=name;
        this.worktime=worktime;
        this.position=position;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name){
        this.name=name;
    }
    public void setWorktime(int worktime){
        this.worktime=worktime;
    }
    public void setPayment(int payment){
        this.payment=payment;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getWorktime() {
        return worktime;
    }

    public int paymentCalc(){
        return 0;
    };
    public String getPosition(){
        return position;
    }



}
