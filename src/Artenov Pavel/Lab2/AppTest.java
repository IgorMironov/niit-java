package Coffee_console;

import org.junit.Test;
import org.junit.Assert;

public class AppTest
{
    Automata testAutomata = new Automata(
            0,
            new String[] {"Americano","Cappuccino","Latte","Espresso","Irish"},
            new int [] {10,20,30,40,50}
    );
    @Test
    public void testOn() throws Exception {
        testAutomata.on();
        Assert.assertEquals(testAutomata.state.WAIT,testAutomata.state);
    }

    @Test
    public void testConstructor() throws Exception {
        Assert.assertArrayEquals(new String[] {"Americano","Cappuccino","Latte","Espresso","Irish"},testAutomata.getMenu());
        Assert.assertEquals(0,testAutomata.getCash());
        Assert.assertArrayEquals(new int[] {10,20,30,40,50},testAutomata.getPrices());
    }

    @Test
    public void testCoin() throws Exception {
        testAutomata.on();
        testAutomata.coin(20);
        Assert.assertEquals(20,testAutomata.getCash());
        Assert.assertEquals(testAutomata.state.ACCEPT,testAutomata.state);
    }
    @Test
    public void testChoice() throws Exception {
        testAutomata.on();
        testAutomata.coin(20);
        testAutomata.choice(2);
        Assert.assertEquals(2,testAutomata.getChoice());
        Assert.assertEquals(testAutomata.state.CHECK,testAutomata.state);
    }
    @Test
    public void testCheck() throws Exception {
        testAutomata.on();
        testAutomata.coin(30);
        testAutomata.choice(2);
        testAutomata.check();
        Assert.assertEquals(testAutomata.state.COOK,testAutomata.state);
    }
    @Test
    public void testCook() throws Exception {
        testAutomata.on();
        testAutomata.coin(30);
        testAutomata.choice(2);
        testAutomata.check();
        testAutomata.cook();
        Assert.assertEquals(testAutomata.state.WAIT,testAutomata.state);
        Assert.assertEquals(0,testAutomata.getCash());



    }
}
