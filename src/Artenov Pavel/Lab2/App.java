package Coffee_console;

class Automata {
    enum STATES {
        OFF,WAIT,ACCEPT,CHECK,COOK
    }
    private int cash,i,choice;
    private String[] menu;
    private int[] prices;
    STATES state=STATES.OFF;
    Automata(int cash,String[] menu,int[] prices){
        this.cash=cash;this.menu=menu;this.prices=prices;this.state=state;
    }


    void on(){
        if(state==STATES.OFF)
            state=STATES.WAIT;
    }

    void printMenu(int count){
        if(state==STATES.WAIT)
            for(i=0;i<count;i++){
                System.out.println(menu[i]+" - "+prices[i]);
            }
    }
    void printState(){
        if(state!=STATES.OFF)
            System.out.println(state);
    }
    void coin(int cash){
        if(state==STATES.WAIT){
            state=STATES.ACCEPT;
        }
        this.cash=cash+this.cash;
    }
    void choice(int choice){
        if(state==STATES.ACCEPT){
            this.choice=choice;
            state=STATES.CHECK;
        }
    }
    void check(){
        if(cash>=prices[choice])
            state=STATES.COOK;
    }
    void cook(){
        if(state==STATES.COOK)
            finish();
    }
    void finish(){
        state=STATES.WAIT;
        cash=0;
        System.out.println("Please,get your coffee");
    }
    public String[] getMenu(){
        return menu;
    }
    public int getCash(){
        return cash;
    }
    public int[] getPrices(){
        return prices;
    }
    public int getChoice() { return choice; }
}

public class App {
    public static void main( String[] args ){
        String[] menu = new String[]
                {"Americano","Cappuccino","Latte","Espresso","Irish"};
        int[] prices={10,20,30,40,50};
        Automata one=new Automata(0,menu,prices);
        one.on();
        one.printMenu(menu.length);
        one.coin(10);
        one.coin(10);
        one.coin(10);
        one.choice(2);
        one.check();
        one.cook();




    }



}