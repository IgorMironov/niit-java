package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AutomataDemoGUI extends JFrame{
    Automata automata = new Automata();

    private JPanel panel1;
    private JPanel panel2;
    private JPanel panel3;
    private JPanel panel4;

    private JButton ONOFFButton;
    private JButton a5Button;
    private JButton a10Button;
    private JButton a50Button;
    private JProgressBar progressBar1;
    private JTextArea textArea1;                           // экран с выводом состояния
    private JTextArea textArea2;
    private JTextArea textArea3;
    private JTextArea textArea4;
    private JTextArea textArea5;
    private JTextArea textArea6;
    private JTextArea textArea7;
    private JTextArea textArea8;
    private JTextArea textArea9;
    private JTextArea textArea10;
    private JTextArea textArea11;
    private JTextArea textArea12;
    private JTextArea textArea13;
    private JButton CHOOSEButton;
    private JButton CHOOSEButton1;
    private JButton CHOOSEButton2;
    private JButton CHOOSEButton3;
    private JButton CHOOSEButton4;
    private JButton CHOOSEButton5;
    private JButton CHOOSEButton6;
    private JButton CHOOSEButton7;
    private JButton CHOOSEButton8;
    private JButton CHOOSEButton9;
    private JButton CHOOSEButton10;
    private JButton CHOOSEButton11;

    JTextArea[] arr=new JTextArea[] {textArea2,textArea3,textArea4,textArea5,textArea6,textArea7,
            textArea8,textArea9,textArea10,textArea11,textArea12,textArea13};

    public AutomataDemoGUI(){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        textArea1.setEditable(false);
        textArea1.setText(automata.printState());
        ONOFFButton.setBackground(Color.black);
        setContentPane(panel1);
        setSize(1000,700);
        setVisible(true);

        // включение выключение автомата
        ONOFFButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean powerOnOff;
                if (automata.getStatus() == "OFF") {
                    powerOnOff = true;
                    ONOFFButton.setBackground(Color.red);
                    ONOFFButton.update(ONOFFButton.getGraphics());
                }
                else {
                    powerOnOff = false;
                    ONOFFButton.setBackground(Color.black);
                    ONOFFButton.update(ONOFFButton.getGraphics());
                    // при выключении очищаем меню напитков
                    for (int i=0; i<12; i++){
                        arr[i].setText(null);
                    }
                    // обнуляем cash
                    automata.setCash(0);
                }
                automata.Power(powerOnOff);

                textArea1.setText(automata.printState());
                textArea1.update(textArea1.getGraphics());

                // после включения автомата переводим его через 1 сек в режим "Ожидания..."
                if (automata.getStatus() == "ON") {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    automata.Wait(true);
                    textArea1.setText(automata.printState());
                    textArea1.update(textArea1.getGraphics());

                    // заполнение меню - карточек напитков - идет загрузка из массива
                    for (int i=0; i<12; i++){
                        arr[i].setText(automata.menu[i]);
                        arr[i].append("\n" + automata.prices[i] + " руб.");
                    }
                }

                // после режима "Ожидания..." переводим его через 1 сек в режим "Приема денег..."
                if (automata.getStatus() == "WAIT") {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    automata.Accept(true);
                    textArea1.setText(automata.printState());
                    textArea1.update(textArea1.getGraphics());
                }
            }
        });

        // добавление на баланс 5 рублей
        a5Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(automata.getStatus() == "ACCEPT") {
                    int addCash = 5;
                    int cash = automata.coin(5);
                    printCoin(addCash, cash);
                }
            }
        });
        // добавление на баланс 10 рублей
        a10Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(automata.getStatus() == "ACCEPT") {
                    int addCash = 10;
                    int cash = automata.coin(10);
                    printCoin(addCash, cash);
                }
            }
        });
        // добавление на баланс 50 рублей
        a50Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(automata.getStatus() == "ACCEPT") {
                    int addCash = 50;
                    int cash = automata.coin(50);
                    printCoin(addCash, cash);
                }
            }
        });

        // создаем массив кнопок - выбора напитков именю
        final JButton[] arrButton = new JButton[]{CHOOSEButton,CHOOSEButton1,CHOOSEButton2,
                CHOOSEButton3,CHOOSEButton4,CHOOSEButton5,CHOOSEButton6,CHOOSEButton7,CHOOSEButton8,
                CHOOSEButton9,CHOOSEButton10,CHOOSEButton11};

        ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String nameButton = ((JButton)e.getSource()).getText();
                if(automata.getStatus() == "ACCEPT") {
                    switch (nameButton) {
                        case "CHOOSE 1":
                            choose(0);
                            break;
                        case "CHOOSE 2":
                            choose(1);
                            break;
                        case "CHOOSE 3":
                            choose(2);
                            break;
                        case "CHOOSE 4":
                            choose(3);
                            break;
                        case "CHOOSE 5":
                            choose(4);
                            break;
                        case "CHOOSE 6":
                            choose(5);
                            break;
                        case "CHOOSE 7":
                            choose(6);
                            break;
                        case "CHOOSE 8":
                            choose(7);
                            break;
                        case "CHOOSE 9":
                            choose(8);
                            break;
                        case "CHOOSE 10":
                            choose(9);
                            break;
                        case "CHOOSE 11":
                            choose(10);
                            break;
                        case "CHOOSE 12":
                            choose(11);
                            break;
                    }
                }
            }
        };

        for (int i = 0; i < 12; i++)
                arrButton[i].addActionListener(listener);
    }

    // вывод на эран пополнение и текущий баланс
    private void printCoin(int addCash, int cash){
        textArea1.setText("Внесено " + addCash + " руб.");
        textArea1.append("\nТекущий баланс " + cash + " руб");
        textArea1.append("\nДобавьте купюры или выберите напиток.");
    }

    // выбрав напиток проверяем баланс, если денег не достаточно просим добавить
    // если денег достаточно готовим напиток, отдаем сдачу, обнуляем баланс, выключаем
    private void choose(int menuItem){
        textArea1.append("\nВы выбрали " + automata.menu[menuItem] +
                " стоимостью " + automata.prices[menuItem] + " руб.");

        textArea1.update(textArea1.getGraphics());
        int cash = automata.choice(0);
        textArea1.append("\n" + automata.printState());
        textArea1.update(textArea1.getGraphics());

        int change = automata.check(cash);
        if (change >= 0) {
            textArea1.append("\n" + automata.printState());
            textArea1.update(textArea1.getGraphics());

            ThreadPB progressBar = new ThreadPB();
            progressBar.start();
            try {
                Thread.sleep(6000);
            } catch (InterruptedException n) {
            }

            textArea1.setText("\nВаш напиток готов.\nВаша сдача: " + change);
            textArea1.update(textArea1.getGraphics());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            // выключаем автомат
            ONOFFButton.setBackground(Color.black);
            ONOFFButton.update(ONOFFButton.getGraphics());
            // при выключении очищаем меню напитков
            for (int i = 0; i < 12; i++) {
                arr[i].setText(null);
            }
            // обнуляем cash
            automata.setCash(0);
            automata.Power(false);
            textArea1.setText(automata.printState());
            textArea1.update(textArea1.getGraphics());

        } else {
            textArea1.setText("Недостаточно средств.\nПополните баланс.");
            textArea1.update(textArea1.getGraphics());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            automata.Accept(true);
            textArea1.setText(automata.printState());
            textArea1.update(textArea1.getGraphics());
        }
    }

    // progressBar1
    class ThreadPB extends Thread {
        @Override
        public void run() {
            progressBar1.setStringPainted(true);
            progressBar1.setMinimum(0);
            progressBar1.setMaximum(100);

            for (int i = progressBar1.getMinimum(); progressBar1.getValue() < progressBar1.getMaximum(); i += 10) {
                try {
                    Thread.sleep(500);
                    progressBar1.setValue(i);
                    progressBar1.update(progressBar1.getGraphics());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            progressBar1.setValue(0);
            Thread.interrupted();
        }
    }
}
