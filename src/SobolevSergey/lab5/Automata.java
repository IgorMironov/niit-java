package GUI;

public class Automata
{
    // state - текущее состояние автомата;
    enum STATES { OFF, ON, WAIT, ACCEPT, CHECK, COOK }
    public STATES state;

    // cash - для хранения текущей суммы;
    int cash = 0;

    public void setCash(int cash) {this.cash = cash;}

    // конструктор
    // on() - включение автомата;
    public Automata () {
        this.cash = 0;
        this.state = STATES.OFF;
        printState();
    }

    public String getStatus(){return state.toString();}

    // coin() - занесение денег на счёт пользователем;
    public int coin(int cash){
        if (state == STATES.ACCEPT){
            return this.cash += cash;
        }else{
            this.state = STATES.OFF;
            return 0;
        }
    }

    // prices - массив цен напитков (соответствует массиву menu);
    int[] prices = new int[] {12,14,14,24,22,8,12,8,20,10,1,1};

    // printMenu() - отображение меню с напитками и ценами для пользователя;
    String[] menu = new String[] {"Эспрессо         ","Американо        ","Капучино         ","Мокачино         ",
            "Горячий шоколад  ",		"Чай              ","Бульон           ","Холодный чай     ","Кофе глиссе      ",
            "Молочный коктейль",		"Молоко           ","Лимон            "};

    int menuItem;
    // выбор напитка пользователем
    public int choice(int indexItem){
        if (state == STATES.ACCEPT){
            this.state = STATES.CHECK;
            this.menuItem = indexItem;
        }
       return cash;
    }

    // cancel() - отмена сеанса обслуживания пользователем;
    public void cancel (int cancelSession){
        if(cancelSession == 2){
            this.state = STATES.OFF;
        }
        else
            this.state = STATES.ACCEPT;
    }

    // check() - проверка наличия необходимой суммы;
    public int check(int balance){

        if(balance >= prices[menuItem]){
            Cook(true);
        }else{
            Wait(true);
        }
        return cash - prices[menuItem];
    }

    public void Power(boolean powerOnOff){
        state = powerOnOff == true ? STATES.ON : STATES.OFF;
    }

    public void Wait(boolean wait){
        state = wait == true ? STATES.WAIT : STATES.OFF;
    }

    public void Accept(boolean accept){
        state = accept == true ? STATES.ACCEPT : STATES.OFF;
    }

    public void Cook(boolean cook){
        state = cook == true ? STATES.COOK : STATES.OFF;
    }

    // printState() - отображение текущего состояния для пользователя;
    public String printState(){
        switch (state){
            case OFF:
                return "Автомат выключен.";
            case ON:
                return "Автомат включен.";
            case WAIT:
                return "Ожидание...";
            case ACCEPT:
                return "Прием денег...";
            case CHECK:
                return "Проверка наличности...";
            case COOK:
                return "Приготовление...";
        }
        return "-1";
    }
}
