package staff;

// Класс Employee
// Employee - работник. Основной родительский класс для всех разновидностей работников.
// Заработная плата работникам начисляется, исходя из должности, почасовой ставки и участию в проектах.

abstract class Employee {

    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

    // id - идентификационный номер.
    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // name - ФИО.
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // worktime - отработанное время.
    protected int worktime;

    public void addWorkTime(int hours) {
        this.worktime = hours;
    }

    // payment - заработная плата.
    protected int payment;

    public void setPayment(int payment) {
        this.payment = payment;
    }

    // расчет зарплаты
    public void calc(){}

    // вывод на экран таблицы результатов
    public void info(){
        System.out.printf("%-4s%-16s%-20s%-20s%n",id,"| "+name,"| "+getClass().getSimpleName(),"| "+ payment);
    }
}
