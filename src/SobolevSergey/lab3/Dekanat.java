package Dekanat;

import java.io.*;
import java.util.*;
import java.lang.*;
/**
 * Created by SKS on 02.04.2017.
 */

/*
* Разработать класс Dekanat
Примерный перечень полей:
Students - массив студентов
Groups - массив групп

Обеспечить класс следующими методами:
создание студентов на основе данных из файла
создание групп на основе данных из файла
добавление случайных оценок студентам
накопление статистики по успеваемости студентов и групп
перевод студентов из группы в группу
отчисление студентов за неуспеваемость
сохранение обновленных данных в файлах
инициация выборов старост в группах
вывод данных на консоль
Создать два файла с данными для групп и студентов (не менее 3 групп и 30 студентов).
Использовать эти файлы при формировании данных групп и студентов

* */

public class Dekanat {
    int M=100;
    Student [] students = new Student[M];                   //Students - массив студентов
    int countOfStudents = 0;                                //количество студентов в деканате
    int N=10;
    Group[] groups = new Group[N];                          //Groups - массив групп
    int countOfGroups = 0;                                  //количество групп в деканате

    public Dekanat(){

    }

    //Обеспечить класс следующими методами:
    //создание групп на основе данных из файла
    public void CreatingGroups(){
        try {
            File file = new File("src\\main\\java\\Dekanat\\student-group.txt");
            FileReader reader = new FileReader(file);
            BufferedReader breader = new BufferedReader(reader);
            String line;
            int i=0;
            recordHistory("Группы в деканате: ");
            while ((line = breader.readLine()) != null) {
                groups[i] = new Group(line);
                recordHistory(groups[i].getTitle());
                i++;
            }
            countOfGroups = i;
            recordHistory("Количество групп в деканате: " + countOfGroups);

            reader.close();
        }catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    //создание студентов на основе данных из файла
    public void CreatingStudent(){
        try {
            File file = new File("src\\main\\java\\Dekanat\\list-students.txt");
            FileReader reader = new FileReader(file);
            BufferedReader breader = new BufferedReader(reader);
            String line;
            int i=0;
            while ((line = breader.readLine()) != null) {
                String[] arr = line.split("\t");
                int ID = Integer.parseInt(arr[0]);
                String Fio = arr[1];
                String groupName = arr[2];
                students[i] = new Student(ID, Fio);                 //создаем студента
                countOfStudents++;
                for(int j=0; j<countOfGroups;j++){
                    if(groups[j].getTitle().equals(groupName)) {
                        students[i].setGroup(groups[j]);
                    }
                }
                i++;
            }
            reader.close();
        }catch(IOException ex) {
            recordHistory(ex.getMessage());
        }
    }

    //ЗАЧИСЛЕНИЕ В ГРУППЫ СТУДЕНТОВ
    public void addStudentsGroups(){
        recordHistory("\nЗАЧИСЛЕНИЕ В ГРУППЫ СТУДЕНТОВ");
        for(int j=0; j<countOfGroups; j++) {
            recordHistory("В группу " + groups[j].getTitle() + " зачисленны студенты: ");
            for(int i=0; students[i]!= null; i++){
                boolean temp;
                if( temp =  groups[j].equals(students[i].getGroup()) ){
                    students[i].setGroup(groups[j]);			        //назначение группы студенту
                    groups[j].addStudent(students[i]);                  //добавление студента в группу
                    recordHistory(students[i].getID() + " " + students[i].getFio()+ " " + groups[j].getTitle());
                }
            }
        }

        //ПРОВЕРКА ВСЕХ ЛИ СТУДЕНТОВ РАСПРЕДЕЛИЛИ ПО ГРУППАМ
        for(int i=0; students[i]!= null; i++){
            int studentWithoutGroup = 0;                                //студент с неправильной группой
            for(int j=0; j<countOfGroups; j++) {
                boolean temp;
                if( temp =  groups[j].equals(students[i].getGroup()) ){
                    j = countOfGroups;
                }else{
                    studentWithoutGroup++;
                }
            }
            if(studentWithoutGroup == countOfGroups){
                recordHistory("Студент " + students[i].getID() + " " + students[i].getFio()
                        + " никуда не зачислен! Проверьте в файле его группу! Либо создайте новые группы!");
            }
        }
    }

    //добавление случайных оценок студентам
    public void addRandomRatingsToTheStudents(){
        int countNum = 10;                                              //выставляем каждому студенту по 10 оценок
        for(int i=0; students[i]!= null; i++) {
            String temp="Студенту " + students[i].getID() + " " + students[i].getFio()+ " поставлена оценка ";
            for(int j=0; j<countNum; j++) {
                int mark = 2 + (int)(Math.random() * ((5 - 2) + 1));
                students[i].addMark(mark);
                temp =temp +(" " + students[i].Marks[j]);
            }
            recordHistory(temp + "\n");
        }
    }

    //накопление статистики по успеваемости студентов и групп
    public void generationStatisticsProgressStudentsGroups(){
        //СТАТИСТИКА по студенту, СРЕДНИЙ БАЛ СТУДЕНТА
        for(int i=0; students[i]!= null; i++) {
            double averageMark = students[i].CalculatingAverageRatingStudent();

            recordHistory("У студента " + students[i].getID() + " "
                    + students[i].getFio()+ " " +" средний бал " + averageMark);
        }

        //СТАТИСТИКА по группе, СРЕДНИЙ БАЛ ГРУППЫ
        for(int j=0; j<countOfGroups; j++) {
            double averageMarkGroup =groups[j].calculationAverageScoreGroup();

            recordHistory("У группы " + groups[j].getTitle()
                    + " средний бал " + averageMarkGroup);
        }
    }

    //перевод студентов из группы в группу
    public void transferStudentsFromGroup(){
        int transferStudent = (int)(Math.random() * countOfStudents);           // выбираем случайного студента
        Group fromGroupStudent = students[transferStudent].getGroup();          // узнаем в какой он сейчас группе
        int fromGroup = Arrays.asList(groups).indexOf(fromGroupStudent);        // определяем индекс этой группы в массиве

        int inGroup = 0;
        do {
            inGroup = (int)(Math.random() * countOfGroups);                     // выбираем в какую группу
        }while (inGroup == fromGroup);

        // добавляем студента в другую (новую для него) группу
        students[transferStudent].setGroup(groups[inGroup]);			        //назначение группы студенту
        groups[inGroup].addStudent(students[transferStudent]);                  //добавление студента в группу

        // удаляем студента из группы в которой он до этого был
        groups[fromGroup].deleteStudentFromGroup(students[transferStudent]);	 //удаление студента из группы

        recordHistory("Студент " + students[transferStudent].getID() + " " +students[transferStudent].getFio()
        + " был переведен из группы " + groups[fromGroup].getTitle() + " в группу " + groups[inGroup].getTitle());
    }

    //отчисление студентов за неуспеваемость
    public void expulsionStudentsAcademicFailure(){
        double passingGrade = 3.0;                                          // проходной бал 3, если меньше то отчисляем

        for(int i=0; students[i]!= null; i++) {
            double averageMark = students[i].CalculatingAverageRatingStudent();
            if(averageMark < passingGrade){
                Group groupStudent = students[i].getGroup();                    // определяем группу где он числится
                groupStudent.deleteStudentFromGroup(students[i]);	            // удаление его из этой группы

                recordHistory("Студент " + students[i].getID() + " " + students[i].getFio()
                        + "  средний бал " + averageMark + " отчислен ");
            }
        }
    }

    //сохранение обновленных данных в файлах
    public void savingUpdatedDataInFiles(){
        try(FileWriter file = new FileWriter("src\\main\\java\\Dekanat\\new-list-students.txt",false)) {
            for(int i=0; i<countOfGroups; i++){
                for (int j=0; j<groups[i].getNum();j++) {
                    file.write(groups[i].students[j].getID()+"\t"+groups[i].students[j].getFio()+"\t"+groups[i].getTitle()+"\r\n");
                }
            }
            recordHistory("Данные в файле студентов обновлены!");
        }
        catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
        catch(IOException ioex){System.out.println("Ошибка записи файла!");}
    }

    //инициация выборов старост в группах
    public void choiceLeaderGroup(){
        for(int i=0; i<countOfGroups; i++)
            recordHistory(groups[i].LeaderGroup());
    }

    // запись истории работы Dekanata в файл
    public void recordHistory(String record){
        try(FileWriter file = new FileWriter("src\\main\\java\\Dekanat\\history.txt",true)) {
            file.write(record +"\r\n");
            System.out.println("Данные в файле истории обновлены!");
        }
        catch(FileNotFoundException fnfex){System.out.println("Файл истории не найден!");}
        catch(IOException ioex){System.out.println("Ошибка записи файла истории!");}
    }

    //вывод данных на консоль
    public void consoleOutput(){
        try {
            File file = new File("src\\main\\java\\Dekanat\\history.txt");
            FileReader reader = new FileReader(file);
            BufferedReader breader = new BufferedReader(reader);
            String line;
            while ((line = breader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
        }catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
