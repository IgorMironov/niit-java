package Network;

import javax.swing.*;

public class ClientGUI extends JFrame implements Runnable{
    Client new_client = new Client();

    private JPanel panel1;
    private JPanel panel2;
    private JPanel panel3;
    private JPanel panel4;
    private JPanel panel5;
    private JPanel panel6;
    private JPanel panel7;
    private JPanel panel8;
    private JPanel panel9;
    private JPanel panel10;
    private JPanel panel11;
    private JPanel panel12;

    private JTextArea textArea1;
    private JTextArea textArea2;
    private JTextArea textArea3;
    private JTextArea textArea4;

    public ClientGUI() {
        (new Thread(this)).start();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(panel1);
        setSize(800,600);
        setVisible(true);
    }

    public void run() {
        for(;;) {
            textArea1.setText(new_client.print_time());
            textArea2.setText(new_client.random_aphorism());
            textArea3.setText(new_client.event_messages());
            textArea4.setText(new_client.currency_rate());
        }
    }
}
