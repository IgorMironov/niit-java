import mypackage.*;
import java.io.*;

/**
 * Created by me on 4/18/17.
 */
public class StaffDemo
{
    public static Employee[] parseEmployeesFile(String file)
    {
        Employee retVal[];
        try
        {
            File f = new File(file);
            FileReader fr = new FileReader(f);
            BufferedReader bufferedReader = new BufferedReader(fr);
            String buf;
            String parsedBuf[];
            bufferedReader.mark((int)f.getUsableSpace());
            retVal = new Employee[(int) bufferedReader.lines().count()];

            bufferedReader.reset();
            int i = 0;
            while ((buf = bufferedReader.readLine()) != null)
            {
                //System.out.println("debug!!!!!!!!!!!!!!");
                parsedBuf = buf.split(",");
                switch (parsedBuf[2].toString())
                {
                    case "Driver":
                        retVal[i] = new Driver(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]));
                        break;
                    case "Cleaner":
                        retVal[i] = new Cleaner(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]));
                        break;
                    case "Programmer":
                        retVal[i] = new Programmer(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]),
                                1);
                        break;
                    case "Tester":
                        retVal[i] = new Tester(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]),
                                1);
                        break;
                    case "TeamLeader":
                        retVal[i] = new TeamLeader(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]),
                                2,
                                7);
                        break;
                    case "ProjectManager":
                        retVal[i] = new ProjectManager(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1]);
                        break;
                    case "SeniorManager":
                        retVal[i] = new SeniorManager(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1]);
                        break;
                    default:
                        throw new IOException("Wrong file!");
                }
                i++;
            }
            return retVal;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String args[])
    {
        WorkProject wp = new WorkProject("BuyKonur", 100000, 4);
        Employee arr[] = parseEmployeesFile("staff.csv");
        for (Employee e : arr)
            wp.addEmployee(e);
        wp.printSalary();
    }
}