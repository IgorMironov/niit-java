//Найти наибольшую последовательность Коллатца для чисел в диапазоне от 1 до 1 000 000. 
public class Collatz1 
{
    private static long collatz_(long n, int seq_len) 
    {
        if (n == 1) return seq_len;
        else if (n % 2 == 0) return collatz_(n / 2, seq_len + 1);
        else return collatz_(3*n + 1, seq_len + 1);
    }
    
    public static long collatz(long n)
    {
        return collatz_(n, 1);
    }
    
    public static void main(String[] args)
    {
        long cur_seq_len = 0;
        long max_seq_len = 0;
        long max_n = 0;
        long i = 1000000;

        while(i > 1)
        {
            if((cur_seq_len = collatz(i)) > max_seq_len)
            {
                max_seq_len = cur_seq_len;
                max_n = i;
            }
            i--;
        }
        System.out.println("Max Collatz sequence has " + max_seq_len + " members, n = " + max_n);
    }
}


