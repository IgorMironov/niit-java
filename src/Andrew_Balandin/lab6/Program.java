import javafx.application.Application;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


class GlobalVars
{
    static final String REQUEST_TIME = "REQUEST_TIME";
    static final String REQUEST_APH = "REQUEST_APH";
    final static String REQUEST_END_CON = "REQUEST_END_CON";
    final static String REQUEST_SET_CLIENT_ID = "REQUEST_SET_CLIENT_ID";
    final static String ANSWER_TIME = "TIME_ON_SERVER:";
    final static String ANSWER_APH = "ANSWER_APH:";
    final static String ANSWER_MESSAGE = "ANSWER_MESSAGE:";
    static int port = 1235;
}


class Server extends Thread
{
    private Timer timerForMessages  = new Timer();
    private TimerTask taskForMessages;
    private Integer clientId;
    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;

    public Server(Socket s) throws Exception
    {
        socket = s;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }

    void setClientId(Integer id)
    {
        clientId = id;
    }

    String getDateTime(String format)
    {
        java.util.Date date = new java.util.Date();
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    void sendString(String msg) throws Exception
    {
        System.out.println("Server send > " + msg);
        out.write(msg);
        out.newLine();
        out.flush();
    }

    String getRandomAphorism() throws Exception
    {
        Random rnd = new Random(System.nanoTime());
        File file = new File("file.txt");
        BufferedReader fromFile = new BufferedReader(new FileReader(file));
        ArrayList<String> aphorisms = new ArrayList<>();
        fromFile.lines().forEach(s -> aphorisms.add(s));
        fromFile.close();
        return aphorisms.get(rnd.nextInt(aphorisms.size()));
    }


    boolean hasAction() throws Exception
    {
        String str = in.readLine();
        if(str.equals(GlobalVars.REQUEST_END_CON))
            return false;
        if(str.equals(GlobalVars.REQUEST_TIME))
            sendString(GlobalVars.ANSWER_TIME + getDateTime("yyyy/MM/dd HH:mm:ss"));
        if(str.equals(GlobalVars.REQUEST_APH))
            sendString(GlobalVars.ANSWER_APH + getRandomAphorism());
        if(str.startsWith(GlobalVars.REQUEST_SET_CLIENT_ID))
        {
            setClientId(Integer.parseInt(str.replace(GlobalVars.REQUEST_SET_CLIENT_ID,"")));
            runTimerForMessages();
        }

        /*System.out.println("--------------");
        System.out.println(str);
        System.out.println("client id: " + clientId);*/
        return true;
    }

    private void runTimerForMessages()
    {
        MySqlSet mySqlSet = new MySqlSet(clientId);
        taskForMessages = new TimerTask()
        {
            @Override
            public void run()
            {
                try
                {
                    Integer hour = LocalDateTime.now().getHour();
                    Integer minute = LocalDateTime.now().getMinute();

                    System.out.println("find event");
                    mySqlSet.list.stream().forEach(e->{
                        if(e.hour == hour && (e.minute >= minute && e.minute <= (minute + 1)))
                        {
                            try
                            {
                                sendString(GlobalVars.ANSWER_MESSAGE +
                                        e.message + " at " + e.hour + ":" + e.minute);
                            } catch (Exception e1)
                            {
                                //System.out.println("!!!!!!!!!");
                                e1.printStackTrace();
                            }
                        }
                    });
                }
                catch (Exception e)
                {e.printStackTrace();}
            }

        };
        timerForMessages.scheduleAtFixedRate (taskForMessages , 1000, 55000);
        taskForMessages.run();
    }

    public void run()
    {
        try
        {
            System.out.println(Thread.activeCount() + " active thread(s)!");
            while(hasAction());
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            try
            {
                timerForMessages.cancel();
                socket.close();
                System.out.println("End connection! " + socket.getInetAddress());
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    }
}

class RunnableServer
{
    public RunnableServer() throws Exception
    {
        ServerSocket serverSocket = new ServerSocket(GlobalVars.port);
        System.out.println("Server started!");
        while(true)
        {
            Socket socket = serverSocket.accept();
            System.out.println("Connection was set: " + socket.getInetAddress());
            new Server(socket);
        }
    }
}

class Client
{

    private BufferedReader in;
    private BufferedWriter out;
    private Socket serverSocket;
    private Integer id;
    //private String fromServer = null;

    public Client(String url, Integer id) throws Exception
    {
        this.id = id;
        System.out.println("Client started with id: " + this.id);
        serverSocket = new Socket(url, GlobalVars.port);
        System.out.println("Connection was set: " + url);
        in = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(serverSocket.getOutputStream()));
        //sendString();
    }

    public void disconnect() throws Exception
    {
        sendString(GlobalVars.REQUEST_END_CON);
        out.close();
        in.close();
        serverSocket.close();
    }

    public void sendString(String msg) throws Exception
    {
        out.write(msg);
        out.newLine();
        out.flush();
    }
    public String getStringFromServer()
    {
        try
        {
            if (in.ready())
                return in.readLine();
        }
        catch (Exception e)
        {}
        return null;
    }

    public Integer getId()
    {
        return id;
    }
}

public class Program
{
    public static void main(String[] args) throws Exception
    {
        if(args.length != 0)
            Application.launch(GUI.class, args);
        else
            new RunnableServer();
    }
}

