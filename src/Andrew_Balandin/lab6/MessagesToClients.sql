CREATE SCHEMA message_for_clients;
USE message_for_clients;

CREATE TABLE messages(id INT NOT NULL, message_time TIME NOT NULL, 
                      message_txt VARCHAR(128) NOT NULL);
INSERT INTO messages (id, message_time, message_txt)
VALUES(3, '18:10', 'my message for 3'),
      (1, '15:01', 'my message for 1'),
      (1, '15:02', 'my message for 1'),
      (2, '15:00', 'my message for 2'),
      (3, '18:12', 'my message for 3'),
      (4, '15:00', 'my message for 4'),
      (5, '15:00', 'my message for 5');
      
CREATE USER 'new_user'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON * TO 'new_user'@'localhost';