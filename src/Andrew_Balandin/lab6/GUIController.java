import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;

import java.util.Timer;
import java.util.TimerTask;


public class GUIController
{
    private Timer timerListener;
    static TimerTask taskListener;

    private Timer timerTimeRequest;
    static TimerTask taskTimeRequest;

    @FXML
    private TextField txtUSD;
    @FXML
    private TextField txtRUB;
    @FXML
    private TextField txtMessage;
    @FXML
    private TabPane tabPane;
    @FXML
    private Button btnAph;
    @FXML
    private TextField txtTime;
    @FXML
    private TextField txtAph;

    private void runRequestTime() throws Exception
    {
        timerTimeRequest  = new Timer();
        taskTimeRequest = new TimerTask()
        {
            @Override
            public void run()
            {
                try
                {
                    GUI.client.sendString(GlobalVars.REQUEST_TIME);
                }
                catch (Exception e)
                {e.printStackTrace();}
            }

        };
        timerTimeRequest.scheduleAtFixedRate (taskTimeRequest ,1000,1000);
        taskTimeRequest.run();
    }


    @FXML
    private void requestAph() throws Exception
    {

        GUI.client.sendString(GlobalVars.REQUEST_APH);
    }

    public void initialize() throws Exception
    {
        GUI.client.sendString(GlobalVars.REQUEST_SET_CLIENT_ID + GUI.client.getId());
        runTimerListener();
        runRequestTime();
    }

    private void runTimerListener()
    {
        timerListener = new Timer();
        taskListener = new TimerTask()
        {
            @Override
            public void run()
            {
                try
                {
                    String str = GUI.client.getStringFromServer();
                    if(str == null)
                        return;
                    if (str.startsWith(GlobalVars.ANSWER_TIME))
                        txtTime.setText(str.replace(GlobalVars.ANSWER_TIME, ""));
                    if(str.startsWith(GlobalVars.ANSWER_APH))
                        txtAph.setText(str.replace(GlobalVars.ANSWER_APH, ""));
                    if(str.startsWith(GlobalVars.ANSWER_MESSAGE))
                        txtMessage.setText(str.replace(GlobalVars.ANSWER_MESSAGE, ""));
                }
                catch(Exception e)
                {e.printStackTrace();}
            }
        };
        timerListener.scheduleAtFixedRate(taskListener, 1000, 1000);
        taskListener.run();
    }

    @FXML
    private void currencyConvert()
    {
        Double rub = Double.parseDouble(txtUSD.getText()) * Double.parseDouble((new CurrencyConverter()).getCurrency());
        txtRUB.setText(rub.toString());
    }
}
