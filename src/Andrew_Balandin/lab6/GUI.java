import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class GUI extends Application
{

    public static Client client;
    @Override
    public void start(Stage stage) throws Exception
    {
        client = new Client(getParameters().getUnnamed().get(0),
                Integer.parseInt(getParameters().getUnnamed().get(1)));
        VBox root = FXMLLoader.load(getClass().getResource("time_aph.fxml"));
        //root.getChildren().
        stage.setTitle("Client");
        stage.setScene(new Scene(root));
        getParameters().getUnnamed().get(1);
        //GUIController.taskListener.run();

        stage.show();
    }

    @Override
    public void stop() throws Exception
    {
        try
        {
            GUIController.taskTimeRequest.cancel();
        }
        catch(Exception e)
        {}
        try
        {
            GUIController.taskListener.cancel();
        }
        catch(Exception e)
        {}
        client.disconnect();
        System.exit(0);
    }

}
