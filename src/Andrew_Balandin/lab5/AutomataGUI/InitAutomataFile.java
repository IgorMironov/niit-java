import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
/**
 * Created by me on 5/8/17.
 */
public class InitAutomataFile
{
    private String[] menu;
    private int[] prices;
    private int[] moneyCash;
    private int[] coins;

    InitAutomataFile(File fXmlFile) throws Exception
    {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        NodeList nListMenu = doc.getElementsByTagName("item");
        NodeList nListMoneyCash = doc.getElementsByTagName("money");
        NodeList nListCoins = doc.getElementsByTagName("coins");
        Node nNode;
        Element eElement;
        menu = new String[nListMenu.getLength()];
        prices = new int[nListMenu.getLength()];

        //get menu
        for (int i = 0; i < nListMenu.getLength(); i++)
        {
            nNode = nListMenu.item(i);
            //System.out.println("\nCurrent Element :" + nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE)
            {
                eElement = (Element) nNode;
                menu[i] = eElement.getElementsByTagName("drink").item(0).getTextContent();
                prices[i] = Integer.parseInt(eElement.getElementsByTagName("price").item(0).getTextContent());
                //System.out.println("drink : " + eElement.getElementsByTagName("drink").item(0).getTextContent());
                //System.out.println("price : " + eElement.getElementsByTagName("price").item(0).getTextContent());
            }
        }

        nNode = nListMoneyCash.item(0);

        //get moneyCash
        if (nNode.getNodeType() == Node.ELEMENT_NODE)
        {
            eElement = (Element) nNode;
            moneyCash = new int[eElement.getElementsByTagName("m").getLength()];
            for (int i = 0; i < eElement.getElementsByTagName("m").getLength(); i++)
            {
                moneyCash[i] = Integer.parseInt(eElement.getElementsByTagName("m").item(i).getTextContent());
                //System.out.println("cash : " + eElement.getElementsByTagName("m").item(i).getTextContent());
            }
        }

        nNode = nListCoins.item(0);

        //get coins
        if (nNode.getNodeType() == Node.ELEMENT_NODE)
        {
            eElement = (Element) nNode;
            coins = new int[eElement.getElementsByTagName("c").getLength()];
            for (int i = 0; i < eElement.getElementsByTagName("c").getLength(); i++)
            {
                coins[i] = Integer.parseInt(eElement.getElementsByTagName("c").item(i).getTextContent());
                //System.out.println("coin : " + eElement.getElementsByTagName("c").item(i).getTextContent());
            }
        }
    }

    public int[] getPrices()
    {
        return prices;
    }

    public String[] getMenu()
    {
        return menu;
    }

    public int[] getMoneyCash()
    {
        return moneyCash;
    }

    public int[] getCoins()
    {
        return coins;
    }
}