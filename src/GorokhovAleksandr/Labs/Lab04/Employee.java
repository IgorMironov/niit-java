public abstract class Employee {
	protected int id;
	protected String name;		
	protected int worktime;
	protected double payment;
	
	public Employee (int id, String name) {
		this.id = id;
		this.name = name;		
	}	
	
	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
	
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	
	public void addWorkTime(int hours) {
        this.worktime = hours;
    }
	
	public void setPayment(double payment) {
        this.payment = payment;
    }
	
	public void Calc(){}

	public void info() {
		//System.out.println(id + " - " + name);
		System.out.printf("%-4s%-16s%-20s%-20s%n", id, name, getClass().getSimpleName(), payment);
	}	
}