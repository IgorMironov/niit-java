public class Personal  extends Employee implements WorkTime {	
	int base;		
	public Personal(int id, String name, int base) {
		super(id, name);		
		this.base = base;		
	}	
	@Override
	public double CalcWorkTime(int Hour, int base) {
		double worktime = Hour * base;
		return worktime;
	}	
	public void Calc(){
            setPayment(CalcWorkTime(worktime,base));
    }	
}

final class Cleaner extends Personal {
    public Cleaner(int id, String name, int base) {
        super(id, name, base);
    }
}

final class Driver extends Personal{
    public Driver(int id, String name, int base) {
        super(id, name, base);
    }
}