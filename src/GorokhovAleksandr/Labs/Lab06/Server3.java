import java.io.*;
import java.net.*;
import java.net.URL;
import java.util.Date;
import java.util.Random;

class ServerOne extends Thread {
   private Socket socket;
   private BufferedReader in;
   private PrintWriter out;
   private Date date;
   private Random random;
   private int NumberAph;
   static String[] Aphorisms = new String[150];
   
   static void AphorismReader() {	    
	    try {
			File file = new File("Aphorism.txt");
			FileReader reader = new FileReader(file);
			BufferedReader breader = new BufferedReader(reader);
			String line;
			int count = 0;
			while ((line=breader.readLine()) != null) {
				Aphorisms[count]=line;
				count++;
			}
			reader.close();
		}
		catch (IOException e1) {
			System.err.println("������ ������/������ �����");
		}
    }
	
	static String Rate(String from, String to) throws IOException {
        //String from = "USD";
        //String to = "RUB";

        java.net.URL url = new java.net.URL(
           "http://www.webservicex.net/CurrencyConvertor.asmx"
                        + "/ConversionRate?FromCurrency=" + from
                        + "&ToCurrency=" + to);
        java.util.Scanner sc = new java.util.Scanner(url.openStream());

        // <?xml version="1.0" encoding="utf-8"?>
        sc.nextLine();

        // <double xmlns="http://www.webserviceX.NET/">0.724</double>
        String str = sc.nextLine().replaceAll("^.*>(.*)<.*$", "$1");

        sc.close();

        //Double rate = Double.parseDouble(str);
        //System.out.println(rate);
		return str;
    }
   
   public ServerOne(Socket s) throws IOException {
      socket = s;
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
      start(); 
   }
   
   public void run() {
	  //date = new Date();
	  AphorismReader();
      try {
         while (true) {
            String str = in.readLine();
			
			/*switch (str) {
				case "END":
					break;
				case "DATE": 
					date = new Date();
					out.println(date.toString());
					break;
				
				case "APHORISM": 
					random = new Random();
					NumberAph = random.nextInt(100);
					System.out.println(Aphorisms[NumberAph]);
					out.println(Aphorisms[NumberAph]);
					break;				
				default:
					out.println(str);
					
			}*/
            if (str.equals("END"))
               break;			
			else if (str.equals("DATE")) {
				date = new Date();
				out.println(date.toString());
			}
			else if (str.equals("APHORISM")) {
				random = new Random();
				NumberAph = random.nextInt(100);				
				out.println(Aphorisms[NumberAph]);				
			}
			else if (str.equals("RATE")) {
				//out.println("FROM:");
				//String from = in.readLine();
				//out.println("TO:");
				//String to = in.readLine();
				String from = "USD";
				String to = "RUB";
				out.println(Rate(from,to));
			}
            else 
				out.println(str);	
				
			System.out.println("��������: " + str);				
         
		 }
		 
         System.out.println("���������� �������");
      }
      catch (IOException e) {
         System.err.println("������ ������/������");
      }
      finally {
         try {
            socket.close();
         }
         catch (IOException e) {
            System.err.println("����� �� ������");
         }
      }
   }
   
   
}

public class Server3 {
    static final int PORT = 1234;
	
         
   public static void main(String[] args) throws IOException {
	  
      ServerSocket s = new ServerSocket(PORT);
      System.out.println("�������������� ������ ���������");
      try {
         while (true) {
            Socket socket = s.accept();
            try {
               System.out.println("����� ���������� �����������");
               System.out.println("������ �������: "+
                  socket.getInetAddress());
               new ServerOne(socket);
            }
            catch (IOException e) {
               socket.close();
            }
         }
      }
      finally {
         s.close();
      }
   }
} 