enum STATES	{
		ON(1), 
		OFF(2), 
		WAIT(3), 
		ACCEPT(4),
		CHECK(5),
		COOK(6);
		final int number;
		STATES(int number)
		{
		   this.number=number;
		}

		public int get() 
		{
		   return number;
		}
	}	


class Automata {
		
	int cash = 0;
	String[] menu = {"1. Cappuccino","2. Espresso", "3. Americano", "4. Latte", "5. Mocha", "6. Hot chocolate", "7. Double chocolate", "8. Double espresso", "9. Ristretto", "10. Double cappuccino"};
	int[] prices = {30,25,20,25,25,30,35,30,30,35};
	int drink;
	STATES state = STATES.OFF;			
	
	void on() {state = STATES.WAIT;}		
		
	void off() {state = STATES.OFF;}
	
	void coin(int nextcoin) {					
		cash += nextcoin;
		state = STATES.ACCEPT;
	}
	
	void printMenu() {
		System.out.println ("\n�������:");
		for (int i = 0; i<menu.length; i++ ) {	
			System.out.println(menu[i] + " - " + prices[i] + " ������");
		}
		System.out.println();
	}
	STATES getState() {return state;}
	
	//void printState() {
	//	System.out.println ("������� ���������: " + state);
	//}
	
	void choice(int n) {drink = n;}
	
	boolean check() {
		state = STATES.CHECK;
		if (cash>=prices[drink]) {			
			return true;}
		else {
			state = STATES.WAIT;
			return false;}
			
	}
	void cancel() {state = STATES.OFF;}
	void cook() {		 
			state = STATES.COOK;			
		}
	void finish() {state = STATES.WAIT;}
	
	int money() {return cash-prices[drink];}
}

public class AutomataDemo {
	public static void main(String[] args) {
		Automata Demo1 = new Automata();
		System.out.println ("������� ���������: " + Demo1.getState());		
		Demo1.on();
		System.out.println ("���������...");
		System.out.println ("������� ���������: " + Demo1.getState());
		Demo1.printMenu();
		Demo1.coin(10);
		System.out.println ("������� 10 ������. �����: " + Demo1.cash + " ������");		
		System.out.println ("������� ���������: " + Demo1.getState());
		Demo1.coin(10);
		System.out.println ("������� 10 ������. �����: " + Demo1.cash + " ������");		
		System.out.println ("������� ���������: " + Demo1.getState());
		Demo1.coin(5);
		System.out.println ("������� 5 ������. �����: " + Demo1.cash + " ������");		
		System.out.println ("������� ���������: " + Demo1.getState());
		Demo1.choice(5);		
		System.out.println ("������ ������� �6");		
		if (Demo1.check()==true) {
			System.out.println ("������� ���������: " + Demo1.getState());
			Demo1.cook();
			System.out.println ("������� ���������: " + Demo1.getState());
			System.out.println ("������� ���������!");
			Demo1.finish();
			System.out.println ("������� ���������: " + Demo1.getState());			
		}
		else { 
			System.out.println ("������������ �������! �������� ������ ������� ��� �������� ������.");
			System.out.println ("������� ���������: " + Demo1.getState());}
		Demo1.choice(2);
		System.out.println ("������ ������� �3");
			
		if (Demo1.check()==true) {
			System.out.println ("������� ���������: " + Demo1.getState());
			Demo1.cook();
			System.out.println ("������� ���������: " + Demo1.getState());
			System.out.println ("������� ���������...\n������!");
			Demo1.finish();
			System.out.println ("������� ���������: " + Demo1.getState());
						
		}
		else { 
			System.out.println ("������������ �������!");
			System.out.println ("������� ���������: " + Demo1.getState());}
		System.out.println ("���� �����: " + Demo1.money() + "������");
		Demo1.cancel();
		System.out.println ("����������...");
		System.out.println ("������� ���������: " + Demo1.getState());
		
	}	
}