package jevo;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alexander on 16.04.2017.
 */
public abstract class Employee {
    public static final int BASE = 900; //ставка за человека
    public static final int BASE_PROJECT = 4000; // бонус за участие в проекте
    private String projects;
    private int hours;
    private int rate;
    private String name;
    private String position;
    private int id;
    int count = 0;
    static Map<String, Integer> map = new HashMap<String, Integer>();
    static String[] proj = {"sky", "star", "space", "planet"};

    public void setProjects(String projects){
        this.projects = projects;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getHours() {
        return hours;
    }

    public int getRate() {
        return rate;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getProjects() {
        return projects;
    }

    public String getPosition() {
        return position;
    }

    public int healingPay() {
        int premium1=0; //  число подчиненных
        Set<Map.Entry<String, Integer>> set = map.entrySet();
        for (Map.Entry<String, Integer> me : set) {
            for(String pro : proj){
                //   System.out.println("Ключ:" + me.getKey().toString() + me.getValue() + "   Проект и Должность:" +ddd);
                if (me.getKey().toString().equals((pro + this.getPosition()).toString()) && this.getProjects().indexOf(pro)>0)
                    premium1 += me.getValue();
            }
        }
        return premium1 * BASE;
    }
    public int projectPay(){return 0;}

    abstract int buh();

    void setSub() {
        if (map.size() == 0) {
            for (String pro : proj) {
                map.put(pro + "TeamLeader", 0);
                map.put(pro + "ProjectManager", 0);
                map.put(pro + "SenjorManager", 0);
            }
        }
        for (String pro : proj) {
            if (this.getPosition().equals("Programmer") || this.getPosition().equals("Tester")) {
                //  System.out.println(projects);
                if (projects.indexOf(pro) > 0) {
                    count = map.get(pro+"TeamLeader");
                    map.put(pro+"TeamLeader", count + 1);
                }
            } else if (this.getPosition().equals("Manager")) {
                if (projects.indexOf(pro) > 0) {
                    count = map.get(pro+"ProjectManager");
                    map.put(pro+"ProjectManager", count + 1);
                    map.put(pro+"SenjorManager", count + 1);
                }
            } else if (this.getPosition().equals("ProjectManager")) {
                if (projects.indexOf(pro) > 0) {
                    count = map.get(pro+"SenjorManager");
                    map.put(pro+"SenjorManager", map.get(pro+"ProjectManager") + 1);
                }
            }
        }
    }
}
