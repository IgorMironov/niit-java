package jevo;

/**
 * Created by Alexander on 16.04.2017.
 */
public abstract class Personal extends Employee implements WorkTime {
    @Override
    public int workTimePay() {return this.getHours() * this.getRate();}
    public int buh(){return workTimePay();}
}

class Cleaner extends Personal {
    public Cleaner(){
        this.setRate(100);
    }

}

class Driver extends Personal{
    public Driver(){
        this.setRate(300);
    }
}

