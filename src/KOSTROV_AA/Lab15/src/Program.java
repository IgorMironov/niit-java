
import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Integer.parseInt;/**
 * Created by Alexander on 14.03.2017.
 */
public class Program {

    static String[] one ={
            "   *   ",
            "  **   ",
            " * *   ",
            "   *   ",
            "   *   ",
            "   *   ",
            " ***** "};
    static String[] two ={
            "  ***  ",
            " *   * ",
            "    *  ",
            "   *   ",
            "  *    ",
            " *     ",
            " ***** "};
    static String[] three ={
            "  ***  ",
            " *   * ",
            "    *  ",
            "   *   ",
            "    *  ",
            " *   * ",
            "  ***  "};
    static String[] four ={
            " *   * ",
            " *   * ",
            " *   * ",
            " ***** ",
            "     * ",
            "     * ",
            "     * "};
    static String[] five ={
            "  **** ",
            " *     ",
            " *     ",
            "  **** ",
            "      *",
            " *    *",
            "  **** "};
    static String[] six ={
            "  ***  ",
            " *   * ",
            " *     ",
            " ***** ",
            " *    *",
            " *    *",
            "  **** "};
    static String[] seven ={
            " ***** ",
            "     * ",
            "    *  ",
            "   *   ",
            "  *    ",
            "  *    ",
            "  *    "};
    static String[] eight ={
            "  ***  ",
            " *   * ",
            " *   * ",
            "  ***  ",
            " *   * ",
            " *   * " ,
            "  ***  "};
    static String[] nine ={
            "  ***  ",
            " *   * ",
            " *   * ",
            "  **** ",
            "     * ",
            " *   * ",
            "  ***  "};
    static String[] zero ={
            " ***** ",
            "*    **",
            "*   * *",
            "*  *  *",
            "* *   *",
            "**    *",
            " ***** "};
    public static void sw(char sym, int n, int h){
        switch (sym) {
            case '1':  System.out.print(one[h]);
                break;
            case '2':  System.out.print(two[h]);
                break;
            case '3':  System.out.print(three[h]);
                break;
            case '4':  System.out.print(four[h]);
                break;
            case '5':  System.out.print(five[h]);
                break;
            case '6':  System.out.print(six[h]);
                break;
            case '7':  System.out.print(seven[h]);
                break;
            case '8':  System.out.print(eight[h]);
                break;
            case '9':  System.out.print(nine[h]);
                break;
            case '0':  System.out.print(zero[h]);
                break;
            default: System.out.print(zero[h]);

        }
    }

    public static void main(String[] args){
        char [] cif = args[0].toCharArray();
        for (int h=0;h<7;h++) {
            for (int i = 0; i < cif.length; i++) {

                sw(cif[i], i, h);
            }
            System.out.println();
        }



    }
}
