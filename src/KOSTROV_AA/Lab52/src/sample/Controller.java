package sample;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.awt.*;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

public class Controller  {
    Automata auton = new Automata();
    @FXML
    Label lb_1;
    @FXML
    TextField tf_1;
    @FXML
    Button btn1;
    @FXML
    Button btn2;
    @FXML
    Button btn3;
    @FXML
    Button btn4;
    @FXML
    Button btn5;
    @FXML
    Button btn6;
    @FXML
    Polygon glass;




Task<Void> loadTask = new Task<Void>(){

    @Override
    protected Void call() throws Exception {
        String text = "";
        for (int i=0; i<15; i++) {

            Thread.sleep(1000);
            if (i<=3) {
                text = auton.getStatusS() + i;
              //  auton.check();
            }
            else if(i>3 && i<6) {
                auton.check();
                if (auton.getState().toString()=="COOK") {
                    text = "Ваш " + auton.getUserChoice() + " готов. Приходите ещё!";
                  //  glass
                }
                else
                    text = "Недостаточно средств";
            }
            else {
                auton.finish();
                text =auton.getStatusS();
            }
            String finalText = text;
            Platform.runLater(() ->  lb_1.setText(finalText));
        }


        return null;
    }

};
    Thread t = new Thread(loadTask);
    boolean suspended;

    public  void on()  {
        t.start();

    }

    public Controller(){
        auton.on();
    }

    @FXML
    public void  changeMoney(){
         lb_1.setText(auton.printState());
    }
    @FXML
    public void goAction1() throws InterruptedException{

        auton.coin(Integer.parseInt(tf_1.getText().toString()));
        auton.choice(0);
        t.setDaemon(true);
        this.on();

   }
    @FXML
    public void goAction2() throws InterruptedException{
        auton.coin(Integer.parseInt(tf_1.getText().toString()));
        auton.choice(1);
        this.on();

    }
    @FXML
    public void goAction3() throws InterruptedException{
        auton.coin(Integer.parseInt(tf_1.getText().toString()));
        auton.choice(2);
        this.on();
    }
    @FXML
    public void goAction4() throws InterruptedException{
        auton.coin(Integer.parseInt(tf_1.getText().toString()));
        auton.choice(3);
        this.on();
    }
    @FXML
    public void goAction5() throws InterruptedException{
        auton.coin(Integer.parseInt(tf_1.getText().toString()));
        auton.choice(4);
        this.on();
    }
    @FXML
    public void goAction6() throws InterruptedException{
        auton.coin(Integer.parseInt(tf_1.getText().toString()));
        auton.choice(5);
        this.on();
    }
}
