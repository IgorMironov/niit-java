package jevojava;

import org.junit.Test;

import static org.junit.Assert.*;



/**
 * Created by Alexander on 24.03.2017.
 */

public class AutomataTest {
    Automata aTest = new Automata();


    @Test
    public void on() throws Exception {
      String actual = "WAIT";
      aTest.on();
      String result = aTest.getState().toString();
      assertEquals("ТЕСТ на включение автомата провален", actual, result);
    }

    @Test
    public void off() throws Exception {
        String actual = "OFF";
        aTest.off();
        String result = aTest.getState().toString();
        assertEquals("ТЕСТ на выключение автомата провален", actual, result);

    }

    @Test
    public void coin() throws Exception {
        int actual = 100;
        aTest.coin(100);
        int result = aTest.getCash();
        assertEquals("Тест на внесенную сумму не пройден", actual, result);

        String actual2 = "ACCEPT";
        String result2 = aTest.getState().toString();
        assertEquals("ТЕСТ на внесение средств провален", actual2, result2);

    }

    @Test
    public void printMenu() throws Exception {

    }

    @Test
    public void printState() throws Exception {

    }

    @Test
    public void choice() throws Exception {
        aTest.index = 3;
        aTest.choice(aTest.index);
        String result = aTest.getUserChoice();
        String actual = "Глинтвейн";
        assertEquals("Тест на выбранный напиток не пройден", actual, result);

        String actual2 = "CHECK";
        String result2 = aTest.getState().toString();
        assertEquals("ТЕСТ на выбор напитка провален", actual2, result2);


    }

    @Test
    public void check() throws Exception {

    }

    @Test
    public void cancel() throws Exception {
        String actual = "WAIT";
        aTest.cancel();
        String result = aTest.getState().toString();
        assertEquals("ТЕСТ на отклонение обслуживание клиента провален", actual, result);
    }

    @Test
    public void cook() throws Exception {
        String actual = "COOK";
        aTest.cook();
        String result = aTest.getState().toString();
        assertEquals("ТЕСТ на приготовление напитка провален", actual, result);

    }

    @Test
    public void finish() throws Exception {
        String actual = "WAIT";
        aTest.finish();
        String result = aTest.getState().toString();
        assertEquals("ТЕСТ на окончание обслуживания провален", actual, result);

    }

}