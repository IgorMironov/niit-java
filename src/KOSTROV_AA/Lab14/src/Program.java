
import static java.lang.Integer.parseInt;

/**
 * Created by Alexander on 13.03.2017.
 */
public class Program {

    public static String go(String s){
        String itog="";
        String part ="";
        String[] split = s.split(",");

            for(int i=1;i<split.length;i++){
                if (parseInt(split[i]) == parseInt(split[i-1])+1 ){
                   if(part=="")
                   part+=split[i-1] + "-";
                }
                else {
                    if(part!=""){
                    itog+=part+split[i-1] + ",";
                    part = "";
                                }
                    else
                    itog+=split[i-1] + ",";
                }
                if (i+1==split.length)
                    itog+=part + split[i];
        }
              return itog;
    }
    public static void main(String[] args){
        System.out.println(go(args[0]));
    }
}

