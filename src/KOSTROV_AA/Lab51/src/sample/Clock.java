package sample;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.util.Duration;


import java.text.SimpleDateFormat;
import java.util.Date;

import static javafx.animation.Animation.INDEFINITE;


/**
 * Created by Alexander on 08.05.2017.
 */
public class Clock extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        final double X_CENTER = 175;
        final double Y_CENTER = 175;
        final double X_STR = 4;
        final double Y_STR = 150;
        int ugol = 0;
        Text[] text = new Text[12];

        BorderPane root = new BorderPane();
        VBox vbox = new VBox();
        primaryStage.setTitle("Часы 1.0 JavaFX");

        Rectangle[] zasekMini = new Rectangle[60];
        for (int i=0; i<zasekMini.length; i++){
            zasekMini[i] = new Rectangle(X_CENTER,Y_CENTER,12,1);
            zasekMini[i].setFill(Color.BLACK);
            ugol-=6;
            Rotate rotate = new Rotate(ugol,X_CENTER,Y_CENTER) ;
            Translate translate = new Translate() ;
            translate.setX(135) ;

            if (ugol%5==0) {
                rotate.setAngle(ugol-1) ; //хейдт толстеет в одну сторону, поэтому корректируем угол больших отсечек
                zasekMini[i].setHeight(6);
                zasekMini[i].setWidth(25);
                translate.setX(125) ;
            }
            //   translate.setY(150) ;
            zasekMini[i].getTransforms().addAll(rotate,translate);
        }
        ugol = -105;
        for (int i=11; i>=0; i--){
            text[i] = new Text(Integer.toString(i+1));


            text[i].setFill(Color.BLACK);
            text[i].setFont(Font.font("Verdana",FontWeight.NORMAL,26));
            ugol-=30;
            Rotate rotate = new Rotate(ugol,X_CENTER,Y_CENTER) ;
            Translate translate = new Translate() ;
            translate.setX(254) ;
            translate.setY(254) ;
            //   text[i].setRotate(-1*ugol);
            Rotate rotate2 = new Rotate(-1*ugol,text[i].getX(),text[i].getY()) ;

            Translate translate2 = new Translate() ;
            translate2.setX(-10) ;
            translate2.setY(10) ;
            text[i].getTransforms().addAll(rotate,translate,rotate2,translate2);

        }


        Image image = new Image("file:Dragon.jpg");
        ImageView iv = new ImageView();
        iv.setImage(image);
        iv.setOpacity(0.6);
        iv.toBack();
        iv.setX(X_CENTER-70);
        iv.setY(Y_CENTER-80);
        Circle c1 = new Circle(X_CENTER, Y_CENTER+1, 150);
        c1.setStroke(Color.BLACK);
        c1.setFill(null);
        c1.setStrokeWidth(7);

        Circle c2 = new Circle(X_CENTER, Y_CENTER+1, 135);
        c2.setStroke(Color.BLACK);
        c2.setFill(null);
        c2.setStrokeWidth(1);

        Circle c3 = new Circle(X_CENTER, Y_CENTER+1, 7);
        c3.setStroke(Color.RED);
        c3.setFill(Color.RED);
        c3.setStrokeWidth(7);


        Rectangle rect = new Rectangle(X_CENTER-32, Y_CENTER, 175, 1);
        rect.setArcHeight(15);
        rect.setArcWidth(15);
        rect.setFill(Color.RED);
        movePivot(rect, -55, 0);

        Rectangle rectM = new Rectangle(X_CENTER-35, Y_CENTER, 70, 1.8);
        rectM.setArcHeight(15);
        rectM.setArcWidth(15);
        rectM.setFill(Color.RED);
        movePivot(rectM, 0, 0);

        Rectangle rect2 = new Rectangle(X_CENTER, Y_CENTER, 150, 3);
        rect2.setArcHeight(50);
        rect2.setArcWidth(50);
        rect2.setFill(Color.GREEN);


        Polygon polygon2 = new Polygon();
        polygon2.getPoints().addAll(new Double[]{
                X_CENTER+X_STR, Y_CENTER,
                X_CENTER, Y_CENTER-120,
                X_CENTER-X_STR, Y_CENTER,
                X_CENTER, Y_CENTER+45,
                X_CENTER+X_STR, Y_CENTER});
        movePivot(polygon2, 0, 38);

        Rectangle rect3 = new Rectangle(X_CENTER, Y_CENTER, 150, 4);
        rect3.setArcHeight(50);
        rect3.setArcWidth(50);
        rect3.setFill(Color.GREEN);
        movePivot(rect3, -75, 0);

        Polygon polygon3 = new Polygon();
        polygon3.getPoints().addAll(new Double[]{
                X_CENTER+X_STR, Y_CENTER,
                X_CENTER, Y_CENTER-90,
                X_CENTER-X_STR, Y_CENTER,
                X_CENTER, Y_CENTER+45,
                X_CENTER+X_STR, Y_CENTER});
        movePivot(polygon3, 0, 24);

        Date d = new Date();
        SimpleDateFormat sek = new SimpleDateFormat("ss");
        SimpleDateFormat min = new SimpleDateFormat("mm");
        SimpleDateFormat hrs = new SimpleDateFormat("hh");

        RotateTransition rt = new RotateTransition(Duration.seconds(60),rect);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setFromAngle(Double.parseDouble(sek.format(d))*6 - 90 );
        rt.setToAngle(Double.parseDouble(sek.format(d))*6 +270);
        rt.setCycleCount(INDEFINITE);
        rt.play();

        RotateTransition rtM = new RotateTransition(Duration.seconds(60),rectM);
        rtM.setInterpolator(Interpolator.LINEAR);
        rtM.setFromAngle(Double.parseDouble(sek.format(d))*6 - 90 );
        rtM.setToAngle(Double.parseDouble(sek.format(d))*6 + 270);
        rtM.setCycleCount(INDEFINITE);
        rtM.play();

        RotateTransition rt2 = new RotateTransition(Duration.minutes(60),polygon2);
        rt2.setInterpolator(Interpolator.LINEAR);
        rt2.setFromAngle(Double.parseDouble(min.format(d))*6 + Double.parseDouble(sek.format(d))/10);
        rt2.setToAngle(Double.parseDouble(min.format(d))*6 + 360 + Double.parseDouble(sek.format(d))/10);
        rt2.setCycleCount(INDEFINITE);
        rt2.play();

        RotateTransition rt3 = new RotateTransition(Duration.hours(12),polygon3);
        rt3.setInterpolator(Interpolator.LINEAR);
        rt3.setFromAngle(Double.parseDouble(hrs.format(d))*30  + Double.parseDouble(min.format(d))*6/12+0.5);
        rt3.setToAngle(Double.parseDouble(hrs.format(d))*30 + 360 + Double.parseDouble(min.format(d))*6/12+0.5);
        rt3.setCycleCount(INDEFINITE);
        rt3.play();



        root.setCenter(vbox);
       // root.setTop(btn);

        root.getChildren().addAll(iv,rect,polygon3,rectM,c1,c2,polygon2,c3);
       // Scene scene = new Scene(root,300,200);
        for (int i=0;i<text.length;i++)
            root.getChildren().add(text[i]);
        for (int i=0;i<zasekMini.length;i++)
            root.getChildren().add(zasekMini[i]);

        primaryStage.setScene(new Scene(root,350,350));
        primaryStage.show();


    }

    private void movePivot(Node node, double x, double y) {
        node.getTransforms().add(new Translate(-x,-y));
        node.setTranslateX(x);
        node.setTranslateY(y);
    }

    public static void main(String[] args) {

        Clock.launch();
    }
}
