class Program4
{
	static String RangeRecord (int start_num, int count,String result_str)
	{
		switch (count){
			case 1:
			  break;
			case 2:
			  result_str = result_str +"," + (start_num + count-1);
			  break;
			default:
			  result_str = result_str +"-" + (start_num + count-1);
			}
		return result_str;
	}
	
	static void Scan(String str)
	{
		int start_num=0, num, count=0;
		String result_str="";
		for (String arr_str : str.split(",")) {
			num = Integer.parseInt(arr_str);
			if (count==0){
				start_num = num;
				count = 1;
				result_str = result_str + "result: " + num;
				continue;
				}
			if(num == (start_num + count)) count++;
			else {
				result_str = RangeRecord(start_num,count,result_str);
				result_str = result_str +"," + num;
				start_num = num;
				count = 1;
			}
		}
		result_str = RangeRecord(start_num,count,result_str);
		System.out.println(result_str);
	}
	
	public static void main(String[] args)
    {
		String str=args[0];
		System.out.println(str);
		Scan(str);
	}
}