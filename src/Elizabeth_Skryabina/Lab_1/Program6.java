public class Program6
{
	public static void main(String[] args)
	{
		CircleDemo();
		EarthAndRope();
		Pool();
	}
	
	static void CircleDemo()
	{
		Circle circle = new Circle();
		circle.SetRadius(4);
		System.out.println("R = "+ circle.GetRadius() +" => F = " + circle.GetFerence() + ", A = " + circle.GetArea());
		circle.SetFerence(207);
		System.out.println("F = "+ circle.GetFerence() +" => R = " + circle.GetRadius() + ", A = " + circle.GetArea());
		circle.SetArea(706);
		System.out.println("A = "+ circle.GetArea() +" => F = " + circle.GetRadius() + ", R = " + circle.GetFerence());
	}
	
	static void EarthAndRope()
	{
		System.out.println("\n*Earth and rope*");
		Circle earth = new Circle();
		Circle rope = new Circle();
		earth.SetRadius(6378.1*1000);
		rope.SetFerence(earth.GetFerence() + 1);
		double gap = Math.abs(earth.GetRadius()-rope.GetRadius());
		System.out.println("Gap = " + gap);
	}
	
	static void Pool()
	{
		System.out.println("\n*Pool*");
		Circle pool = new Circle();
		Circle pool_track = new Circle();
		pool.SetRadius(3);
		pool_track.SetRadius(pool.GetRadius()+1);
		double AreaTrack = Math.abs(pool.GetArea()-pool_track.GetArea());
		double Cost_Track = AreaTrack * 1000;
		double Cost_Fence = pool_track.GetFerence()*2000;
		System.out.printf("Cost = %.2f + %.2f = %.2f rub\n",Cost_Track,Cost_Fence,Cost_Track+Cost_Fence);
	}
}

class Circle
{
	private double Radius;
	private double Ference;
	private double Area;
	
	void SetRadius(double R){
		Radius=R;
		Ference = 2 * Math.PI * Radius;
		Area = Math.PI * Math.pow(Radius,2);
	}
	void SetFerence(double F){
		Ference=F;
		Radius = Ference / (2*Math.PI);
		Area = Math.PI * Math.pow(Radius,2);
	}
	void SetArea(double A){
		Area=A;
		Radius = Math.sqrt(Area/Math.PI);
		Ference = 2 * Math.PI * Radius;
	}
	
	double GetRadius(){
		return Radius;
	}
	double GetFerence(){
		return Ference;
	}
	double GetArea(){
		return Area;
	}
}