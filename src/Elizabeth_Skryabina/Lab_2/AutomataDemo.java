class AutomataDemo {
    public static void main (String args[]) {
        Automata automat = new Automata();
        automat.printmenu();
        automat.printState();

        automat.on();
        automat.printState();

        automat.coin(50);
        automat.printState();

        automat.givebackmoney();
        automat.printState();

        automat.coin(100);
        automat.printState();

        automat.choice("Чай");
        automat.printState();

        automat.off();
        automat.printState();
    }
}