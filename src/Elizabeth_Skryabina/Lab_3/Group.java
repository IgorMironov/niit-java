import java.math.BigDecimal;
import java.math.RoundingMode;

public class Group {
    private String Title; //- название группы
    Student students[] = new Student[25];//- массив из ссылок на студентов
    private int Num = 0; //- количество студентов в группе
    private Student Head; //- ссылка на старосту (из членов группы)

    public Group(String line) { //создание группы с указанием названия
        this.Title = line;
    }

    public String getTitle() {
        return Title;
    }

    //добавление студента
    void AddStudent(Student newStudent) {
        this.students[Num] = newStudent;
        this.Num++;
    }

    //избрание старосты
    void ElectionCaptain() {
        double Max = 0;
        for (int i = 0; i < this.Num; i++)
            if (Max < students[i].CalculatingAverageMarkStudent()) {
                Max = students[i].CalculatingAverageMarkStudent();
                this.Head = students[i];
            }
    }

    public Student getHead() {
        return Head;
    }

    //поиск студента по ФИО или ИД
    Student SearchStudentID(int searchID) {
        for (int i = 0; i < Num; i++)
            if (students[i].getID() == searchID) {
                return students[i];
            }
        return null;
    }

    Student SearchStudentFIO(String searchFIO) {
        for (int i = 0; i < Num; i++)
            if (students[i].getFIO().equals(searchFIO)) {
                return students[i];
            }
        return null;
    }

    //вычисление соеднего балла в группе
    double CalculatingAverageMarkGroup() {
        double summ = 0;
        for (int i = 0; i < this.Num; i++)
            summ += students[i].CalculatingAverageMarkStudent();
        double middle = summ / Num;
        return new BigDecimal(middle).setScale(3, RoundingMode.UP).doubleValue();
    }

    //исключение студента из группы
    void DeleteStudentFromGroup(Student delStudent) {
        int i;
        for (i = 0; i < Num; i++)
            if (this.students[i].equals(delStudent))
                break;
        for (int k = i; k < Num - 1; k++)
            this.students[k] = this.students[k + 1];
        students[Num] = null;

        if (delStudent.equals(Head)) ElectionCaptain();
        Num -= 1;
    }

    void print() {
        System.out.println("Group №" + this.Title);
        for (int j = 0; j < Num; j++) {
            students[j].printStudent();
            if (students[j].equals(Head)) System.out.println(" HEAD");
            else System.out.println();
        }
        System.out.println();
    }
}
