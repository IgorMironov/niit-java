import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Dekanat {
    final int NUMBER_OF_GROUPS = 3;
    int NUMBER_OF_STUDENTS = 25;
    Student Students[] = new Student[NUMBER_OF_STUDENTS]; //- массив студентов
    Group Groups[] = new Group[NUMBER_OF_GROUPS]; //- массив групп
    double StudentStatistics;
    double GroupStatistics = 0;

    //создание студентов на основе данных из файла
    public void CreateStudentsFromFile(String file) {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file), StandardCharsets.UTF_8))) {
            String str;
            int i = 0;
            while ((str = reader.readLine()) != null) {
                String record[] = str.split(":");
                Students[i] = new Student(Integer.parseInt(record[0]), record[1]);
                for (int j = 0; j < NUMBER_OF_GROUPS; j++)
                    if (record[2].equals(Groups[j].getTitle()))
                        Students[i].setGroup(Groups[j]);
                i += 1;
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    //создание групп на основе данных из файла
    public void CreateGroupsFromFile(String file) {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file), StandardCharsets.UTF_8))) {
            String str;
            int i = 0;
            while ((str = reader.readLine()) != null)
                Groups[i++] = new Group(str);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    //распределение студентов по группам
    void DistributionStudentsInGroups() {
        for (int i = 0; i < NUMBER_OF_GROUPS; i++)
            for (int j = 0; j < NUMBER_OF_STUDENTS; j++)
                if ((Students[j].getgroup()).equals(Groups[i]))
                    Groups[i].AddStudent(Students[j]);
    }

    //добавление случайных оценок студентам
    void AddRandomMarks() {
        //System.out.println("Добавление рандомных оценок: ");
        for (int j = 0; j < NUMBER_OF_STUDENTS; j++) {
            for (int i = 0; i < 10; i++)
                Students[j].AddMarks();
        }
    }

    //накопление статистики по успеваемости студентов и групп
    void Statistic() {
        for (int i = 0; i < NUMBER_OF_STUDENTS; i++) {
            StudentStatistics = Students[i].CalculatingAverageMarkStudent();
        }

        for (int i = 0; i < NUMBER_OF_GROUPS; i++) {
            GroupStatistics = Groups[i].CalculatingAverageMarkGroup();
        }
        System.out.println("\n*******************Начальное состояние групп************************");
        PrintAll();
    }

    //перевод студентов из группы в группу
    void TransferStudents() {
        System.out.println("*******************Перевод студентов************************");
        int NumRandomStudent = (int) (Math.random() * Students.length); //выбрали студента для перевода
        int OldGroup = Arrays.asList(Groups).indexOf(Students[NumRandomStudent].getgroup());
        int NumRandomGroup = OldGroup;
        while (OldGroup == NumRandomGroup)
            NumRandomGroup = (int) (Math.random() * Groups.length); //выбрали группу для перевода != группе студента;

        System.out.println("переводим " + Students[NumRandomStudent].getFIO() + " из " + Students[NumRandomStudent].group.getTitle() +
                " в " + Groups[NumRandomGroup].getTitle());
        Groups[OldGroup].DeleteStudentFromGroup(Students[NumRandomStudent]);
        Groups[NumRandomGroup].AddStudent(Students[NumRandomStudent]);
        Students[NumRandomStudent].setGroup(Groups[NumRandomGroup]);
        ElectionCaptains();
    }

    //отчисление студентов за неуспеваемость
    void DeductionStudentsForUnderachievement() {
        System.out.println("*******************Отчисление за неуспеваемость************************");
        int count = 0;
        for (int i = 0; i < NUMBER_OF_STUDENTS; i++) {
            if (Students[i].CalculatingAverageMarkStudent() < 3.5) {
                Group StudentGroup = Students[i].getgroup();
                StudentGroup.DeleteStudentFromGroup(Students[i]);
                count++;
            }
        }
        int NEW_NUMBER_OF_STUDENTS = NUMBER_OF_STUDENTS - count;
        Student new_Students[] = new Student[NEW_NUMBER_OF_STUDENTS];
        int s = 0;
        for (int i = 0; i < NUMBER_OF_STUDENTS; i++) {
            if (Students[i].CalculatingAverageMarkStudent() >= 3.5) {
                new_Students[s] = Students[i];
                s++;
            }
        }
        int i = 0;
        for (s = 0; s < NEW_NUMBER_OF_STUDENTS; s++) {
            Students[i] = new_Students[s];
            i++;
        }
        NUMBER_OF_STUDENTS = NEW_NUMBER_OF_STUDENTS;
        PrintAll();
    }

    //сохранение обновленных данных в файлах
    void SaveToFile(String file) {
        try (FileWriter writer = new FileWriter(file, false)) {
            for (int i = 0; i < NUMBER_OF_STUDENTS; i++) {
                writer.write(Students[i].group.getTitle() + "\t" +
                        Students[i].getID() + "\t" +
                        Students[i].getFIO() + " (" +
                        Students[i].CalculatingAverageMarkStudent() + ")");
                writer.append('\n');
            }
            writer.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    //инициация выборов старост в группах
    void ElectionCaptains() {
        for (int i = 0; i < NUMBER_OF_GROUPS; i++) {
            Groups[i].ElectionCaptain();
            System.out.println("в группе " + Groups[i].getTitle() + " староста - " + Groups[i].getHead().getFIO());
        }
    }

    //поиск студентов
    void SearchSomeStudents() {
        System.out.println("*******************Поиск студентов по ID и ФИО************************");
        Student SearchStudent;
        int SearchID = 5;
        boolean flag = false;
        for (int i = 0; i < NUMBER_OF_GROUPS; i++) {
            SearchStudent = Groups[i].SearchStudentID(SearchID);
            if (SearchStudent != null) {
                flag = true;
                System.out.println("Student with ID " + SearchID + " - " + SearchStudent.getFIO() + " in group " + SearchStudent.group.getTitle());
            }
        }
        if (!flag)
            System.out.println("Sorry! no student with ID " + SearchID);

        flag = false;
        String SearchFIO = "Torkunova NV";
        for (int i = 0; i < NUMBER_OF_GROUPS; i++) {
            SearchStudent = Groups[i].SearchStudentFIO(SearchFIO);
            if (SearchStudent != null) {
                flag = true;
                System.out.println("Student with FIO " + SearchFIO + " - in group " + SearchStudent.group.getTitle());
            }
        }
        if (!flag)
            System.out.println("Sorry! no student with FIO " + SearchFIO);
    }

    //вывод данных на консоль
    void PrintAll() {
        for (int i = 0; i < NUMBER_OF_GROUPS; i++)
            Groups[i].print();

    }
}
