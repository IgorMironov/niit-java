package StaffDemo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Company {
    ArrayList<Cleaner> cleaners = new ArrayList<Cleaner>();
    ArrayList<Driver> drivers = new ArrayList<Driver>();
    ArrayList<Programmer> programmers = new ArrayList<Programmer>();
    ArrayList<Teaster> teasters = new ArrayList<Teaster>();
    ArrayList<TeamLeader> team_leaders = new ArrayList<TeamLeader>();
    ArrayList<ProjectManager> project_managers = new ArrayList<ProjectManager>();
    ArrayList<SeniorManager> senior_managers = new ArrayList<SeniorManager>();
    ArrayList<ListProject> projects = new ArrayList<ListProject>();

    //базовые ставки за час
    final int BASE_PROGRAMMER = 350;
    final int BASE_TEAMLEADER = 400;
    final int BASE_TEASTER = 300;
    final int BASE_CLEANER = 90;
    final int BASE_DRIVER = 200;

    //коэфф. вклада в проект
    final double CONTRIBUTION_PROGRAMMER = 0.56;
    final double CONTRIBUTION_TEAMLEADER = 0.06;
    final double CONTRIBUTION_TEASTER = 0.18;
    final double CONTRIBUTION_PR_MANAGER = 0.08;
    final double CONTRIBUTION_SENIOR = 0.12;

    //коэфф. руководства
    final int HEAD_CONST_TEAMLEADER = 300;
    final int HEAD_CONST_PRMANAGER = 4000;
    final int HEAD_CONST_SENIOR = 5000;

    void CreatePersonalFromFile(String file) {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file), StandardCharsets.UTF_8))) {
            String str;
            while ((str = reader.readLine()) != null) {
                String record[] = str.split("\t");
                switch (record[2]) {
                    case "senior manager": {
                        senior_managers.add(new SeniorManager(Integer.parseInt(record[0]), record[1], record[3]));
                        for (int i = 0; i < projects.size(); i++)
                            if (projects.get(i).getName_project().equals(record[3]))
                                projects.get(i).setCount_senior(projects.get(i).getCount_senior() + 1);
                        break;
                    }
                    case "project manager": {
                        project_managers.add(new ProjectManager(Integer.parseInt(record[0]), record[1], record[3]));
                        for (int i = 0; i < projects.size(); i++)
                            if (projects.get(i).getName_project().equals(record[3]))
                                projects.get(i).setCount_pr_manager(projects.get(i).getCount_pr_manager() + 1);
                        break;
                    }
                    case "team leader": {
                        team_leaders.add(new TeamLeader(Integer.parseInt(record[0]), record[1], Integer.parseInt(record[3]), record[4]));
                        for (int i = 0; i < projects.size(); i++)
                            if (projects.get(i).getName_project().equals(record[4]))
                                projects.get(i).setCount_team_leader(projects.get(i).getCount_team_leader() + 1);
                        break;
                    }
                    case "programmer": {
                        programmers.add(new Programmer(Integer.parseInt(record[0]), record[1], Integer.parseInt(record[3]), record[4]));
                        for (int i = 0; i < projects.size(); i++)
                            if (projects.get(i).getName_project().equals(record[4]))
                                projects.get(i).setCount_programmer(projects.get(i).getCount_programmer() + 1);
                        break;
                    }
                    case "teaster": {
                        teasters.add(new Teaster(Integer.parseInt(record[0]), record[1], Integer.parseInt(record[3]), record[4]));
                        for (int i = 0; i < projects.size(); i++)
                            if (projects.get(i).getName_project().equals(record[4]))
                                projects.get(i).setCount_teaster(projects.get(i).getCount_teaster() + 1);
                        break;
                    }
                    case "cleaner": {
                        cleaners.add(new Cleaner(Integer.parseInt(record[0]), record[1], Integer.parseInt(record[3])));
                        break;
                    }
                    case "driver": {
                        drivers.add(new Driver(Integer.parseInt(record[0]), record[1], Integer.parseInt(record[3])));
                        break;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    void CreateProjectsFromFile(String file) {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file), StandardCharsets.UTF_8))) {
            String str;
            while ((str = reader.readLine()) != null) {
                String record[] = str.split("\t");
                projects.add(new ListProject(record[0], Integer.parseInt(record[1])));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    void setBonus() {
        for (int i = 0; i < cleaners.size(); i++)
            cleaners.get(i).setBonus_cleaner(BASE_CLEANER);

        for (int i = 0; i < drivers.size(); i++)
            drivers.get(i).setBonus_driver(BASE_DRIVER);

        for (int p = 0; p < projects.size(); p++) {
            for (int i = 0; i < programmers.size(); i++) {
                int count_programmer = projects.get(p).getCount_programmer();
                if (programmers.get(i).getName_project().equals(projects.get(p).getName_project())) {
                    programmers.get(i).setBonus_programmer(CONTRIBUTION_PROGRAMMER / count_programmer, BASE_PROGRAMMER);
                    programmers.get(i).setMoney_project(projects.get(p).getMoney_progect());
                }
            }

            for (int i = 0; i < team_leaders.size(); i++) {
                int count_teamleader = projects.get(p).getCount_team_leader();
                if (team_leaders.get(i).getName_project().equals(projects.get(p).getName_project())) {
                    team_leaders.get(i).setBonus_teamleader(CONTRIBUTION_TEAMLEADER / count_teamleader, BASE_TEAMLEADER, HEAD_CONST_TEAMLEADER);
                    team_leaders.get(i).setMoney_project(projects.get(p).getMoney_progect());
                }
                team_leaders.get(i).setCount_inferior(projects.get(i).Count_For_TeamLeader());
            }

            for (int i = 0; i < teasters.size(); i++) {
                int count_teaster = projects.get(p).getCount_teaster();
                if (teasters.get(i).getName_project().equals(projects.get(p).getName_project())) {
                    teasters.get(i).setBonus_teaster(CONTRIBUTION_TEASTER / count_teaster, BASE_TEASTER);
                    teasters.get(i).setMoney_project(projects.get(p).getMoney_progect());
                }
            }

            for (int i = 0; i < project_managers.size(); i++) {
                int count_project_manager = projects.get(p).getCount_pr_manager();
                if (project_managers.get(i).getName_project().equals(projects.get(p).getName_project())) {
                    project_managers.get(i).setBonus_pr_manager(CONTRIBUTION_PR_MANAGER / count_project_manager, HEAD_CONST_PRMANAGER);
                    project_managers.get(i).setMoney_project(projects.get(p).getMoney_progect());
                }
                project_managers.get(i).setCount_inferior(projects.get(i).Count_For_Pr_Manager());
            }

            for (int i = 0; i < senior_managers.size(); i++) {
                int count_senior_manager = projects.get(p).getCount_senior();
                if (senior_managers.get(i).getName_project().equals(projects.get(p).getName_project())) {
                    senior_managers.get(i).setBonus_senior(CONTRIBUTION_SENIOR / count_senior_manager, HEAD_CONST_SENIOR);
                    senior_managers.get(i).setMoney_project(projects.get(p).getMoney_progect());
                }
                senior_managers.get(i).setCount_inferior(projects.get(i).Count_For_Senior());
            }
        }
    }

    void payroll() {
        setBonus();
        for (int i = 0; i < cleaners.size(); i++)
            cleaners.get(i).pay();
        for (int i = 0; i < drivers.size(); i++)
            drivers.get(i).pay();
        for (int i = 0; i < teasters.size(); i++)
            teasters.get(i).pay();
        for (int i = 0; i < programmers.size(); i++)
            programmers.get(i).pay();
        for (int i = 0; i < team_leaders.size(); i++)
            team_leaders.get(i).pay();
        for (int i = 0; i < project_managers.size(); i++)
            project_managers.get(i).pay();
        for (int i = 0; i < senior_managers.size(); i++)
            senior_managers.get(i).pay();
    }

    void PrintAll() {
        for (int p = 0; p < projects.size(); p++) {
            System.out.println("Project: " + projects.get(p).getName_project());
            for (int i = 0; i < senior_managers.size(); i++)
                if (senior_managers.get(i).getName_project().equals(projects.get(p).getName_project()))
                    System.out.println(senior_managers.get(i).getName() + " (senior_manager) - " + senior_managers.get(i).getPayment());
            for (int i = 0; i < project_managers.size(); i++)
                if (project_managers.get(i).getName_project().equals(projects.get(p).getName_project()))
                    System.out.println(project_managers.get(i).getName() + " (project_manager) - " + project_managers.get(i).getPayment());
            for (int i = 0; i < team_leaders.size(); i++)
                if (team_leaders.get(i).getName_project().equals(projects.get(p).getName_project()))
                    System.out.println(team_leaders.get(i).getName() + " (team_leader) - " + team_leaders.get(i).getPayment());
            for (int i = 0; i < programmers.size(); i++)
                if (programmers.get(i).getName_project().equals(projects.get(p).getName_project()))
                    System.out.println(programmers.get(i).getName() + " (programmer) - " + programmers.get(i).getPayment());
            for (int i = 0; i < teasters.size(); i++)
                if (teasters.get(i).getName_project().equals(projects.get(p).getName_project()))
                    System.out.println(teasters.get(i).getName() + " (teaster) - " + teasters.get(i).getPayment());
            System.out.println();
            System.out.println();
        }
        for (int i = 0; i < drivers.size(); i++)
            System.out.println(drivers.get(i).getName() + " (driver) - " + drivers.get(i).getPayment());
        for (int i = 0; i < cleaners.size(); i++)
            System.out.println(cleaners.get(i).getName() + " (cleaner) - " + cleaners.get(i).getPayment());
    }
}