package StaffDemo;

public class Engineer extends Employee implements WorkTime, Project {
    private String name_project;
    private int base;
    private double personal_contribution;
    private int money_project;

    public void setName_project(String name_project) {
        this.name_project = name_project;
    }

    Engineer(int new_id, String new_name) {
        super(new_id, new_name);
    }

    @Override
    public int pay_work_time(int worktime, int base) {
        return (worktime * base);
    }

    @Override
    public double pay_project(int money_project, double personal_contribution) {
        return money_project * personal_contribution;
    }

    public void setMoney_project(int new_money_project) {
        this.money_project = new_money_project;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public void setPersonal_contribution(double personal_contribution) {
        this.personal_contribution = personal_contribution;
    }

    public String getName_project() {
        return name_project;
    }

    void pay() {
        setPayment(pay_work_time(getWorktime(), base) + pay_project(money_project, this.personal_contribution));
    }
}

class Teaster extends Engineer {
    Teaster(int new_id, String new_name, int new_worktime, String New_name_project) {
        super(new_id, new_name);
        setWorktime(new_worktime);
        setName_project(New_name_project);
    }

    public void setBonus_teaster(double contribution_teaster, int BASE_TEASTER) {
        setPersonal_contribution(contribution_teaster);
        setBase(BASE_TEASTER);
    }
}

class Programmer extends Engineer {
    Programmer(int new_id, String new_name, int new_worktime, String New_name_project) {
        super(new_id, new_name);
        setWorktime(new_worktime);
        setName_project(New_name_project);
    }

    public void setBonus_programmer(double contribution_programmer, int BASE_PROGRAMMER) {
        setPersonal_contribution(contribution_programmer);
        setBase(BASE_PROGRAMMER);
    }
}

class TeamLeader extends Programmer implements Heading {
    int HEAD_CONST_TEAMLEADER;
    int count_inferior;

    TeamLeader(int new_id, String new_name, int new_worktime,String New_name_project) {
        super(new_id, new_name, new_worktime,New_name_project);
        setWorktime(new_worktime);
        setName_project(New_name_project);
    }

    public void setBonus_teamleader(double contribution_teamleader, int BASE_TEAMLEADER, int HEAD_CONST_TEAMLEADER) {
        setBase(BASE_TEAMLEADER);
        setPersonal_contribution(contribution_teamleader);
        this.HEAD_CONST_TEAMLEADER = HEAD_CONST_TEAMLEADER;
    }

    public void setCount_inferior(int count_inferior) {
        this.count_inferior = count_inferior;
    }

    @Override
    public int pay_heading(int count_inferior, int HEAD_CONST_TEAMLEADER) {
        return count_inferior * HEAD_CONST_TEAMLEADER;
    }

    void pay() {
        super.pay();
        setPayment(getPayment() + pay_heading(count_inferior, HEAD_CONST_TEAMLEADER));
    }
}

