package StaffDemo;

public class ListProject {
    private String name_project;
    private int money_project;
    private int Count_teaster;
    private int Count_programmer;
    private int Count_team_leader;
    private int Count_pr_manager;
    private int Count_senior;

    ListProject (String name, int money) {
        this.name_project = name;
        this.money_project = money;
    }

    int Count_For_TeamLeader (){
        return Count_programmer;
    }

    int Count_For_Pr_Manager (){
        return Count_programmer+Count_teaster+Count_team_leader;
    }

    int Count_For_Senior (){
        return Count_programmer+Count_teaster+Count_team_leader+Count_pr_manager;
    }

    public String getName_project() {
        return name_project;
    }

    public int getMoney_progect() {
        return money_project;
    }

    public int getCount_teaster() {
        return Count_teaster;
    }

    public void setCount_teaster(int count_teaster) {
        Count_teaster = count_teaster;
    }

    public int getCount_programmer() {
        return Count_programmer;
    }

    public void setCount_programmer(int count_programmer) {
        Count_programmer = count_programmer;
    }

    public int getCount_team_leader() {
        return Count_team_leader;
    }

    public void setCount_team_leader(int count_team_leader) {
        Count_team_leader = count_team_leader;
    }

    public int getCount_pr_manager() {
        return Count_pr_manager;
    }

    public void setCount_pr_manager(int count_pr_manager) {
        Count_pr_manager = count_pr_manager;
    }

    public int getCount_senior() {
        return Count_senior;
    }

    public void setCount_senior(int count_senior) {
        Count_senior = count_senior;
    }
}

