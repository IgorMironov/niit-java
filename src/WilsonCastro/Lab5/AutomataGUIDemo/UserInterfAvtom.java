
import java.awt.Color;
import java.awt.event.ActionEvent;
import static java.lang.Thread.sleep;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Timer;

enum States {
    OFF, WAIT, ACCEPT, COOK
};

public class UserInterfAvtom extends javax.swing.JFrame {

    demoAvtomat demoAvto = new demoAvtomat();
    avtomat avtomat = new avtomat();
    Timer t;
    int count = 0;
    int count2;

    public UserInterfAvtom() {
        initComponents();
        t = new Timer(100, fun());
        // count = 0;
        count2 = 0;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        hotChocolate = new javax.swing.JToggleButton();
        doblChocolate = new javax.swing.JToggleButton();
        blackTea = new javax.swing.JToggleButton();
        greenTea = new javax.swing.JToggleButton();
        hotMilk = new javax.swing.JToggleButton();
        orangeJuice = new javax.swing.JToggleButton();
        appleJuice = new javax.swing.JToggleButton();
        coolKvas = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        displayTxt = new javax.swing.JTextField();
        monet20 = new javax.swing.JToggleButton();
        monet25 = new javax.swing.JToggleButton();
        monet30 = new javax.swing.JToggleButton();
        monet40 = new javax.swing.JToggleButton();
        OnOffBtn = new javax.swing.JToggleButton();
        jLabel9 = new javax.swing.JLabel();
        pb = new javax.swing.JProgressBar();
        monet50 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        hotChocolate.setText("Горячий шоколад");
        hotChocolate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hotChocolateActionPerformed(evt);
            }
        });

        doblChocolate.setText("Двоиной шоколад");
        doblChocolate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doblChocolateActionPerformed(evt);
            }
        });

        blackTea.setText("Черный чай");
        blackTea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blackTeaActionPerformed(evt);
            }
        });

        greenTea.setText("Зеленйы чай");
        greenTea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greenTeaActionPerformed(evt);
            }
        });

        hotMilk.setText("Горячее молоко");
        hotMilk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hotMilkActionPerformed(evt);
            }
        });

        orangeJuice.setText("Апельсиновый сок");
        orangeJuice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                orangeJuiceActionPerformed(evt);
            }
        });

        appleJuice.setText("Яблочный сок");
        appleJuice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                appleJuiceActionPerformed(evt);
            }
        });

        coolKvas.setText("Холодный квас");
        coolKvas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                coolKvasActionPerformed(evt);
            }
        });

        jLabel1.setText("30 руб.");

        jLabel2.setText("35 руб.");

        jLabel3.setText("20руб.");

        jLabel4.setText("20 руб.");

        jLabel5.setText("40 руб.");

        jLabel6.setText("30 руб.");

        jLabel7.setText("30 руб.");

        jLabel8.setText("25 руб.");

        displayTxt.setText("Off");

        monet20.setText("20");
        monet20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                monet20ActionPerformed(evt);
            }
        });

        monet25.setText("25");
        monet25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                monet25ActionPerformed(evt);
            }
        });

        monet30.setText("30");
        monet30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                monet30ActionPerformed(evt);
            }
        });

        monet40.setText("40");
        monet40.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                monet40ActionPerformed(evt);
            }
        });

        OnOffBtn.setText("On/Off");
        OnOffBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OnOffBtnActionPerformed(evt);
            }
        });

        jLabel9.setText("Монеты");

        monet50.setText("50");
        monet50.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                monet50ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(displayTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(OnOffBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(greenTea, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(blackTea, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(doblChocolate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(hotChocolate, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))
                                .addGap(47, 47, 47)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(hotMilk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(orangeJuice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(appleJuice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(coolKvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel7)
                                        .addComponent(jLabel8))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel6)
                                        .addComponent(jLabel5))))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(monet20, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(monet25, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(monet30, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(monet40, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(monet50))
                                    .addComponent(jLabel9))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap(47, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(144, 144, 144)
                .addComponent(pb, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(displayTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(OnOffBtn))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(monet20)
                                    .addComponent(monet25)
                                    .addComponent(monet30)
                                    .addComponent(monet40)
                                    .addComponent(monet50)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(hotMilk)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(orangeJuice)
                                    .addComponent(jLabel6)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(176, 176, 176)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hotChocolate)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(doblChocolate)
                            .addComponent(jLabel2))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(blackTea)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(greenTea)
                            .addComponent(jLabel4)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(appleJuice)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(coolKvas)
                            .addComponent(jLabel8))))
                .addGap(18, 18, 18)
                .addComponent(pb, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void doblChocolateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doblChocolateActionPerformed
        displayTxt.setText("Двоиной шоколад");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        
        pb.setValue(0);
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            t.stop();
            pb.setValue(0);
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_doblChocolateActionPerformed

    private void blackTeaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blackTeaActionPerformed
        displayTxt.setText("Черный чай");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_blackTeaActionPerformed

    private void greenTeaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greenTeaActionPerformed
        displayTxt.setText("Зеленйы чай");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_greenTeaActionPerformed

    private void hotMilkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hotMilkActionPerformed
        displayTxt.setText("Горячее молоко");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_hotMilkActionPerformed

    private void orangeJuiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_orangeJuiceActionPerformed
        displayTxt.setText("Апельсиновый сок");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_orangeJuiceActionPerformed

    private void appleJuiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_appleJuiceActionPerformed
        displayTxt.setText("Яблочный сок");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_appleJuiceActionPerformed

    private void coolKvasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_coolKvasActionPerformed
        displayTxt.setText("Холодный квас");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_coolKvasActionPerformed


    private void OnOffBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OnOffBtnActionPerformed
        //  OnOffBtn.setBackground(Color.red);
        if (OnOffBtn.isSelected()) {
            OnOffBtn.setBackground(Color.green);
            if (avtomat.state == avtomat.off()) {

                avtomat.on();
                displayTxt.setText("On. Wait a moment...");
                displayTxt.update(displayTxt.getGraphics());
                try {
                    sleep(1000);
                } catch (InterruptedException ex) {
                }
                displayTxt.setText("Welcome, please insert coins");
            }
        } else if (!OnOffBtn.isSelected()) {
            avtomat.off();
            displayTxt.setText("Off");
            OnOffBtn.setBackground(Color.red);
        }

    }//GEN-LAST:event_OnOffBtnActionPerformed

    public Action fun() {
        return new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                pb.setValue(count++);
                count2 = count;
            }
        };
    }

    public int getCount() {
        System.out.println(count);
        return count;
    }


    private void monet20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_monet20ActionPerformed
        displayTxt.setText("Your credit: 20 Rub.");
        displayTxt.update(displayTxt.getGraphics());
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Choose a drink");
    }//GEN-LAST:event_monet20ActionPerformed

    private void monet25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_monet25ActionPerformed
        displayTxt.setText("Your credit: 25 Rub.");
        displayTxt.update(displayTxt.getGraphics());
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Choose a drink");
    }//GEN-LAST:event_monet25ActionPerformed

    private void monet30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_monet30ActionPerformed
        displayTxt.setText("Your credit: 30 Rub.");
        displayTxt.update(displayTxt.getGraphics());
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Choose a drink");
    }//GEN-LAST:event_monet30ActionPerformed

    private void monet40ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_monet40ActionPerformed
        displayTxt.setText("Your credit: 40 Rub.");
        displayTxt.update(displayTxt.getGraphics());
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.update(displayTxt.getGraphics());
        if (evt.getSource() == monet40) {
            displayTxt.setText("40");
        }
        displayTxt.setText("Choose a drink");
    }//GEN-LAST:event_monet40ActionPerformed

    private void hotChocolateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hotChocolateActionPerformed
        displayTxt.setText("Горячий шоколад");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Please, wait a moment, the drink is cooking");
        displayTxt.update(displayTxt.getGraphics());

        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        t.start();
        int ss = getCount();
        avtomat.cook();
        System.out.println(pb.getValue());
        if (pb.getPercentComplete() == 0.1) {
            displayTxt.setText("Please, take your drink");
            displayTxt.update(displayTxt.getGraphics());
            avtomat.off();
            //  t.restart();
            pb.setValue(pb.getMinimum());
            //pb.update(pb.getGraphics());
            // Thread.interrupted();
        }
    }//GEN-LAST:event_hotChocolateActionPerformed

    private void monet50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_monet50ActionPerformed
        displayTxt.setText("Your credit: 50 Rub.");
        displayTxt.update(displayTxt.getGraphics());
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
        displayTxt.setText("Choose a drink");
    }//GEN-LAST:event_monet50ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton OnOffBtn;
    private javax.swing.JToggleButton appleJuice;
    private javax.swing.JToggleButton blackTea;
    private javax.swing.JToggleButton coolKvas;
    private javax.swing.JTextField displayTxt;
    private javax.swing.JToggleButton doblChocolate;
    private javax.swing.JToggleButton greenTea;
    private javax.swing.JToggleButton hotChocolate;
    private javax.swing.JToggleButton hotMilk;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JToggleButton monet20;
    private javax.swing.JToggleButton monet25;
    private javax.swing.JToggleButton monet30;
    private javax.swing.JToggleButton monet40;
    private javax.swing.JButton monet50;
    private javax.swing.JToggleButton orangeJuice;
    private javax.swing.JProgressBar pb;
    // End of variables declaration//GEN-END:variables
}
