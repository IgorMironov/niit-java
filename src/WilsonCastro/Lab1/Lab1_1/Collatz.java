/*
Задача 1 Последовательность Коллатца.
Найти наибольшую последовательность Коллатца для чисел в диапазоне 
от 1 до 1 000 000.
*/

public class Collatz {

    public static long collatz(long n, int precision_len) {

        if (n == 1) {
            return precision_len;
        } else if (n % 2 == 0) {
            return collatz(n / 2, precision_len + 1);

        } else {
            return collatz(3 * n + 1, precision_len);
        }
    }

    public static long collatz(long n) {
        return collatz(n, 1);
    }

    public static void main(String[] args) {
        long act_precision_leng = 0;
        long max_precision_leng = 0;
        long max_n = 0;
        long t = 1000000;

        while (t > 1) {

            if ((act_precision_leng = collatz(t)) > max_precision_leng) {
                max_precision_leng = act_precision_leng;
                max_n = t;
            }
        }
        System.out.println("The longest progression from 1 to 1000000 is to "
                + "the number " + t + " whit " + max_precision_leng
                + " steps, n = " + max_n);
    }
}
