
class Circle {

    private double Radius;
    private double Ference;
    private double Area;

    public Circle() {
        Radius = 0;
        Ference = 0;
        Area = 0;
    }

    void are(double a) {
        Area = a;
        Radius = Math.sqrt(a / Math.PI);
        Ference = 2 * Math.PI * Radius;
    }

    void feren(double c) {
        Ference = c;
        Radius = (c / (2 * Math.PI));
        Area = c * c / (4 * Math.PI);
    }

    void radi(double r) {
        Radius = r;
        Ference = 2 * Math.PI * r;
        Area = Math.PI * r * r;
    }

    double getRadius() {
        return Radius;
    }

    double getArea() {
        return Area;
    }

    double getFerence() {
        return Ference;
    }
}
