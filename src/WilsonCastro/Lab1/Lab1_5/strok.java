
import static jdk.nashorn.internal.objects.Global.print;

/*
 Задача 5. Вывод числа символами.
Напишите программу, которая будет считывать из командной строки число 
(например 41072819) и выводить его в таком виде:
                     * * *  
                    *     *  
                         *   
                        *    
                       *     
                      ****** 
 */
public class strok {

    private static final int WIDTHSCREEN = 10;

    public static void main(String[] args) {
        int i = 0;
        int t = 0;

        int numbers[] = new int[WIDTHSCREEN];
        char arr[] = args[0].toCharArray();
        while (i < arr.length && t < WIDTHSCREEN) {
            numbers[t++] = Character.digit(arr[i++], 10);
            print(numbers, t);
            System.out.println();
        }
    }

    static void print(int numbers[], int m) {
        if (m > 0) {
            for (int i = 0; i < 7; i++) {
                for (int j = 0; j < m; j++) 
                    System.out.println(Numbers.strings[numbers[j]][i]);
                
                System.out.println();
            }
        }
    }
}
