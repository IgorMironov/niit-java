
import java.io.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class StaffDemo {

    public static Employee[] EmployeeFile(String file) {

        Employee val[];
        try {
            File f = new File(file);
            FileReader fr = new FileReader(f);
            BufferedReader bufferedReader = new BufferedReader(fr);
            String buf;
            String parsedBuf[];
            bufferedReader.mark((int) f.getUsableSpace());
            val = new Employee[(int) bufferedReader.lines().count()];
            bufferedReader.reset();
            int i = 0;
            while ((buf = bufferedReader.readLine()) != null) {
                parsedBuf = buf.split(",");
                switch (parsedBuf[2].toString()) {
                    case "Driver":
                        val[i] = new Driver(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]));
                        break;
                    case "Cleaner":
                        val[i] = new Cleaner(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]));
                        break;
                    case "Programmer":
                        val[i] = new Programmer(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]),
                                1);
                        break;
                    case "Tester":
                        val[i] = new Tester(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]),
                                1);
                        break;
                    case "TeamLeader":
                        val[i] = new TeamLeader(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1],
                                40,
                                Integer.parseInt(parsedBuf[3]),
                                2,
                                7);
                        break;
                    case "ProjectManager":
                        val[i] = new ProjectManager(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1]);
                        break;
                    case "SeniorManager":
                        val[i] = new SeniorManager(
                                Integer.parseInt(parsedBuf[0]),
                                parsedBuf[1]);
                        break;
                    default:
                        throw new IOException("Wrong file!");
                }
                i++;
            }
            return val;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String args[]) {
        WorkProject wp = new WorkProject("Project-A", 1700000, 0.1, 15);
        Employee arr[] = EmployeeFile("staff.txt");
        for (Employee e : arr) {
            wp.addEmployee(e);
        }
        wp.printSalary();
    }
	}

