
public class ProjectManager extends Manager implements Heading{
    final static int paymentForEachProgrammer=120;
    public ProjectManager(int id, String name){
        super(id, name);
    }
    
    public int calPayment(){
        return calcPaymentForHeading() +  calcPaymentForProject();
    }
    public int calcPaymentForProject()
    {
        return workProject.price * workProject.percentForManager / 100;
    }

    public int calcPaymentForHeading()
    {
        return workProject.getNumOfEmployees() * paymentForEveryProgrammer;
    }
}
