/*
Задача 2.
Разработать сетевое приложение, сервер которого пересылает клиентам 
случайный афоризм из хранящегося на сервере текстового файла.
 */

import java.io.IOException;

public class main {

    public static void main(String[] args) throws IOException {
        /* formAforism formAfori = new formAforism();
        formAfori.setTitle("Server's Time and Date");
        formAfori.setLocationRelativeTo(null);
        formAfori.setVisible(true);
         */
        ClienteAforism clientafor = new ClienteAforism();
        clientafor.answerOfServer();
        clientafor.clientDT();
    }
}
