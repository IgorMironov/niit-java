
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerAforismo {

    public static void main(String[] args) throws IOException {
        String line;
        String[] aforistms = new String[10];

        BufferedReader br = new BufferedReader(new FileReader("C:"
                + "\\Users\\Platon\\Desktop\\aforismos.txt"));

        while ((line = br.readLine()) != null) { //crea el array de renglones
            for (int i = 0; i < 10; i++) {
                aforistms[i] = br.readLine();

            }

        }

        System.out.println("Старт сервера");
        BufferedReader in = null;
        PrintWriter out = null;

        ServerSocket server = null;
        Socket client = null;

        // создаем серверный сокет
        try {
            server = new ServerSocket(1234);
        } catch (IOException e) {
            System.out.println("Ошибка связывания с портом 1234");
            System.exit(-1);
        }

        try {
            System.out.println("Ждем соединения");
            client = server.accept();
            System.out.println("Клиент подключился");
        } catch (IOException e) {
            System.out.println("Не могу установить соединение");
            System.exit(-1);
        }

        in = new BufferedReader(
                new InputStreamReader(client.getInputStream()));
        out = new PrintWriter(client.getOutputStream(), true);
        String input, outAform;
        int rand = 0;

        System.out.println("Ожидаем сообщений");
        while ((input = in.readLine()) != null) {

            if (input.equalsIgnoreCase("exit")) {
                break;
            }
           
            int aforRand = (int) (Math.random() * 10);
            outAform = aforistms[aforRand]; /// rand
            out.println("Ваш афоризм: " + outAform);
            System.out.println(outAform);

            // System.out.println("");
            //  System.out.println("Aforismo Aleatorio: " + aforistms[aforRand]);
            // out.println(formatForDateTime.format(dateNow=new Date()));
            // out.println("Дата и время Сервера: " + formatForDateTime.format(dateNow=new Date()));
            // out.println("Дата Сервера: " + formatForDate.format(dateNow=new Date()));
            //out.println("Время Сервера: " + formatForTime.format(dateNow=new Date()));
            System.out.println(input);
            System.out.println(outAform);
        }
        out.close();
        in.close();
        client.close();
        server.close();
    }
}
