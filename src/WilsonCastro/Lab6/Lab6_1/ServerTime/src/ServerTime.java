import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerTime {
     public static void main(String[] args) throws IOException {
    System.out.println("Старт сервера");
    BufferedReader in = null;
    PrintWriter    out= null;

    ServerSocket server = null;
    Socket       client = null;
    
    Date dateNow;
     SimpleDateFormat formatForDateTime = new 
        SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
    SimpleDateFormat formatForDate = new 
        SimpleDateFormat("yyyy.MM.dd");
    SimpleDateFormat formatForTime = new 
        SimpleDateFormat("hh:mm:ss");
    // создаем серверный сокет
    try {
      server = new ServerSocket(1234);
    } catch (IOException e) {
      System.out.println("Ошибка связывания с портом 1234");
      System.exit(-1);
    }

    try {
      System.out.println("Ждем соединения");
      client= server.accept();
      System.out.println("Клиент подключился");
    } catch (IOException e) {
      System.out.println("Не могу установить соединение");
      System.exit(-1);
    }

    in  = new BufferedReader(
          new InputStreamReader(client.getInputStream()));
    out = new PrintWriter(client.getOutputStream(),true);
    String input,output;

    System.out.println("Ожидаем сообщений");
    while ((input = in.readLine()) != null) {
     if (input.equalsIgnoreCase("exit")) 
       break;
     out.println(formatForDateTime.format(dateNow=new Date()));
     // out.println("Дата и время Сервера: " + formatForDateTime.format(dateNow=new Date()));
    // out.println("Дата Сервера: " + formatForDate.format(dateNow=new Date()));
    //out.println("Время Сервера: " + formatForTime.format(dateNow=new Date()));
    // System.out.println(input);
    }
    out.close();
    in.close();
    client.close();
    server.close();
  }
}
