/*
Лабораторный практикум №3
Проект DekanatDemo

Разработать класс Student для хранения информации о студенте.
Примерный перечень полей:
ID - идентификационный номер
Fio - фамилия и инициалы
Group - ссылка на группу (объект Group)
Marks - массив оценок
Num - количество оценок
Обеспечить класс следующими методами:

создание студента с указанием ИД и ФИО
зачисление в группу
добавление оценки
вычисление средней оценки
Разработать класс Group для хранения информации об учебной группе

Примерный перечень полей:
Title - название группы
Students - массив из ссылок на студентов
Num - количество студентов в группе
Head - ссылка на старосту (из членов группы)
Обеспечить класс следующими методами:

создание группы с указанием названия
добавление студента
избрание старосты
поиск студента по ФИО или ИД
вычисление соеднего балла в группе
исключение студента из группы
Разработать класс Dekanat

Примерный перечень полей:
Students - массив студентов
Groups - массив групп

Обеспечить класс следующими методами:
создание студентов на основе данных из файла
создание групп на основе данных из файла
добавление случайных оценок студентам
накопление статистики по успеваемости студентов и групп
перевод студентов из группы в группу
отчисление студентов за неуспеваемость
сохранение обновленных данных в файлах
инициация выборов старост в группах
вывод данных на консоль
Создать два файла с данными для групп и студентов (не менее 3 групп и 30 
студентов). Использовать эти файлы при формировании данных групп и студентов
Написать демонстрационную версию приложения
*/


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Random;
import java.util.StringTokenizer;

public class Student {

    private int id;
    private String Fio;
    private Group group;
    private int Marks[] = new int[10];
    private int Num; // количество оценок
    private int summa;
    private int average;

    Student(int id, String Fio) {
        this.id = id;
        this.Fio = Fio;
        this.group = null;
        this.Num = 0;
        summa = 0;
        average = 0;
    }

    public String getFio() {
        return Fio;
    }

    public int getId() {
        return id;
    }

    public Group getGroup() {
        return group;
    }

    public void toGroup(Group group) {
        this.group = group;
    }

    public void addMarks(int Mark) {
        if (Num <= Marks.length) {
            Marks[Num++] = Mark;
        }
    }

    public double averageMarksStud() {
        for (int i = 0; i < Marks.length; i++) {
            summa += Marks[i];
        }
        average = summa / Marks.length;
        return average;
    }
}

class Group {

    private String Title;
    private Student[] students;
    private Group[] group;
    private int Num;
    private Student Head;

    Group(String Title) {
        Num = 10;
        this.Title = Title;
        students = new Student[Num];
        group = new Group[3];
        Head = null;
    }

    public Student getHead() {
        return Head;
    }

    public String getTitle() {
        return Title;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student student) {
        for (int i = 0; i < students.length; i++) {
            if (students[i] == null) {
                students[i] = student;
            }
        }
    }

    public void choiseHead() {
        Random rand = new Random();
        Head = students[rand.nextInt(Num) + 1];
    }

    public Student searchStudentId(int id) {
        for (Student i : students) {
            if (i != null && i.getId() == id) {
                return i;
            }
        }
        return null;
    }

    public Student searchStudentFio(String Fio) {
        for (Student i : students) {
            if (i != null && Fio.equals(i.getFio())) {
                return i;
            }
        }
        return null;
    }

    public void removeStudent(Student student) {
        int index = 0;
        while (index < students.length) {
            if (students[index] == student) {
                students[index] = null;
                index++;
            }
        }
    }

    public double averageMarksGr() {
        double sumAverages = 0;
        double counterAverages = 0;
        for (Student index : students) {
            sumAverages += index.averageMarksStud();
            counterAverages++;
        }
        return sumAverages / counterAverages;
    }

}

class Decanat {

    Group group[] = new Group[3];
    String line;
    Student students[] = new Student[30];
    public final int NumStudIngroup = 10;
    int i = 0;

    public Decanat() throws IOException {   //Aqui**************
        Writer file = new FileWriter("random.txt");
        try (BufferedReader br = new BufferedReader(new FileReader("students5.txt"))) {
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");

                int id = 0;
                String fios = "";

                while (st.hasMoreElements()) {
                    String ids = st.nextToken();
                    id = Integer.parseInt(ids);
                    fios = st.nextElement().toString();

                }
                students[i] = new Student(id, fios.toString());

                System.out.println(students[i].getId() + ", "
                        + students[i].getFio());
                i++;
            }

        } catch (IOException ex) {
            System.out.println("Ошибка ввода-вывода:" + ex);
        }

        try (BufferedReader br = new BufferedReader(new FileReader("grups.txt"))) {
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");
                int j = 0;
                String grupp = "";

                while (st.hasMoreElements()) {
                    grupp = st.nextElement().toString();

                }
                group[j] = new Group(grupp.toString());
                System.out.println(group[j].getTitle());
                j++;

            }

        } catch (IOException ex) {
            System.out.println("Ошибка ввода-вывода:" + ex);
        }
    }

    // FALta  CODIGO PARA COPIAR LOS GRUPOS Y ESTUDIANTES EN UN NUEVO ARCHIVO
    public void SaveInfoInFile() throws IOException {
        Writer file = new FileWriter("Information.txt");
        //   int n=(int)(Math.random()*10);
        //  String outstr="Number:"+n;
        Student[] stud = new Student[30];
        Group[] grp = new Group[3];
        for (Group g : group) {
            if (g != null) {
                String lin = g.toString();
                /// file.write(" lin);   ????
                file.close();
            }
        }

    }

    public void randMarks() {
        Random rand = new Random();
        for (Student stud : students) {
            if (stud != null) {
                for (int k = 0; k < 10; k++) {
                    stud.addMarks(rand.nextInt(4) + 1);
                }
            }
        }
    }

    public String statisticMarks() {
        String statistic = "";
        for (Group grp : group) {
            if (grp != null) {
                statistic += "\nGroup: " + grp.toString() + "\n--\n" + "ID\t       FIO      \t AVERAGE MARK" + "\n"
                        + "--------------------\n";
                for (Student std : grp.getStudents()) {
                    if (std != null) {
                        statistic += std.getId() + "\t" + String.format("%20s", std.getFio()) + std.averageMarksStud()
                                + "\n";
                    }
                    statistic += "---\nTotal: " + grp.averageMarksGr() + "\n";
                    statistic += "Head: " + grp.getHead().getId() + " " + grp.getHead().getFio() + "\n";
                }

            }
        }
        return statistic;
    }

    public void moveStudent(Student student, Group group) {
        if (student.getGroup() != null) {
            student.getGroup().removeStudent(student);
        }
        student.toGroup(group);
        group.setStudents(student);
    }

    public void removeStudentByMarks() {
        final Double minMark = 2.0;
        for (Student st : students) {
            if (st != null && st.averageMarksStud() <= minMark) {
                if (st.getGroup() != null) {
                    st.getGroup().removeStudent(st);
                }
                st = null;
            }
        }
    }

    public void fillGroups() {
        int x = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < NumStudIngroup; j++) {
                if (this.group[i] != null && this.students[x] != null) {
                    this.moveStudent(this.students[x++], this.group[i]);
                }
            }
        }
    }

    public void choiseHeadInGroup() {
        int k;
        Random rand = new Random();
        for (Group grp : group) {
            if (grp != null) {
                while (grp.getHead() == null) {
                    k = rand.nextInt(NumStudIngroup - 1);
                    if (grp.getStudents()[k] != null) {
                        grp.choiseHead();   //(grp.getStudents()[k]);
                    }
                }
            }
        }

    }

    Group searchingGroup(String name) {
        for (Group grp : group) {
            if (grp != null && name.equals(grp.toString())) {
                return grp;
            }
        }
        return null;
    }

    Student searchingStudentId(int id) {
        for (Student std : students) {
            if (std != null && std.getId() == id) {
                return std;
            }
        }
        return null;
    }

    Student searchingStudentFIO(String fio) {
        for (Student std : students) {
            if (std != null & std.equals(std.toString())) {
                return std;
            }
        }
        return null;
    }

    public int getNumGroups() {
        int j = 0;
        for (Group grp : group) {
            if (grp != null) {
                j++;
            }
        }
        return j;

    }

    public int getNumStudents() {
        int j = 0;
        for (Student std : students) {
            if (std != null) {
                j++;
            }
        }
        return j;
    }
}

class decanatDemo {

    public static void main(String[] args) throws IOException {
        Decanat decan = new Decanat();
        decan.fillGroups();
        decan.choiseHeadInGroup();
        decan.randMarks();
        System.out.println(decan.statisticMarks());

        decan.removeStudentByMarks();
        decan.choiseHeadInGroup();
        System.out.println(decan.statisticMarks());

        decan.SaveInfoInFile();

        Decanat decan2 = new Decanat();
        decan2.fillGroups();
        decan2.choiseHeadInGroup();
        System.out.println(decan2.statisticMarks());
    }
}
