class Engineer extends Employee implements WorkTime, Project {
	int base;
	StaffProject project;

	public Engineer( int id, String name, int base, StaffProject project ) {
		super(id, name);
		this.base = base;
		this.project = project;
	}
	
	public int calculateWorkTimeSalary() {
		return this.worktime * this.base;
	}
	
	public int calculateProjectSalary() {
		return project.GetBudget() / project.GetEmployeesNumber();
	}
	
	public void calculateSalary() {
		this.payment = this.calculateWorkTimeSalary() + this.calculateProjectSalary();
	}
	
	protected String getPerconalInfo() {
		return "project: " + project.GetName();
	}
}
