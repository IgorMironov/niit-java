import java.util.*;
import java.io.*;

public class Automat{
	private int cash;
	private ArrayList<Drink> menu;
	private Drink choise;
	enum AutomatState {OFF, WAIT, ACCEPT, CHECK, COOK};
	private AutomatState state;
	private AutomatGUIInterface gui;
	private long cook_start ;
	private long cook_duration; // ms
	
	Automat(String file_name, AutomatGUIInterface _gui){
		this.gui = _gui;
		
		this.cook_start = 0;
		this.cook_duration = 10000; // ms
		
		this.cash = 0;
		this.choise = null;
		this.state = AutomatState.OFF;
		this.menu = new ArrayList<Drink>();
		File menu_file = new File(file_name);
		if(menu_file.exists() && menu_file.isFile()){
			try{
				FileReader menu_file_reader = new FileReader(menu_file);
				BufferedReader menu_reader = new BufferedReader(menu_file_reader);
				String line = null;
				while((line = menu_reader.readLine()) != null){
					String[] line_parts = line.split(";");
					if(line_parts.length >= 3){
						this.menu.add(new Drink(line_parts[0], Integer.parseInt(line_parts[1]), Integer.parseInt(line_parts[2])));
					}
				}
				menu_reader.close();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		else
			System.out.println("File " + file_name + " not found!!!");
	}
	
	private void ShowInfo( String msg ) {
		System.out.println(msg);
		this.gui.showText("Your money: " + this.cash + "\nYour choise: " + (this.choise != null ? this.choise.name : "-") + "\n" + msg );
	}
	
	public String GetStateName(){
		switch(state){
			case OFF:
				return "off";
			case WAIT:
				return "wait";
			case ACCEPT:
				return "accept";
			case CHECK:
				return "check";
			case COOK:
				return "cook";
		}
		System.out.println("Error: unknown state!!!");
		return "error";
	}
	
	public boolean On(){
		if(state != AutomatState.OFF){
			System.out.println("Error: On: Imposible from state " + GetStateName());
			ShowInfo("Imposible operation");
			return false;
		}
		state = AutomatState.WAIT;
		ShowInfo("Please enter money and press key with your choise...");
		return true;
	}
	
	public boolean Off(){
		if(state != AutomatState.WAIT){
			System.out.println("Error: Off: Imposible from state " + GetStateName());
			ShowInfo("Imposible operation");
			return false;
		}
		state = AutomatState.OFF;
		ShowInfo("Automat is Off");
		return true;
	}
	
	public boolean Coin(int coin){
		if(state != AutomatState.WAIT && state != AutomatState.ACCEPT){
			System.out.println("Error: Coin: Imposible from state " + GetStateName());
			ShowInfo("Imposible operation");
			return false;
		}
		state = AutomatState.ACCEPT;
		cash += coin;
		ShowInfo("Please press key with your choise...");
		return true;
	}
	
	private boolean Check(int drink_ind){
		if(drink_ind >= menu.size()){
			ShowInfo("Not found...");
			System.out.println("Error. Drink with index " + drink_ind + " not exist!!!");
			return false;
		}
		if(menu.get(drink_ind).price > cash){
			ShowInfo("Error. Not enough money for " + menu.get(drink_ind).name + "(price is " + menu.get(drink_ind).price + ", cash is " + cash + ")");
			return false;
		}
		return true;
	}
	
	public boolean Cancel(boolean show_info){
		if(state != AutomatState.CHECK && state != AutomatState.ACCEPT){
			ShowInfo("Imposible operation");
			System.out.println("Error: Cancel: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.WAIT;
		int back_money = cash;
		cash = 0;
		this.choise = null;
		if(show_info) ShowInfo("Action is cancel. " + (back_money > 0 ? ("Please, get your money back - " + back_money) : ""));
		return true;
	}
	
	public boolean CheckCookFinish(){
		long cook_diff = System.currentTimeMillis() - this.cook_start;
		
		if(cook_diff >= this.cook_duration) return true;

		String info = "";
		while( cook_diff > 0 ) {
			info += "...";
			cook_diff -= 1000;
		}
		ShowInfo(info);
		return false;
	}

	public boolean Cook(){
		if(state != AutomatState.CHECK){
			ShowInfo("Imposible operation");
			System.out.println("Error: Cook: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.COOK;
		
		this.cook_start = System.currentTimeMillis();
		return true;
	}
	
	public boolean Finish(){
		if(state != AutomatState.COOK){
			ShowInfo("Imposible operation");
			System.out.println("Error: Finish: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.WAIT;
		cash = 0;
		this.choise = null;
		ShowInfo("Prepared. Please take your drink and good luck.");
		return true;
	}
	
	public boolean Choice(int drink_ind){
		if(state != AutomatState.ACCEPT && state != AutomatState.WAIT){
			ShowInfo("Imposible operation");
			System.out.println("Error: Choice: Imposible from state " + GetStateName());
			return false;
		}
		state = AutomatState.CHECK;
		if(!Check(drink_ind)) {
			state = AutomatState.ACCEPT;
			return false;
		}
		this.choise = menu.get(drink_ind);

		int change = cash - this.choise.price;
		if(change > 0) {
			ShowInfo("Please wait! Cook in process! Get your change: " + change);
			cash -= change;		
			try {
				Thread.sleep(200);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		} else {
			ShowInfo("");
		}
		return true;
	}

	public int GetMenuLen(){
		return menu.size();
	}
	
	public String GetMenuItemLabel(int ind){
		if(ind >= menu.size()){
			System.out.println("Error. Drink with index " + ind + " not exist!!!");
			return "";
		}
		return menu.get(ind).name;
	}
	
	public int GetMenuItemPrice(int ind){
		if(ind >= menu.size()){
			System.out.println("Error. Drink with index " + ind + " not exist!!!");
			return 0;
		}
		return menu.get(ind).price;
	}
	
	public int GetMenuItemWeight(int ind){
		if(ind >= menu.size()){
			System.out.println("Error. Drink with index " + ind + " not exist!!!");
			return 0;
		}
		return menu.get(ind).weight;
	}
}
