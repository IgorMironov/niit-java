class Circle{
	private double Radius; // - радиус
	private double Ference; // - длина окружности
	private double Area; // - площадь круга
	final double Pi = 3.1415926536;
	// Конструктор, вызывается один раз при создании
	Circle(double _Radius){
		SetByRadius(_Radius);
	}
	//Функции, устанавливающие параметры класса по одному из параметров
	public void SetByRadius(double _Radius){
		Radius = _Radius;
		Ference = 2*Pi*Radius;
		Area = Pi*Math.pow(Radius, 2);
	}
	public void SetByFerence(double _Ference){
		Ference = _Ference;
		Radius = Ference/(2*Pi);
		Area = Pi*Math.pow(Radius, 2);
	}
	public void SetByArea(double _Area){
		Area = _Area;
		Radius = Math.sqrt(Area/Pi);
		Ference = 2*Pi*Radius;;
	}
	//функции, возвращающие параметры класса
	public double GetRadius(){
		return Radius;
	}
	public double GetFerence(){
		return Ference;
	}
	public double GetArea(){
		return Area;
	}
}

public class CircleDemo{
	final static double EarthRadius = 6378100; // meters
	private static void EarthAndRope(){
		Circle Earth = new Circle (EarthRadius);
		double EarthFerence = Earth.GetFerence();
		Earth.SetByFerence(EarthFerence + 1.0);
		double Delta = Earth.GetRadius() - EarthRadius;
		System.out.println("1. Earth Radius = " + EarthRadius + "  Earth with Rope Radius = " + Earth.GetRadius() + " Delta = " + Delta);
	}
	
	// Необходимо рассчитать стоимость материалов для бетонной дорожки вокруг круглого бассейна, 
	// а также стоимость материалов ограды вокруг бассейна (вместе с дорожкой). 
	// Стоимость 1 квадратного метра бетонного покрытия 1000 р. 
	// Стоимость 1 погонного метра ограды 2000 р. Радиус бассейна 3 м. 
	// Ширина бетонной дорожки вокруг бассейна 1 м.
	private static void PoolAndPrice(){
		double poolRadius  = 3;
		double walkWidht = 1;
		
		Circle pool = new Circle(poolRadius);
		double poolArea = pool.GetArea();
		pool.SetByRadius(poolRadius + walkWidht);
		double walkArea = pool.GetArea() - poolArea;
		System.out.println("2. Walk price is: " + walkArea * 1000 + "RUB\n   Wall price is: " + pool.GetFerence() * 2000 + "RUB");
	}
	public static void main(String[] args){
		EarthAndRope();
		PoolAndPrice();
	}
}