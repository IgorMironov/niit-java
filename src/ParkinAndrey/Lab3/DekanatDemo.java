import java.util.*;
import java.io.*;

class Student{
	private int ID;
	private String FIO;
	private Group GROUP;
	private ArrayList<Integer> MARKS;
	private int NUM;
	
	public Student(int _ID, String _FIO){
		this.ID = _ID;
		this.FIO = _FIO;
		this.MARKS = new ArrayList<Integer>();
		this.NUM = 0;
	}
	
	public void SetFIO(String _FIO){
		this.FIO = _FIO;
	}
	public void SetGroup(Group _GROUP){
		this.GROUP = _GROUP;
	}
	public void AddMark(int mark){
		MARKS.add(mark);
		NUM++;
	}
	
	public String GetFIO(){
		return FIO;
	}
	
	public Group GetGROUP(){
		return GROUP;
	}
	
	public float GetAverageMark(){
		float sum_mark = 0;
		for(int mark: MARKS)
			sum_mark += mark;
		return NUM > 0 ? sum_mark/NUM : 0;
	}
	
	public void Print(String prefix){
		System.out.println(prefix + "ID: " + ID + "; FIO: "+ FIO + "; AVERAGE MARK: " + GetAverageMark());
	}
	
	public String Statistics(){
		return "ID: " + ID + "; FIO: "+ FIO + "; AVERAGE MARK: " + GetAverageMark();
	}
}

class Group{
	private String TITLE;
	private Map<Integer,Student> STUDENTS;
	private int NUM;
	private Student HEAD;
	
	public Group(String _TITLE){
		this.TITLE = _TITLE;
		this.STUDENTS =  new HashMap<Integer,Student>();
		this.NUM = 0;
		this.HEAD = null;
	}
	public void SetTitle(String _TITLE){
		this.TITLE = _TITLE;
	}
	public String GetTITLE(){
		return TITLE;
	}
	public void AddStudent(int ID, Student STUDENT){
		STUDENTS.put(ID,STUDENT);
		STUDENT.SetGroup(this);
		NUM++;
	}
	public void ChooseHead(){
		float max_average_mark  = -1;
		int head_id = -1;
		for(Map.Entry<Integer,Student> entry: STUDENTS.entrySet()){
			if(entry.getValue().GetAverageMark() > max_average_mark){
				max_average_mark = entry.getValue().GetAverageMark();
				head_id = entry.getKey();
			}
		}
		if(head_id == -1)
			System.out.println("Error!!! Head not find.");
		else
			HEAD = STUDENTS.get(head_id);
	}
	
	public void PrintHead(){
		if(HEAD != null)
			HEAD.Print("Head: ");
		else
			System.out.println("Heaad not find.");
	}
	
	public void SearchByFIO(String fio){
		boolean result = false;
		for(Map.Entry<Integer,Student> entry: STUDENTS.entrySet()){
			if(entry.getValue().GetFIO().indexOf(fio) >=0 ){
				entry.getValue().Print("");
				result = true;
			}
		}
		if(result == false)
			System.out.println("Not find.");
	}
	
	public void SearchByID(int id){
		if(STUDENTS.containsKey(id))
			STUDENTS.get(id).Print("");
		else
			System.out.println("Not find.");
	}

	public float GetAverageMark(){
		float sum = 0;
		for(Map.Entry<Integer,Student> entry: STUDENTS.entrySet())
			sum += entry.getValue().GetAverageMark();
		return sum/NUM;
	}
	
	public void DeleteStudent(int id){
		if(STUDENTS.containsKey(id)){
			STUDENTS.get(id).SetGroup(null);
			STUDENTS.remove(id);
		}
	}
	
	public void Print(){
		System.out.println("Group: " + TITLE + " (students count is " + NUM + "):");
		for(Map.Entry<Integer, Student> entry: STUDENTS.entrySet())
			entry.getValue().Print("   ");
	}
	
	public String Statistics(){
		return TITLE + " (students count is " + NUM + ", Head is " + HEAD.GetFIO() + ", Average mark is " + GetAverageMark() + ")";
	}	
}

class Dekanat{
	private Map<Integer, Student> STUDENTS;
	private Map<Integer, Group> GROUPS;
	
	public Dekanat(){
		this.STUDENTS = new HashMap<Integer, Student>();
		this.GROUPS = new HashMap<Integer, Group>();
	}
	
	public boolean AddStudentsFromFile(String FileName){
		File file = new File(FileName);
		try {
			if(!file.exists()){
				System.out.println("File " + FileName + " not exist.");
				return false;
			}
			FileReader reader = new FileReader(file);
			BufferedReader breader = new BufferedReader(reader);
			String line;
			int count = 1;
			while ((line=breader.readLine()) != null) {
				STUDENTS.put(count, new Student(count, line.trim()));
				count++;
			}
			reader.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
		return true;
	}
	
	public boolean AddGroupsFromFile(String FileName){
		File file = new File(FileName);
		try {
			if(!file.exists()){
				System.out.println("File " + FileName + " not exist.");
				return false;
			}
			FileReader reader = new FileReader(file);
			BufferedReader breader = new BufferedReader(reader);
			String line;
			int count = 1;
			while ((line=breader.readLine()) != null)
				GROUPS.put(count++, new Group(line.trim()));
			reader.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
		return true;
	} 
	
	private Group GetRandomGroup() {
		Random random = new Random();
		ArrayList<Integer> keys = new ArrayList<Integer>(GROUPS.keySet());
		Integer randomKey = keys.get( random.nextInt(keys.size()) );
		return GROUPS.get(randomKey);
	}
	
	public void MoveStudentsToGroups(){
		for(Map.Entry<Integer,Student> entry: STUDENTS.entrySet()){
			Group new_gr = GetRandomGroup();
			Group old_gr = entry.getValue().GetGROUP();
			if(old_gr != null && old_gr != new_gr){
				old_gr.DeleteStudent(entry.getKey());
			}
			new_gr.AddStudent(entry.getKey(), entry.getValue());
		}
	}
	
	public void AddRandomMarks(){
		for(Map.Entry<Integer,Student> entry: STUDENTS.entrySet()){
			 Random random = new Random();
			 int random_mark_cnt = 5 + random.nextInt(10);
			 for(int i = 0; i < random_mark_cnt; i++){
				 int random_mark = 2 + random.nextInt(3);
				 entry.getValue().AddMark(random_mark);
			 }
		}
	}
	
	public void SetHeaders(){
		for(Map.Entry<Integer,Group> entry: GROUPS.entrySet()){
			entry.getValue().ChooseHead();
		}
	}
	
	public void DeleteStudentsByAverageMark(float min_average_mark){
		ArrayList<Integer> deletedStudents = new ArrayList<Integer>();
		for(Map.Entry<Integer,Student> entry: STUDENTS.entrySet()){
			float student_average_mark = entry.getValue().GetAverageMark();
			if(student_average_mark < min_average_mark){
				deletedStudents.add(entry.getKey());
			}
		}
		for(int student_id: deletedStudents){
			STUDENTS.get(student_id).Print("Delete by average mark: ");
			Group student_group = STUDENTS.get(student_id).GetGROUP();
			if( student_group != null ) 
				student_group.DeleteStudent( student_id );
			STUDENTS.remove(student_id);
		}
	}
	
	public boolean SaveNewDataToFiles(String students_filename, String groups_filename){
		File students_file = new File(students_filename);
		File groups_file = new File(groups_filename);
		try {
			if(!students_file.exists()){
				System.out.println("File " + students_filename + " not exist.");
				return false;
			}
			if(!groups_file.exists()){
				System.out.println("File " + groups_filename + " not exist.");
				return false;
			}
			Writer students_writer = new FileWriter(students_file);
			Writer groups_writer = new FileWriter(groups_file);
			
			BufferedWriter students_bw = new BufferedWriter(students_writer);
			BufferedWriter groups_bw = new BufferedWriter(groups_writer);

			// Students
			for(Map.Entry<Integer,Student> entry: STUDENTS.entrySet()){
				students_bw.write(entry.getValue().GetFIO());
				students_bw.newLine();
				students_bw.flush();
			}
			// Groups
			for(Map.Entry<Integer,Group> entry: GROUPS.entrySet()){
				groups_bw.write(entry.getValue().GetTITLE());
				groups_bw.newLine();
				groups_bw.flush();
			}
			
			students_writer.close();
			groups_writer.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
		return true;
	}
	
	public String GetCurrentDate(){
		Date date = new Date();
		return date.toString();
	}
	
	public boolean Statistics(String stat_filename) {
		File stat_file = new File(stat_filename);
		try {
			if(!stat_file.exists()){
				System.out.println("File " + stat_filename + " not exist.");
				return false;
			}
			Writer stat_writer = new FileWriter(stat_file);
			BufferedWriter stat_bw = new BufferedWriter(stat_writer);

			// Groups
			for(Map.Entry<Integer,Group> gr: GROUPS.entrySet()){
				stat_bw.write(GetCurrentDate() + " Group " + gr.getKey() + " - " + gr.getValue().Statistics());
				stat_bw.newLine();
				for(Map.Entry<Integer,Student> st: STUDENTS.entrySet()){
					if(st.getValue().GetGROUP() == gr.getValue()) {
						stat_bw.write(GetCurrentDate() + " Student - " + st.getValue().Statistics());
						stat_bw.newLine();
					}
				}
				stat_bw.flush();
			}
			
			stat_writer.close();
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
		return true;
	}
	
	public void Print(){
		for(Map.Entry<Integer,Group> entry: GROUPS.entrySet()){
			entry.getValue().Print();
		}
	}
}

public class DekanatDemo{
	public static void main(String[] args){
		Dekanat DEKANAT = new Dekanat();
		DEKANAT.AddStudentsFromFile(args[0]);
		DEKANAT.AddGroupsFromFile(args[1]);
		DEKANAT.MoveStudentsToGroups();
		DEKANAT.AddRandomMarks();
		DEKANAT.SetHeaders();
		DEKANAT.Print();
		DEKANAT.DeleteStudentsByAverageMark((float)2.5);
		DEKANAT.SaveNewDataToFiles(args[0], args[1]);
		DEKANAT.Statistics(args[2]);
	}
}