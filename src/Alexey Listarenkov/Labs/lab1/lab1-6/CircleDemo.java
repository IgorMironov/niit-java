public class CircleDemo {

    public static void main(String[] args) {

        double earthRadius = 6378.1;
        double poolRadius = 3;
        double trackWidth = 1;
        int trackCost = 1000;
        int fenceCost = 2000;
        double totalFenceCost, totalTrackCost, totalMaterialCost;

        Circle circle1 = new Circle();
        Circle circle2 = new Circle();
        Circle circle3 = new Circle();

        circle1.setRadius(5);
        circle1.findCircumference();
        circle1.findArea();
        circle1.discriptionCircle();

        circle2.setCircumference(35);
        circle2.findRadiusFromFerence();
        circle2.findArea();
        circle2.discriptionCircle();

        circle3.setArea(100);
        circle3.findRadiusFromArea();
        circle3.findCircumference();
        circle3.discriptionCircle();

        System.out.println();
        System.out.println();
        System.out.println("Земля и верёвка");

        Circle earth = new Circle();
        earth.setRadius(earthRadius);
        earth.findCircumference();
        earth.findArea();
        earth.discriptionCircle();

        Circle earth1 = new Circle();
        earth1.setCircumference(earth.getCircumference() + 0.001);
        earth1.findRadiusFromFerence();
        earth1.findArea();
        earth1.discriptionCircle();

        System.out.println("Величина зазора равна:" + (earth1.getRadius()-earth.getRadius())*1000 +" метров");

        System.out.println();
        System.out.println();
        System.out.println("Бассейн");

        Circle  pool = new Circle();
        pool.setRadius(poolRadius);
        pool.findCircumference();
        pool.findArea();
        pool.discriptionCircle();

        System.out.println("Бассейн плюс дорожка");
        Circle outTrack = new Circle();
        outTrack.setRadius(poolRadius+1);
        outTrack.findCircumference();
        outTrack.findArea();
        outTrack.discriptionCircle();

        System.out.println();

        BuildingMaterial fence = new BuildingMaterial();
        fence.setUnitCost(fenceCost);
        totalFenceCost = fence.getUnitCost()*pool.getCircumference();

        BuildingMaterial track = new BuildingMaterial();
        track.setUnitCost(trackCost);
        totalTrackCost  = track.getUnitCost()*(outTrack.getArea()-pool.getArea());

        totalMaterialCost = totalFenceCost + totalTrackCost;

        System.out.println("Стоимость материалов составляет:");
        System.out.println("ограждение : "+ totalFenceCost);
        System.out.println("дорожка    : "+ totalTrackCost);
        System.out.println("итого      : "+ totalMaterialCost);
    }


}

class Circle {
    private double radius;  //- радиус
    private double circumference; //- длина окружности
    private double area;    //- площадь круга

    public void setRadius(double r) {
        this.radius = r;
    }

    public void setCircumference(double c) {
        this.circumference = c;
    }

    public void setArea(double s) {
        this.area = s;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getCircumference() {
        return this.circumference;
    }

    public double getArea() {
        return this.area;
    }

    public void findRadiusFromFerence() {
        radius = circumference / (2 * Math.PI);
    }

    public void findRadiusFromArea() {
        radius = Math.sqrt(area / Math.PI);
    }

    public void findCircumference() {
        circumference = 2 * Math.PI * radius;
    }

    public void findArea() {
        area = Math.PI * radius * radius;
    }

    public void discriptionCircle() {
        System.out.println("-----------------------");
        System.out.println("| radius        = " + radius);
        System.out.println("| circumference = " + circumference);
        System.out.println("| area          = " + area);
    }
}

class BuildingMaterial {
    private int unitCost;
    
    public void setUnitCost(int cost){
        this.unitCost=cost;
    }
    public int  getUnitCost(){
        return this.unitCost;
    }
    public double totalCost(double value){
        return unitCost*value;
    }
}