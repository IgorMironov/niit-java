import javafx.concurrent.Task;
import static java.lang.Thread.sleep;

public class Automata {

    enum STATES {
        OFF, WAIT, ACCEPT, CHECK, COOK, TRANSFER
    }

    private int cash; // текущая сумма, внесённая пользователем
    private String[] menu = {"Моккаччино", "Ристретто", "Черный кофе",
            "Двойной шоколад", "Эспрессо", "Американо", "Чай лимонный",
            "Шоколад с молоком", "Кофе с шоколадом"}; // массив названий напитков
    private int[] price = {25, 35, 30, 45, 30, 35, 25, 40, 30}; // массив цен напитков (соответствует массиву menu)
    private int maxPrice; // стоимость самого дорого продукта. при внесении пользователем суммы, превышающей maxPrice, лишние деньги возвращаются ему, начиная со следующей внесённой денежной единицы
    private int[] coins = {5, 10, 50, 100}; // номинал принимаемых денежных знаков
    private int[] valueCoins = {10, 10, 5, 0}; // начальный запас монет и купюр разного достоинства
    private int[] tempValueCoins = new int[coins.length]; // временный буфер, в который попадают внесённые покупателем деньги. хранятся до окончательного расчета, после чего выдаётся сдача, либо внесённые деньги возвращаются. Остаток поступает в кассу автомата
    private STATES state = STATES.OFF;  // текущее состояние автомата
    private String selectedMenuItem; // выбранный пользователем напиток
    //private SimpleStringProperty message = new SimpleStringProperty(""); // для отображения текущего состояния на дисплее
    private String message;
    public Task task = null;



    public Automata() {
        on();
    }

    public int getCash() {
        return cash;
    }

    public String[] getMenu() {
        return menu;
    }

    public int[] getPrice() {
        return price;
    }

    public STATES getState() {
        return state;
    }

    public String getStateString() {
        return state.toString();
    }

    public int[] getCoins() {
        return coins;
    }

    public int[] getValueCoins() {
        return valueCoins;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public String getMessage() {
        return message;
    }

    // включение автомата
    private void on() {
        if (this.state == STATES.OFF) {
            this.state = STATES.WAIT;
            findMaxPrice();
            printState();
        }
    }


    // определение стоимости самого дорогого напитка
    private void findMaxPrice() {
        for (int i = 0; i < this.price.length; i++) {
            if (price[i] > this.maxPrice) this.maxPrice = price[i];
        }
    }

    // выключение автомата
    public void off() {
        if (this.state == STATES.WAIT) {
            this.state = STATES.OFF;
            printState();
        }
    }

    public int[] getTempValueCoins() {
        return tempValueCoins;
    }

    // занесение денег на счёт пользователем
    public int coin(int coin) {

        for (int i = 0; i < this.coins.length; i++) {
            // если внесённая денежная единица валидна и автомат в состоянии WAIT либо ACCEPT,
            // то к уже внесённой сумме денег прибавляется только что внесённая
            if (coin == this.coins[i] && (this.state == STATES.WAIT || this.state == STATES.ACCEPT)) {
                this.state = STATES.ACCEPT;
                this.cash += coin;
                printState();
                int j = 0; // индекс номинала внесённой монеты или купюры
                // увеличивается счетчик соответствующих купюр или монет
                switch (coin) {
                    case 5:
                        tempValueCoins[j]++;
                        break;
                    case 10:
                        j = 1;
                        tempValueCoins[j]++;
                        break;
                    case 50:
                        j = 2;
                        tempValueCoins[j]++;
                        break;
                    case 100:
                        j = 3;
                        tempValueCoins[j]++;
                }
                // вернуть внесённую купюру или монету если итоговая сумма больше максимальной стоимости продукта и если после возврата игтоговая сумма не станет меньше максимальной стоимости продукта
                if (this.cash > maxPrice && (this.cash - coin) >= maxPrice) {
                    tempValueCoins[j]--; // уменьшается счетчик соответствующих купюр или монет
                    this.cash -= coin; // уменьшается внесённая сумма
                    printState();
                    returnCoin(i); // визуализация возврата денег
                }
            } else returnCoin(this.coins.length); // иначе вернуть невалидные деньги
        }
        return this.cash;
    }

    // если внесённая денежная единица не валидна, то она сразу возвращается пользователю (появляется в окне выдачи сдачи.) реализовать при написании графического интерфейса
    // визуализация соответствует номиналу возвращаемой денежной единицы. для этой цели используется coinIndex
    private void returnCoin(int coinIndex) {
        // coinIndex - индекс номинала денежного знака
        //   message = new SimpleStringProperty("Внесённая сумма превышает стоимость самого дорогого напитка. Пожалуйста, заберите лишние деньги");

    }

    //отмена сеанса обслуживания пользователем
    public void cancel() {

        if (this.state == STATES.CHECK || this.state == STATES.ACCEPT) {
            giveChange(1); // при отмене сеанса обслуживания, либо, если пользователь выбрал напиток, но внёс недостаточную сумму, пользователю возвращаются внесённые деньги
            this.state = STATES.WAIT;
            printState();
        }

    }


    //выбор напитка пользователем
    public boolean choice(String menuItem) throws InterruptedException {
        if (this.state == STATES.ACCEPT) {
            this.state = STATES.CHECK;
            this.selectedMenuItem = menuItem;
            printState();
            return check();
        }
        printState();
        return false;
    }

    public String getSelectedMenuItem() {
        return selectedMenuItem;
    }

    // проверка наличия необходимой суммы
    private boolean check() throws InterruptedException {
        if (this.state == STATES.CHECK) {
            if (this.cash >= this.maxPrice) {
                giveChange(2); // выдать сдачу
                //          cook(); // готовить напиток
                return true;
            } else {
                this.message = new String("Внесена недостаточная сумма. Заберите деньги.");
                // controller.lblAdditional.textProperty().bind(new SimpleStringProperty("Внесена недостаточная сумма. Заберите деньги."));
                giveChange(1);
                // return false;
                // cancel();
                //  controller.lblAdditional.textProperty().bind(new SimpleStringProperty(""));
            }
        }
        return false;
    }

    // выдать сдачу
    private void giveChange(int eventIndex) {

        // eventIndex = 1 - отмена пользователем сеанса обслуживания, либо выбран напиток, но внесена недостаточная сумма. возвращаются все внесённые деньги
        // eventIndex = 2 - пользователь внёс достаточную сумму, выбрал напиток. возврат суммы, превышающей стоимость напитка

        if (eventIndex == 1) {
            this.cash = 0;
            for (int i = 0; i < this.tempValueCoins.length; i++) {
                while (this.tempValueCoins[i] != 0) {
                    this.tempValueCoins[i]--;
                    returnCoin(i);
                }
            }
        }

        if (eventIndex == 2) {
            // добавить в кассу автомата внесённые пользователем деньги и очистить временный буфер
            for (int i = 0; i < this.tempValueCoins.length; i++) {
                this.valueCoins[i] += this.tempValueCoins[i];
                this.tempValueCoins[i] = 0;
            }
            this.message = new String("Заберите сдачу");
            //  выдать сдачу
            int priceIndex = searchPriceForMenuItem(menu, selectedMenuItem);
            for (int i = this.valueCoins.length - 1; i >= 0; i--) {

                while (this.valueCoins[i] != 0 && (this.cash - this.coins[i] >= this.price[priceIndex])) {
                    this.cash -= this.coins[i];
                    this.valueCoins[i]--;
                    returnCoin(i);
                    if (this.cash == this.price[priceIndex]) break;
                }
                if (this.cash == this.price[priceIndex]) {
                    break;
                    // если в кассе автомата закончились деньги для сдачи
                } else if (this.cash > this.price[priceIndex] && i == 0 && this.valueCoins[0] == 0) {
                    transferMoneyToPhoneNumber(this.cash - this.price[priceIndex]);
                }
            }
            this.cash = 0;
        }

    }

    // если в кассе автомата недостаточно дененг для сдачи, перевести сдачу на телефонный номер
    private void transferMoneyToPhoneNumber(int moneyToPhoneNumber) {
        if (this.state == STATES.CHECK) {
            this.state = STATES.TRANSFER;
            printState();
        }
    }

    private int searchPriceForMenuItem(String[] menu, String selectedMenuItem) {
        for (int i = 0; i < menu.length; i++) {
            if (selectedMenuItem.equals(menu[i]))
                return i;
        }
        return -1;
    }

    //имитация процесса приготовления напитка;
    public void cook() throws InterruptedException {
        if (this.state == STATES.CHECK || this.state == STATES.TRANSFER) {
            this.state = STATES.COOK;
        }
        final int max = 10000000;
        for (int i = 1; i <= max; i++) {

        }

        printState();

        task = new Task<Void>() {
            @Override
            public Void call() throws InterruptedException {
                sleep(500);
                final int max = 10000000;
                for (int i = 1; i <= max; i++) {
                    if (isCancelled()) {
                        break;
                    }
                    updateProgress(i, max);
         //           controller.printMessage(message);
                }
                sleep(500);
                updateProgress(0, 0);
                return null;
            }

        };
        // finish();
    }


    // отображение меню с напитками и ценами для пользователя
    public String[][] printMenu() {
        String[][] menuWithPrices = new String[this.menu.length][this.price.length];
        for (int i = 0; i < menuWithPrices.length; i++) {
            menuWithPrices[i][0] = menu[i];
            menuWithPrices[i][1] = Integer.toString(price[i]);
        }
        return menuWithPrices;
    }

    //преобразует код текущего состояния в вид, удобный для пользователя;
    private void printState() {
        switch (this.state) {
            case OFF:
                this.message = "";

                break;
            case COOK:
                this.message = "Приготовление \nнапитка";
                break;
            case WAIT:
                this.message = "Внесите деньги";
                break;
            case CHECK:
                this.message = "Выбран  " + this.selectedMenuItem;
                break;
            case ACCEPT:
                this.message = "Кредит          " + this.cash;
                break;
            case TRANSFER:
                this.message = "Введите номер телефона для перевода сдачи";
                break;
        }

    }

    // завершение обслуживания пользователя
    public void finish() {
        if (this.state == STATES.COOK) {
            this.state = STATES.WAIT;
            printState();
        }
    }


}
