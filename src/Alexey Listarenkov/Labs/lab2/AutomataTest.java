import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 *
 */
public class AutomataTest {

    private Automata coffeeMachine;
    private String[] menu = {"Моккаччино", "Ристретто", "Черный кофе",
            "Двойной шоколад", "Эспрессо", "Американо", "Чай лимонный",
            "Шоколад с молоком", "Кофе с шоколадом"}; // массив названий напитков
    private int[] price = {25, 35, 30, 45, 30, 35, 25, 40, 30}; // массив цен напитков (соответствует массиву menu)
    private int[] valueCoins = {10, 10, 5, 0}; // начальный запас монет и купюр разного достоинства

    @Before
    public void setUp() throws Exception {
        coffeeMachine = new Automata();
    }

    @After
    public void takeDown() {
        coffeeMachine = null;
    }

    @Test
    public void testInitialState() {
        assertEquals("WAIT", coffeeMachine.getState().toString());
    }

    @Test
    public void testMenu() {
        assertEquals(menu.length, coffeeMachine.getMenu().length );
        assertEquals(menu[1], coffeeMachine.getMenu()[1] );
    }

    @Test
    public void testPrice() {
        assertEquals(price.length, coffeeMachine.getPrice().length);
        assertEquals(price[1], coffeeMachine.getPrice()[1]);
    }

    @Test
    public void isMenuLengthEqualsPriceLength() {
        assertEquals(coffeeMachine.getPrice().length, coffeeMachine.getMenu().length);
    }


    @Test
    public void isMaxPriceCorrect() {
        assertEquals(45, coffeeMachine.getMaxPrice());
    }

    @Test
    public void testCoin() {
        assertEquals(0, coffeeMachine.coin(1));
        assertEquals("WAIT", coffeeMachine.getState().toString());
        assertEquals(0, coffeeMachine.coin(2));
        assertEquals("WAIT", coffeeMachine.getState().toString());
        assertEquals(0, coffeeMachine.coin(3));
        assertEquals("WAIT", coffeeMachine.getState().toString());
        assertEquals(5, coffeeMachine.coin(5));
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(15, coffeeMachine.coin(10));
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(15, coffeeMachine.coin(20));
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(15, coffeeMachine.coin(30));
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(65, coffeeMachine.coin(50));
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(65, coffeeMachine.coin(100));
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(65, coffeeMachine.coin(150));
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
    }

    @Test
    public void testIncreaseTempValueCoin1() {
        int[] testValueCoins;

        coffeeMachine.coin(1);
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(0, coffeeMachine.getCash());

        coffeeMachine.coin(2);
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(0, coffeeMachine.getCash());


        coffeeMachine.coin(5);
        testValueCoins = new int[]{1, 0, 0, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(5, coffeeMachine.getCash());


        coffeeMachine.coin(50);
        testValueCoins = new int[]{1, 0, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(55, coffeeMachine.getCash());


        coffeeMachine.coin(10);
        testValueCoins = new int[]{1, 0, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(55, coffeeMachine.getCash());


        coffeeMachine.coin(7);
        testValueCoins = new int[]{1, 0, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(55, coffeeMachine.getCash());
    }

    @Test
    public void testIncreaseTempValueCoin2() {
        int[] testValueCoins;

        coffeeMachine.coin(10);
        testValueCoins = new int[]{0, 1, 0, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(10, coffeeMachine.getCash());

        coffeeMachine.coin(10);
        testValueCoins = new int[]{0, 2, 0, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(20, coffeeMachine.getCash());

        coffeeMachine.coin(5);
        testValueCoins = new int[]{1, 2, 0, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(25, coffeeMachine.getCash());

        coffeeMachine.coin(5);
        testValueCoins = new int[]{2, 2, 0, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(30, coffeeMachine.getCash());

        coffeeMachine.coin(50);
        testValueCoins = new int[]{2, 2, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(80, coffeeMachine.getCash());

        coffeeMachine.coin(5);
        testValueCoins = new int[]{2, 2, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(80, coffeeMachine.getCash());

        coffeeMachine.coin(10);
        testValueCoins = new int[]{2, 2, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(80, coffeeMachine.getCash());
    }

    @Test
    public void testIncreaseTempValueCoin3() {
        int[] testValueCoins;

        coffeeMachine.coin(50);
        testValueCoins = new int[]{0, 0, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(50, coffeeMachine.getCash());

        coffeeMachine.coin(10);
        testValueCoins = new int[]{0, 0, 1, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(50, coffeeMachine.getCash());
    }


    @Test
    public void testStateIfCancel1() throws InterruptedException {
        coffeeMachine.coin(10);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        coffeeMachine.cancel();
        assertEquals("WAIT", coffeeMachine.getState().toString());
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(0, coffeeMachine.getCash());
    }

    @Test
    public void testStateIfCancel2() throws InterruptedException {
        coffeeMachine.coin(10);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(10, coffeeMachine.getCash());
        assertEquals(1, coffeeMachine.getTempValueCoins()[1]);
        coffeeMachine.coin(10);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(20, coffeeMachine.getCash());
        assertEquals(2, coffeeMachine.getTempValueCoins()[1]);
        coffeeMachine.coin(5);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(25, coffeeMachine.getCash());
        assertEquals(2, coffeeMachine.getTempValueCoins()[1]);
        assertEquals(1, coffeeMachine.getTempValueCoins()[0]);
        coffeeMachine.cancel();
        assertEquals("WAIT", coffeeMachine.getState().toString());
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(0, coffeeMachine.getCash());
    }

    @Test
    public void testStateIfCancel3() throws InterruptedException {
        coffeeMachine.coin(5);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(5, coffeeMachine.getCash());
        assertEquals(1, coffeeMachine.getTempValueCoins()[0]);
        coffeeMachine.coin(5);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(10, coffeeMachine.getCash());
        assertEquals(2, coffeeMachine.getTempValueCoins()[0]);
        coffeeMachine.coin(5);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(15, coffeeMachine.getCash());
        assertEquals(3, coffeeMachine.getTempValueCoins()[0]);

        coffeeMachine.coin(10);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(25, coffeeMachine.getCash());
        assertEquals(3, coffeeMachine.getTempValueCoins()[0]);
        assertEquals(1, coffeeMachine.getTempValueCoins()[1]);
        coffeeMachine.coin(10);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(35, coffeeMachine.getCash());
        assertEquals(3, coffeeMachine.getTempValueCoins()[0]);
        assertEquals(2, coffeeMachine.getTempValueCoins()[1]);
        coffeeMachine.coin(10);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(45, coffeeMachine.getCash());
        assertEquals(3, coffeeMachine.getTempValueCoins()[0]);
        assertEquals(3, coffeeMachine.getTempValueCoins()[1]);

        coffeeMachine.coin(10);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(45, coffeeMachine.getCash());
        assertEquals(3, coffeeMachine.getTempValueCoins()[0]);
        assertEquals(3, coffeeMachine.getTempValueCoins()[1]);

        coffeeMachine.cancel();

        assertEquals("WAIT", coffeeMachine.getState().toString());
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(0, coffeeMachine.getCash());
    }

    @Test
    public void testIsNotEnoughMoney() throws InterruptedException {

        coffeeMachine.coin(5);
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        assertEquals(5, coffeeMachine.getCash());
        assertEquals(1, coffeeMachine.getTempValueCoins()[0]);

        coffeeMachine.choice(menu[0]);//25
        assertEquals("WAIT", coffeeMachine.getState().toString());
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        assertEquals(0, coffeeMachine.getCash());
    }

    @Test
    public void testGiveChangeAndCook1() throws InterruptedException {
        coffeeMachine.coin(5);
        coffeeMachine.coin(5);
        coffeeMachine.coin(10);
        coffeeMachine.coin(10);
        coffeeMachine.coin(50);
        int[] testValueCoins = {2, 2, 1, 0};

        assertEquals(80, coffeeMachine.getCash());
        assertEquals("ACCEPT", coffeeMachine.getState().toString());
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }

        coffeeMachine.choice(menu[0]);//25
        assertEquals(0, coffeeMachine.getCash());
        assertEquals("WAIT", coffeeMachine.getState().toString());
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        testValueCoins = new int[]{11, 12, 5, 0};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }
    }


    @Test
    public void testGiveChangeAndCook2() throws InterruptedException {

        coffeeMachine.coin(100);
        int[] testValueCoins = {0, 0, 0, 1};

        assertEquals(100, coffeeMachine.getCash());
        assertEquals("ACCEPT", coffeeMachine.getState().toString());

        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getTempValueCoins()[i]);
        }

        coffeeMachine.choice(menu[3]); //40
        // price = {25, 35, 30, 45, 30, 35, 25, 40, 30}
        assertEquals(menu[3], coffeeMachine.getSelectedMenuItem());
        assertEquals(45, coffeeMachine.getPrice()[3]);
        assertEquals(0, coffeeMachine.getCash());
        assertEquals("WAIT", coffeeMachine.getState().toString());
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(0, coffeeMachine.getTempValueCoins()[i]);
        }
        testValueCoins = new int[]{9, 10, 4, 1};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(testValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }
    }

    @Test
    public void isNotEnoughMoneyForChange() throws InterruptedException {
        // price = {25, 35, 30, 45, 30, 35, 25, 40, 30}
        coffeeMachine.coin(100);
        // int[] testValueCoins = {0, 0, 0, 1};
        int[] machineValueCoins = {10, 10, 5, 0};
        coffeeMachine.choice(menu[0]);
        machineValueCoins = new int[]{9, 8, 4, 1};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals( machineValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }

        coffeeMachine.coin(100);
        coffeeMachine.choice(menu[6]);
        assertEquals(25, coffeeMachine.getPrice()[6]);
        machineValueCoins = new int[]{8, 6, 3, 2};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals( machineValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }

        coffeeMachine.coin(100);
        coffeeMachine.choice(menu[8]);
        assertEquals(30, coffeeMachine.getPrice()[8]);
        machineValueCoins = new int[]{8, 4, 2, 3};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals( machineValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }

        coffeeMachine.coin(100);
        coffeeMachine.choice(menu[7]);
        assertEquals(40, coffeeMachine.getPrice()[7]);
        machineValueCoins = new int[]{8, 3, 1, 4};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals( machineValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }

        coffeeMachine.coin(100);
        coffeeMachine.choice(menu[5]);
        assertEquals(35, coffeeMachine.getPrice()[5]);
        machineValueCoins = new int[]{7, 2, 0, 5};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(machineValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }

        coffeeMachine.coin(100);
        coffeeMachine.choice(menu[4]);
        assertEquals(30, coffeeMachine.getPrice()[4]);
        machineValueCoins = new int[]{0, 0, 0, 6};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(machineValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }
        assertEquals("WAIT", coffeeMachine.getState().toString());

        coffeeMachine.coin(100);
        coffeeMachine.choice(menu[4]);
        assertEquals(30, coffeeMachine.getPrice()[4]);
        machineValueCoins = new int[]{0, 0, 0, 7};
        for (int i = 0; i < valueCoins.length; i++) {
            assertEquals(machineValueCoins[i], coffeeMachine.getValueCoins()[i]);
        }
        assertEquals("WAIT", coffeeMachine.getState().toString());
    }

    @Test
    public void testOff() {
        coffeeMachine.off();
        assertEquals("OFF", coffeeMachine.getState().toString());
    }

}