import javax.swing.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StaffDemo {
    static ArrayList<Employee> list = new ArrayList<>(); // массив всех сотрудников
    static int workedTime = 15; // отчётный период - 15 дней
    static ArrayList<DevProject> projectsList = new ArrayList<>();
    static DevProject project1 = new DevProject("Супер проект", 60000, 50000, 100000, 150000, 70000);
    static DevProject project2 = new DevProject("Супер проект 2", 20000, 10000, 30000, 40000, 50000);
    static String outFile = "staff.txt";
    static String xmlFile = "staff.xml";

    public static void main(String[] args) {
        String[] st = {"Быбина Алена Евгеньевна", "Тернова Анна Андреевна", "Растригина Ангелина Ивановна", "Дегтярева Елена Вадимовна",
                "Цветкова Светлана Евгеньевна", "Майков Денис Васильевич", "Кабатова Алина Викторовна", "Хохлов Данил Олегович", "Арефьев Антон Владимирович",
//                "Содомовский Никита Максимович", "Молькова Анастасия Дмитриевна", "Маленёва Анастасия Павловна", "Голубков Матвей Германович", "Мартынова Наталия Андреевна",
//                "Крашенинников Владислав Александрович", "Советов Михаил Николаевич", "Пярина Екатерина Радионовна", "Усов Алексей Николаевич", "Пчельникова Наталья Дмитриевна",
//                "Жигалева Анастасия Алексеевна", "Клищ Татьяна Игоревна", "Демина Александра Вячеславовна", "Новосельцева Анастасия Александровна", "Мохнина Евгения Константиновна",
//                "Злобина Елена Валерьевна", "Бобкова Дарья Дмитриевна", "Константинова Анна Романовна", "Тингаева Полина Сергеевна", "Крамков Вячеслав Андреевич", "Суренкова Алла Александровна",
//                "Лопатин Михаил Павлович", "Парфенычева Екатерина Александровна", "Калягина Кристина Александровна", "Вьюнова Дарья Владимировна", "Соколова Любовь Сергеевна", "Кусакина Анна Кирилловна",
//                "Свиридюк Виктория Владимировна", "Синицына Дарья Алексеевна", "Кутергина Екатерина Вадимовна", "Земляк Виктория Михайловна", "Шакирова Карина Александровна",
                // "Лепигина Анастасия Анатольевна", "Савкина Виктория Александровна",
                "Сорокин Семен Александрович"};
//******1. создание штата и запись в xml*****************************************************************************

//// проект 1
//        project1.setProjectCompletion(50); // проект за отчётный период завершён на 50%
//// проект 2
//        project2.setProjectCompletion(20); // проект за отчётный период завершён на 50%
//        projectsList.add(project1);
//        projectsList.add(project2);
//
//        // создание исходного списка сотрудников
//        for (int i = 0; i < st.length; i++) {
//            System.out.println(st[i]);
//            System.out.println("Выберите должность сотрудника: \n" +
//                    "1. Cleaner\n" +
//                    "2. Driver \n" +
//                    "3. Programmer\n" +
//                    "4. Tester\n" +
//                    "5. Team_Leader\n" +
//                    "6. Project_Manager\n" +
//                    "7. Senior_Manager");
//
//            Scanner scanner = new Scanner(System.in);
//            String choice = scanner.nextLine();
//            createEmployee(String.valueOf(i + 1), st[i], choice);
//        }
//          //сохранение в xml
//        StaffWrapper staffWrapper = new StaffWrapper();
//        staffWrapper.setEmployees(list);
//        staffWrapper.setProject(projectsList);
//        marshall(staffWrapper);


//*****1.1. сохранение в тхт ***************************************************************************************
//        // сохранение в txt
//        try {
//            saveFile();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//*****2. загрузка штата из подготовленного ранее xml***************************************************************
        // загрузить объект из xml
        StaffWrapper staffWrapper = new StaffWrapper();
        try {
            staffWrapper = (StaffWrapper) unmarshal();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        list = staffWrapper.getEmployees();
        projectsList = staffWrapper.getProject();
//******************************************************************************************************************

        //вывод информации на консоль
//        printStaff(list);

// вывод информации в окно графического интерфейса
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
                createGUI(list);
            }
        });

    }

    public static void saveFile() throws IOException {
        FileWriter writer = new FileWriter(outFile);
        for (int i = 0; i < list.size(); i++) {
            writer.write(list.get(i).getId() + ";" + list.get(i).getName() + ";" + list.get(i).getPosition() + ";" + list.get(i).getPayment() + "\n");
        }
        writer.close();
    }

    public static void createEmployee(String id, String name, String choice) {

        switch (choice) {
            case "1":
                Cleaner cl = new Cleaner(id, name);
                cl.setWorkTime(workedTime);
                list.add(cl);
                break;
            case "2":
                Driver dr = new Driver(id, name);
                dr.setWorkTime(workedTime);
                list.add(dr);
                break;
            case "3":
                Programmer programmer = new Programmer(id, name);
                DevProject project = choiceProject();
                project.setProjectParticipants(programmer);
                programmer.setWorkTime(workedTime);
                programmer.setProject(project);
                list.add(programmer);
                break;
            case "4":
                Tester tester = new Tester(id, name);
                project = choiceProject();
                project.setProjectParticipants(tester);
                tester.setWorkTime(workedTime);
                tester.setProject(project);
                list.add(tester);
                break;
            case "5":
                TeamLeader teamLeader = new TeamLeader(id, name);
                project = choiceProject();
                project.setProjectParticipants(teamLeader);
                teamLeader.setWorkTime(workedTime);
                teamLeader.setProject(project);
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getPosition() == Employee.POSITIONS.Programmer) {
                        Programmer pr = (Programmer) list.get(i);
                        if (pr.getProject() != null) {
                            System.out.println("Добавить программиста ID: " + pr.getId() + " ФИО: " + pr.getName() + " в команду к " + teamLeader.getName() + " ?");
                            System.out.println("Y / N : ");
                            Scanner scanner = new Scanner(System.in);
                            String yesORno = scanner.nextLine();
                            if (yesORno.equalsIgnoreCase("y")) {
                                teamLeader.setProgrammers(pr);
                            } else continue;
                        }

                    }
                }
                list.add(teamLeader);
                break;
            case "6":
                ProjectManager projectManager = new ProjectManager(id, name);
                project = choiceProject();
                project.setProjectParticipants(projectManager);
                projectManager.setProject(project);
                projectManager.setTeam();
                projectManager.setWorkTime(workedTime);
                list.add(projectManager);
                break;
            case "7":
                SeniorManager seniorManager = new SeniorManager(id, name);
                project = choiceProject();
                project.setProjectParticipants(seniorManager);
                seniorManager.setProject(project);

                project = choiceProject();
                project.setProjectParticipants(seniorManager);
                seniorManager.setProject(project);
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getPosition() == Employee.POSITIONS.Project_Manager) {
                        seniorManager.addManager((ProjectManager) list.get(i));
                    }
                }
                seniorManager.calcTeamSize();
                list.add(seniorManager);
                break;
            default:
                cl = new Cleaner(id, name);
                cl.setWorkTime(workedTime);
                list.add(cl);
        }
    }

    public static DevProject choiceProject() {
        Project project;
        System.out.println("Выберите проект для сотрудника: \n " +
                "1. " + project1.getName() + "\n" +
                "2. " + project2.getName() + "\n");
        Scanner scanner = new Scanner(System.in);
        String choice = scanner.nextLine();
        switch (choice) {
            case "1":
                return project1;
            case "2":
                return project2;
        }
        return project1;
    }


    public static void printStaff(ArrayList<Employee> list) {
        int total = 0;
        for (Employee e : list) {
            String[] str = e.fieldsToStringArray();
            for (String s : str) {
                System.out.print(s + "       ");
            }
            System.out.println();
            total += e.getPayment();
        }
        System.out.println("Итого к выдаче : " + total);
    }

    public static void createGUI(ArrayList<Employee> list) {
        JFrame frame = new JFrame("Ведомость");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String[] columnNames = {
                "ID",
                "ФИО",
                "Должность",
                "Участие в проекте",
                "% выполнения проекта",
                "Оплата за проект",
                "Оплата за руководство",
                "Дней отработано",
                "повремённая оплата",
                "Сумма к выдаче"
        };
        int total = 0; // итоговая сумма к выплате
        String[][] data = new String[list.size()+1][columnNames.length]; // [строки][столбцы]
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i).fieldsToStringArray();
            total += list.get(i).getPayment(); //итоговая сумма
        }
        data[list.size()]= new String[]{"Итого к выплате: ", null, null, null, null, null, null, null, null, String.valueOf(total)};

        JTable table = new JTable(data, columnNames);


        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setSize(800, 200);
        frame.getContentPane().add(scrollPane);
        frame.setPreferredSize(new Dimension(1200, 200));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    // генерация XML и запись в файл
    public static void marshall(Object o) {
        File outFile = new File(xmlFile);
        try {
            JAXBContext context = JAXBContext.newInstance(StaffWrapper.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            marshaller.marshal(o, System.out); // вывод на экран
            marshaller.marshal(o, outFile); // вывод в файл

            System.out.println("\nФайл " + outFile.toString() + " сгенерирован");
        } catch (JAXBException exception) {
            Logger.getLogger(StaffWrapper.class.getName()).log(Level.SEVERE, "marshall threw JAXBException", exception);
        }
    }

    // загружает объект из xml файла
    public static Object unmarshal() throws JAXBException {
        File inFile = new File(xmlFile);
        Object object = new Object();
        try {
            JAXBContext context = JAXBContext.newInstance(StaffWrapper.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            object = unmarshaller.unmarshal(inFile);

            System.out.println("Objects created from XML:");
            System.out.println();
        } catch (JAXBException exception) {
            Logger.getLogger(StaffWrapper.class.getName()).log(Level.SEVERE, "marshall threw JAXBException", exception);
        }
        return object;
    }
}
