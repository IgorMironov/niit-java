import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

// имеет ставку оплаты за рабочее время и бонусы от выполняемого проекта

public class Engineer extends Employee implements WorkTime, Project {
    private int baseAmount; // базовая ставка (стоимость 1 дня работы сотрудника)
    private DevProject project = null; // проект, в котором участвует сотрудник

    public Engineer() {
    }

    public Engineer(String id, String name, POSITIONS position) {
        super(id, name, position);
    }

    public int getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(int baseAmount) {
        this.baseAmount = baseAmount;
    }

    public void setProject(DevProject project) {
        this.project = project;
    }

    public DevProject getProject() {

        return project;
    }

    @Override
    public int getPayment() {
        setPayment(payForTime() + payForProject());
        return super.getPayment();
    }

    @XmlElement(name="maxProjectBonus")
    public int getProjectBonus(){
       return this.project.getBonus(this);
    }

    @Override
    public int payForProject() {
        return (int) this.getProjectBonus() * this.project.getProjectCompletion() / 100;
    }

    @Override
    public int payForTime() {
        return this.getWorkTime() * baseAmount;
    }

    @Override
    public String[] fieldsToStringArray() {
        String[] fields = new String[]{
                this.getId(),                           // "ID"
                this.getName(),                         // "ФИО"
                String.valueOf(this.getPosition()),     // "Должность"
                this.project.getName(),                 // "Участие в проекте"
                String.valueOf(this.project.getProjectCompletion()), // "% выполнения проекта"
                String.valueOf(this.payForProject()),   //"Оплата за проект"
                null,                                   //"Оплата за руководство"
                String.valueOf(this.getWorkTime()),     // "Дней отработано"
                String.valueOf(this.payForTime()),      // "повремённая оплата"
                String.valueOf(this.getPayment())       // "Сумма к выдаче"
        };
        return fields;
    }
}


class Programmer extends Engineer {

    public Programmer(String id, String name) {
        super(id, name, POSITIONS.Programmer);
        this.setBaseAmount(1000);
    }

    public Programmer() {
    }

    public Programmer(String id, String name, POSITIONS team_leader) {
        super(id, name, team_leader);
    }

    @Override
    public int getProjectBonus() {
        return this.getProject().getProgrammerBonus();
    }
}

class Tester extends Engineer {
    public Tester() {
    }

    public Tester(String id, String name) {
        super(id, name, POSITIONS.Tester);
        this.setBaseAmount(800);
    }
}

// имеет ставку оплаты за рабочее время, бонусы от проектов и бонус за руководство
class TeamLeader extends Programmer implements Heading {
    @XmlElement
    private int headAmount = 500; // ставка за одного подчинённого сотрудника
    @XmlElementWrapper(name = "programmers")
    @XmlElement(name = "programmer")
    private ArrayList<Programmer> programmers = new ArrayList<>(); // программисты в подчинении Тим Лидера

    public TeamLeader() {
    }

    public TeamLeader(String id, String name) {
        super(id, name, POSITIONS.Team_Leader);
        this.setBaseAmount(1000);
    }

    public ArrayList<Programmer> getProgrammers() {
        return programmers;
    }

    // добавляет программиста в команду Тим Лидера
    public void setProgrammers(Programmer programmer) {
        this.programmers.add(programmer);
    }

    @Override
    public int payForHeading() {
        return (this.programmers.size() * headAmount);
    }

    @Override
    public int getPayment() {
        return super.getPayment() + payForHeading();
    }

    @Override
    public String[] fieldsToStringArray() {
        String[] fields = new String[]{
                this.getId(),                           // "ID"
                this.getName(),                         // "ФИО"
                String.valueOf(this.getPosition()),     // "Должность"
                this.getProject().getName(),            // "Участие в проекте"
                String.valueOf(this.getProject().getProjectCompletion()), // "% выполнения проекта"
                String.valueOf(this.payForProject()),   //"Оплата за проект"
                String.valueOf(this.payForHeading()),   //"Оплата за руководство"
                String.valueOf(this.getWorkTime()),     // "Дней отработано"
                String.valueOf(this.payForTime()),      // "повремённая оплата"
                String.valueOf(this.getPayment())       // "Сумма к выдаче"
        };

        return fields;
    }
}
