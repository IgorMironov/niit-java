import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

// проект, находящийся в разработке компании
@XmlRootElement(name = "project")
public class DevProject {
    @XmlElement
    private String name; // имя проекта
    private List<Employee> projectParticipants = new ArrayList<>(); // участники проекта
    private int projectCompletion; // процент выполнения проекта за отчётный период

    // бонусы различным категориям участников проекта
    @XmlElement
    private int programmerBonus;
    @XmlElement
    private int testerBonus;
    @XmlElement
    private int teamLeaderBonus;
    @XmlElement
    private int projectManagerBonus;
    @XmlElement
    private int seniorManagerBonus;

    public DevProject() {
    }

    public DevProject(String name, int programmerBonus, int testerBonus, int teamLeaderBonus, int projectManagerBonus, int seniorManagerBonus) {
        this.name = name;
        this.programmerBonus = programmerBonus;
        this.testerBonus = testerBonus;
        this.teamLeaderBonus = teamLeaderBonus;
        this.projectManagerBonus = projectManagerBonus;
        this.seniorManagerBonus = seniorManagerBonus;
    }

    public void setProjectParticipants(Employee employee) {
        if (!(employee instanceof Personal))
            this.projectParticipants.add(employee);
        else System.out.println("Вы пытаетесь включить в проект работника типа "+ employee.getClass().getName() +". Данный тип работника не может принимать участия в проектах");
    }

    public List<Employee> getProjectParticipants() {
        return projectParticipants;
    }

    public String getName() {
        return name;
    }

    public int getProgrammerBonus() {
        return programmerBonus;
    }

    public int getTesterBonus() {
        return testerBonus;
    }

    public int getTeamLeaderBonus() {
        return teamLeaderBonus;
    }

    public int getProjectManagerBonus() {
        return projectManagerBonus;
    }

    public int getSeniorManagerBonus() {
        return seniorManagerBonus;
    }

    public int getBonus(Employee employee) {
        String typeEmployee = employee.getClass().toString();
        int bonus = 0;
        switch (typeEmployee) {
            case "class Programmer":
                bonus = programmerBonus;
                break;
            case "class Tester":
                bonus = testerBonus;
                break;
            case "class TeamLeader":
                bonus = teamLeaderBonus;
                break;
            case "class ProjectManager":
                bonus = projectManagerBonus;
                break;
            case "class SeniorManager":
                bonus = seniorManagerBonus;
                break;
        }
        return bonus;
    }

    // возвращает процент завершения проекта за отчётный период
    public int getProjectCompletion() {
        return projectCompletion;
    }

    // устанавливает процент завершения проекта за отчётный период
    public void setProjectCompletion(int projectCompletion) {
        this.projectCompletion = projectCompletion;
    }
}
