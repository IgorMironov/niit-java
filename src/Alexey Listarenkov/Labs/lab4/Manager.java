import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

// менеджер. получает оплату из денег проекта которым руководит
public class Manager extends Employee implements Project {
    private DevProject project = null; // проект, в котором участвует сотрудник

    public Manager() {
    }

    public Manager(String id, String name, POSITIONS position) {
        super(id, name, position);
    }

    public DevProject getProject() {
        return project;
    }

    public void setProject(DevProject project) {
        this.project = project;
    }

    @Override
    public int payForProject() {
        return this.getProject().getBonus(this)* this.getProject().getProjectCompletion() / 100;
    }
}

// проектный менеджер
class ProjectManager extends Manager implements Heading {
    @XmlElement
    private int headAmount = 300; // ставка за одного подчинённого сотрудника
    @XmlElementWrapper(name = "team")
    @XmlElement(name = "teammate")
    private ArrayList<Engineer> team = new ArrayList<>(); // инженеры в подчинении Project Manager

    public ProjectManager() {
    }

    public ProjectManager(String id, String name) {
        super(id, name, POSITIONS.Project_Manager);
    }

    public ProjectManager(String id, String name, POSITIONS senior_manager) {
        super(id, name, POSITIONS.Senior_Manager);
    }

    public ArrayList<Engineer> getTeam() {
        return team;
    }

    // собирает подчинённых участников проекта : программистов, тестеров, ведущих программистов
    public void setTeam() {
        ArrayList<Engineer> t = new ArrayList<>();
        for (Employee e : this.getProject().getProjectParticipants())
            if (e instanceof Engineer)
                t.add((Engineer) e);
        this.team = t;
    }

    @Override
    public int payForHeading() {
        return (this.team.size() * headAmount);
    }
    @XmlElement(name="maxProjectBonus")
    public int getProjectBonus(){
        return this.getProject().getBonus(this);
    }

    @Override
    public int payForProject() {
        return (int) this.getProjectBonus() * this.getProject().getProjectCompletion() / 100;
    }

    @Override
    public int getPayment() {
        setPayment(payForHeading() + payForProject());
        return super.getPayment();
    }


    @Override
    public String[] fieldsToStringArray() {
        String[] fields = new String[]{
                this.getId(),                           // "ID"
                this.getName(),                         // "ФИО"
                String.valueOf(this.getPosition()),     // "Должность"
                this.getProject().getName(),            // "Участие в проекте"
                String.valueOf(this.getProject().getProjectCompletion()), // "% выполнения проекта"
                String.valueOf(this.payForProject()),   //"Оплата за проект"
                String.valueOf(this.payForHeading()),   //"Оплата за руководство"
                String.valueOf(this.getWorkTime()),     // "Дней отработано"
                null,      // "повремённая оплата"
                String.valueOf(this.getPayment())       // "Сумма к выдаче"
        };

        return fields;
    }
}

//руководитель направления
class SeniorManager extends ProjectManager {
    @XmlElement
    private int headAmount = 1500; // ставка за одного подчинённого сотрудника
    @XmlElementWrapper(name = "projects")
    @XmlElement(name = "project")
    private ArrayList<DevProject> projects = new ArrayList<>();
    private ArrayList<ProjectManager> managers = new ArrayList<>();// сотрудники в подчинении SeniorManager

    private int teamsize;

    public SeniorManager() {
    }

    public SeniorManager(String id, String name) {
        super(id, name, POSITIONS.Senior_Manager);
    }

    public ArrayList<DevProject> getProjects() {
        return projects;
    }

    public void addManager(ProjectManager manager) {
        this.managers.add(manager);
    }

    @Override
    public void setProject(DevProject project) {
        super.setProject(project);
        projects.add(project);
    }

    @Override
    public int payForProject() {
        int payForProjects = 0;
        for (DevProject pr : projects)
            payForProjects += (int) pr.getBonus(this) * pr.getProjectCompletion() / 100;
        return payForProjects;
    }

    @Override
    public int payForHeading() {
       return teamsize*headAmount;
    }

    public int calcTeamSize() {
        int teamSize = 0;
        for (ProjectManager pm : managers) {
            teamSize++;
            teamSize += pm.getTeam().size();
        }
        this.teamsize = teamSize;
        return teamSize;
    }

    @XmlElement
    public int getTeamsize() {
        return teamsize;
    }

    public void setTeamsize(int teamsize) {
        this.teamsize = teamsize;
    }

    @Override
    public String[] fieldsToStringArray() {
        String[] fields = new String[]{
                this.getId(),                           // "ID"
                this.getName(),                         // "ФИО"
                String.valueOf(this.getPosition()),     // "Должность"
                this.getProjectsNamesToString(),            // "Участие в проекте"
                String.valueOf(this.getAverageProjectCompletion()), // "% выполнения проекта"
                String.valueOf(this.payForProject()),   //"Оплата за проект"
                String.valueOf(this.payForHeading()),   //"Оплата за руководство"
                String.valueOf(this.getWorkTime()),     // "Дней отработано"
                null,                                    // "повремённая оплата"
                String.valueOf(this.getPayment())       // "Сумма к выдаче"
        };
        return fields;
    }

    // названия всех проектов в строку
    private String getProjectsNamesToString() {
        String projectsNames = new String();
        for (DevProject pr : projects)
            projectsNames += pr.getName() + "; ";
        return projectsNames;
    }

    // средний процент выполнения проектов
    private int getAverageProjectCompletion() {
        int sumCompletion = 0;
        int numProjects = projects.size();
        if (numProjects != 0) {
            for (DevProject pr : projects)
                sumCompletion += pr.getProjectCompletion();
            return sumCompletion / numProjects;
        }
        return 0;
    }
}