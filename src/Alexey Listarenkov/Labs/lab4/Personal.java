import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

// работник по найму с оплатой за фактич. отабот. время. Имеет ставку за час
@XmlRootElement
public class Personal extends Employee implements WorkTime {

    private int baseAmount; // базовая ставка (стоимость 1 дня работы сотрудника)

    public Personal() {
        this("","",null);
    }

    public Personal(String id, String name, POSITIONS position) {
        super(id, name, position);
    }

    public int getBaseAmount() {
        return baseAmount;
    }
    @XmlElement
    public void setBaseAmount(int baseAmount) {
        this.baseAmount = baseAmount;
    }

    @Override
    public String[] fieldsToStringArray() {
        String[] fields = new String[]{
                this.getId(),                           // "ID"
                this.getName(),                         // "ФИО"
                String.valueOf(this.getPosition()),     // "Должность"
                null,                                   // "Участие в проекте"
                null,                                   // "% выполнения проекта"
                null,                                   //"Оплата за проект"
                null,                                   //"Оплата за руководство"
                String.valueOf(this.getWorkTime()),     // "Дней отработано"
                String.valueOf(this.getPayment()),      // "повремённая оплата"
                String.valueOf(this.getPayment())       // "Сумма к выдаче"
        };
        return fields;
    }

    @Override
    public int getPayment() {
        setPayment(payForTime());
        return super.getPayment();
    }

    @Override
    public int payForTime() {
        setPayment(this.getWorkTime() * baseAmount);
        return this.getWorkTime() * baseAmount;
    }
}

// работник клининговой службы (уборщица)
class Cleaner extends Personal {
    public Cleaner(String id, String name) {
        super(id, name, POSITIONS.Cleaner);
        this.setBaseAmount(500);
    }

}

// водитель
class Driver extends Personal {
    public Driver() {
    }

    public Driver(String id, String name) {
        super(id, name, POSITIONS.Driver);
        this.setBaseAmount(800);
    }
}