public class Automata {

    enum STATES {OFF, ON, WAIT, ACCEPT, CHECK, COOK}

    public STATES state;

    private int cash = 0;
    private String[] menu = new String[]{"Rosabaya de Colombia", "Capriccio", "Livanto", "Arpeggio", "Volluto", "Dulsão do Brasil"};
    private int[] prices = new int[]{20, 20, 25, 30, 35, 40};
    private int coins[] = new int[]{10, 5, 2, 1};
    private int choice = 0;
    private int indexofdrink = 0;
    private int balance = 0;

    Automata() {
        this.cash = 0;
        this.state = STATES.OFF;
        printState();
    }

    public String on() {
        if (this.state == STATES.OFF) {
            this.state = STATES.ON;
            this.state = STATES.WAIT;
        }
        return state.toString();
    }

    public String off() {
        if (state != STATES.OFF) {
            state = STATES.OFF;
        }
        return state.toString();
    }

    public int money() {
        for (int j = 0; j <= (5 + (int) (Math.random() * 10)); j++) {
            balance += coins[(int) (Math.random() * coins.length)];
        }
        return balance;
    }

    public int coin(int coin) {
        if (state == STATES.WAIT || state == STATES.ACCEPT) {
            cash += coin;
            System.out.println("The amount of money" + "\t" + cash + "\t" + "was thrown inside");
            if (this.state != STATES.ACCEPT)
                this.state = STATES.ACCEPT;
            return 0;
        } else
            return 1;
    }

    public int PrintMenu() {
        if (state == STATES.WAIT) {
            System.out.println("Menu is");
            for (int i = 0; i < menu.length; i++) {
                System.out.printf("%-4d%-30s%-10d%n", (i + 1), menu[i], prices[i]);
            }
            return 0;
        } else {
            return 1;
        }
    }

    public void printState() {
        switch (state) {
            case OFF:
                System.out.println("Vending machine is in turned off state!");
                break;
            case ON:
                System.out.println("Vending machine is in turned on state!");
                break;
            case WAIT:
                System.out.println("Vending machine is in wait state!");
                break;
            case ACCEPT:
                System.out.println("Vending machine is in accept state");
                break;
            case CHECK:
                System.out.println("Vending machine is in balance cheking state");
                break;
            case COOK:
                System.out.println("Vending machine is in cook state!");
                break;
        }
    }

    public String choice() {
        if (state == STATES.ACCEPT) {
            this.indexofdrink = ((int) (Math.random() * menu.length));
            System.out.println("\nCustomer have chosen: " + (indexofdrink + 1) + " " + menu[indexofdrink]);
            if (check(this.cash, indexofdrink) == true) {
                this.state = STATES.CHECK;
                System.out.println("Money is enough");
            } else if (check(this.cash, indexofdrink) == false) {
                System.out.println("Not enough money");
                this.state = STATES.OFF;
            }
        }
        return state.toString();
    }


    public boolean check(int cash, int indexofdrink) {
        if (cash >= prices[indexofdrink]) {
            return true;
        } else {
            this.state = STATES.WAIT;
            return false;
        }
    }


    public String cancel() {
        if (state != STATES.WAIT) {
            cash = 0;
            choice = 0;
            state = STATES.WAIT;
        }
        return state.toString();
    }


    public String cook() {
        if (this.state == STATES.CHECK) {
            this.state = STATES.COOK;
        }
        return state.toString();
    }


    public int change() {
        if (check(this.cash, indexofdrink) == true)
            System.out.println("Change is:" + "\t" + (cash - prices[indexofdrink]));
        return 0;
    }
}