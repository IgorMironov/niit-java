package Andrei;
//расчет оплаты исходя из участия в проекте (бюджет проекта делится пропорционально персональному вкладу).
public interface IProject {
    double calcProjectPayment(int projectBudget, double projectContribution);
}
