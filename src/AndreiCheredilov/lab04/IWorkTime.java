package Andrei;
//расчет оплаты исходя из отработанного времени (часы умножаются на ставку).
public interface IWorkTime {
    double calcWorkTimePayment(int hour, int base);
}
