package Andrei;

class TeamLeader extends Programmer implements IHeading {

    int sizeOfTeam;
    int rateOfLeading;

    TeamLeader(int id, String name, int base, String project, int projectBudget, double projectContribution,
               int sizeOfTeam, int rateOfLeading) {
        super(id, name, base, project, projectBudget, projectContribution);
        this.sizeOfTeam = sizeOfTeam;
        this.rateOfLeading = rateOfLeading;
    }

    @Override
    public double calcHeadingPayment(int sizeOfTeam, int rateOfLeading) {
        return sizeOfTeam*rateOfLeading;
    }

}
