package Andrei;

public class Personal extends Employee implements IWorkTime {

    int base;

    public Personal(int id, String name, int base) {
        super(id, name);
        this.base = base;
    }

    @Override
    public double calcWorkTimePayment(int hour, int base) {
        return hour * base;
    }

    public void salaryCalculator() {
        setPayment(calcWorkTimePayment(worktime, base));
    }
}
