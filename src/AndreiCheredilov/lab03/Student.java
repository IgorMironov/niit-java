//import java.security.acl.Group;

import java.util.Arrays;

public class Student {
    private int ID; //дентификационный номер
    private String Fio; //фамилия и инициалы
    private Group group; //ссылка на группу (объект Group)
    protected final int[] Marks = new int[10]; //массив оценок
    private int Num; //Количество оценок

    Student(int ID, String Fio) {
        this.ID = ID;
        this.Fio = Fio;
        for (int i : Marks) {
            Marks[i] = 0;
        }
        this.Num = 0;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    public void removeGroup() {
        this.group = null;
    }


    public int getMarks(int i) {
        return Marks[i];
    }

    public void addMarks(int mark) throws ArrayIndexOutOfBoundsException {
        Marks[Num] = mark;
        Num++;
    }

    public String getMarks() {
        String string = Arrays.toString((Marks));
        return string;
    }


    public double averageGrade() {
        double averagegrade = 0;
        for (int i : Marks) {
            averagegrade += i;
        }
        return ((averagegrade) / (Marks.length));
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setFio(String Fio) {
        this.Fio = Fio;
    }

    public String getFio() {
        return Fio;
    }

    public void setNum(int num) {
        this.Num = num;
    }

    public int getNum() {
        return Num;
    }

    public void printCurrentgroupTitle() {
        System.out.println("Студент: " + Fio + "  " + "зачислен в группу " + group.getTitle());
    }
}