public class DekanatDemo {
    public static void main(String argv[]) {
        Dekanat obj = new Dekanat();
        obj.studentsCreating();
        obj.groupsCreating();
        obj.placementOfStudents();
        obj.addRandomMarks();
        obj.choosingOfHeads();
        obj.statisticsStorage();
        obj.expellingOfStudent();
        obj.randTransfer();
        obj.createXML();
        obj.outputOfData();
    }
}