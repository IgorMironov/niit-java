import java.util.Arrays;
import java.io.*;

public class Digits {
    public static void main(String[] args) {
        String arr = args[0];
        String[][] numbers = new String[][]{
                      {"    ***   ",
                       "   *   *  ",
                       "  *     * ",
                       "  *     * ",
                       "  *     * ",
                       "   *   *  ",
                       "    ***   ",},

                       {"   * ",
                        "  ** ",
                        "   * ",
                        "   * ",
                        "   * ", 
                        "   * ",
                        "  *** ",},

                       {" *** ",
                        "*   *",
                        "   * ",
                        "  *  ",
                        " *   ",
                        "*    ",
                        "*****"},

                       {" *** ",
                        "*   *",
                        "    *",
                        "  ** ",
                        "    *",
                        "*   *",
                        " *** "},

                          {"*",
                        "  **",
                        " * *",
                        "*  *",
                        "****",
                        "   *",
                        "   *"},

                       {"*****",
                        "*    ",
                        "*    ",
                        " *** ",
                        "    *",
                        "*   *",
                        " *** "},

                       {"   * ",
                        "  *  ",
                        " *   ",
                        "**** ",
                        "*   *",
                        "*   *",
                        " *** "},

                       {"****",
                        "   *",
                        "   *",
                        "  * ",
                        " *  ",
                        "*   ",
                        "*   "},

                       {" *** ",
                        "*   *",
                        "*   *",
                        " *** ",
                        "*   *",
                        "*   *",
                        " *** "},

                       {" *** ",
                        "*   *",
                        "*   *",
                        " ****",
                        "   * ",
                        "  *  ",
                        " *   "}};

        System.out.println(arr);
        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < arr.length(); i++) {
                int number = Character.digit(arr.charAt(i), 10);
                System.out.print(numbers[number][j] + "");
            }
            System.out.println();
        }
    }
}