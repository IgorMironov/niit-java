public class Collatz {
	static int count=0;
    public static void collatz(long n) {
        //System.out.print(n + " ");
		count++;
        if (n == 1) return;
        else if (n % 2 == 0) collatz(n / 2);
        else collatz(3*n + 1);
    }
    public static void main(String[] args) {
		int countmax=0;
		for (long i=1;i<=1000000;i++)
		{
			collatz(i);
		    if (count > countmax)
		    {
			   countmax = count;
		    }
		    count = 0;
			
		}
        System.out.println(countmax);
    }
}