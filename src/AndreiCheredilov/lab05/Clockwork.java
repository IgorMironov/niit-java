import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.util.Duration;

import java.util.Calendar;

class Clockwork {

    public final SimpleIntegerProperty hour = new SimpleIntegerProperty(0);
    public final SimpleIntegerProperty minute = new SimpleIntegerProperty(0);
    public final SimpleIntegerProperty second = new SimpleIntegerProperty(0);

    public Clockwork() {
        startTicking();
    }

    private void startTicking() {
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
            Calendar calendar = Calendar.getInstance();
            hour.set(calendar.get(Calendar.HOUR));
            minute.set(calendar.get(Calendar.MINUTE));
            second.set(calendar.get(Calendar.SECOND));
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

}