package com.vorobjeva.apps.automat;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vorobjeva.apps.automat.adapters.AutomatAdapter;
import com.vorobjeva.apps.automat.adapters.OnAutomatButtonClickListener;
import com.vorobjeva.apps.automat.async.CoffeePrepareAsyncTask;
import com.vorobjeva.apps.automat.async.OnCoffeePreparedListener;
import com.vorobjeva.apps.automat.model.AutomatButton;
import com.vorobjeva.apps.automat.statemachine.AutomatStateMachine;
import com.vorobjeva.apps.automat.statemachine.OnStateChangedListener;
import com.vorobjeva.apps.automat.utils.AnimationUtils;
import com.vorobjeva.apps.automat.utils.CursorParserUtils;
import com.vorobjeva.apps.automat.utils.UiUtils;

import java.util.ArrayList;

public class MainActivity extends Activity implements OnAutomatButtonClickListener, LoaderManager.LoaderCallbacks<Cursor>,
        OnStateChangedListener, OnCoffeePreparedListener {

    private static final String EXTRA_COFFEE_TYPE = "extra_coffee_type";
    private static final int COFFEE_CUP_PREPARATION_TIME_S = 10;

    private RecyclerView mRecyclerView;
    private ArrayList<AutomatButton> mButtons = new ArrayList<>();
    private AutomatAdapter mAutomatAdapter;
    private AutomatStateMachine mAutomatStateMachine;
    private TextView mDisplayText;
    private int mAvailableCredit = 0;
    private Button mMoneyBackBtn;
    private Button mAddOneCoinBtn;
    private Button mAddFiveCoinsBtn;
    private Button mAddTenCoinsBtn;
    private ImageView mCupImageView;
    private ImageView mChangeCoinImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.buttonsRecycler);
        mDisplayText = (TextView) findViewById(R.id.displayText);
        mMoneyBackBtn = (Button) findViewById(R.id.moneyBackBtn);
        mAddOneCoinBtn = (Button) findViewById(R.id.oneCoinAddBtn);
        mAddFiveCoinsBtn = (Button) findViewById(R.id.fiveCoinAddBtn);
        mAddTenCoinsBtn = (Button) findViewById(R.id.tenCoinAddBtn);
        mCupImageView = (ImageView) findViewById(R.id.cofeeCup);
        mChangeCoinImageView = (ImageView) findViewById(R.id.changeCoinImageView);

        mAutomatAdapter = new AutomatAdapter(mRecyclerView.getContext(), mButtons, this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mRecyclerView.setAdapter(mAutomatAdapter);

        mAutomatStateMachine = new AutomatStateMachine(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onAutomatButtonClick(Button button, int price) {
        String text = button.getText().toString().toLowerCase();
        mDisplayText.setText("Working");
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_COFFEE_TYPE, text);
        if (mAvailableCredit >= price) {
            getLoaderManager().restartLoader(1, bundle, this);
        } else {
            Toast.makeText(this, "Not enought credits", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case 0:
                return new CursorLoader(this,
                        Uri.parse("content://" + "com.vorobjeva.apps.serviceapp.drinksprovider" + "/drinks/prices"),
                        null, null, null, null);
            case 1:
                if (null == args || !args.containsKey(EXTRA_COFFEE_TYPE)) {
                    return null;
                }
                return new CursorLoader(this,
                        Uri.parse("content://" + "com.vorobjeva.apps.serviceapp.drinksprovider" + "/drinks/" + args.getString(EXTRA_COFFEE_TYPE)),
                        null, null, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case 0:
                boolean restoredEnabled = false;
                if (null != mButtons && !mButtons.isEmpty() && mButtons.get(0).isEnabled()) {
                    restoredEnabled = true;
                }
                mButtons = new ArrayList<>();
                CursorParserUtils.parseDrinks(data, mButtons);
                for (AutomatButton button : mButtons) {
                    button.setEnabled(restoredEnabled);
                }
                mAutomatAdapter.setButtons(mButtons);
                mAutomatAdapter.notifyDataSetChanged();
                break;
            case 1:
                Pair<String, Integer> cursorData = CursorParserUtils.parseCount(data);
                if (cursorData.second <= 0) {
                    showReloadDialog();
                } else {
                    mAutomatStateMachine.processState();
                    mDisplayText.setText("Preparing coffee");
                    AnimationUtils.startFadeOutAnimation(mCupImageView, COFFEE_CUP_PREPARATION_TIME_S);
                    CoffeePrepareAsyncTask asyncTask = new CoffeePrepareAsyncTask(this, this, COFFEE_CUP_PREPARATION_TIME_S, cursorData.first);
                    asyncTask.execute();
                }
                break;
        }
    }

    private void showReloadDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please call Services");
        builder.setMessage("Not enought product. Would you like to call Services to reload automate or choose another drink?");
        builder.setPositiveButton("Call", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDisplayText.setText("Available credit " + mAvailableCredit);
                Intent intent = new Intent();
                intent.setAction("com.vorobjeva.apps.action.LAUNCH");
                startActivityForResult(intent, 0);
            }
        });
        builder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDisplayText.setText("Available credit " + mAvailableCredit);
            }
        });
        builder.show();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onStateChanged(AutomatStateMachine.State state) {
        switch (state) {
            case READY:
                updateCoffeeButtons(false);
                updateCoinsButtons(true);
                updateMoneyBacksButton(false);
                mDisplayText.setText(getString(R.string.please_enter_money));
                break;
            case COINS_INPUT:
                updateCoffeeButtons(true);
                updateCoinsButtons(true);
                updateMoneyBacksButton(true);
                break;
            case WORKING:
                updateCoffeeButtons(false);
                updateCoinsButtons(false);
                updateMoneyBacksButton(false);
                break;
            case COFFEE:
                updateCoffeeButtons(false);
                updateCoinsButtons(false);
                updateMoneyBacksButton(false);
                mDisplayText.setText("Please take your coffee");
                break;
            case CHANGE:
                updateCoffeeButtons(false);
                updateCoinsButtons(false);
                updateMoneyBacksButton(false);
                if (mAvailableCredit > 0) {
                    mDisplayText.setText("Please take your change");
                    mChangeCoinImageView.setVisibility(View.VISIBLE);
                } else {
                    mAutomatStateMachine.processState();
                }
                break;
            case ERROR:
                mDisplayText.setText("Critical error");
                updateCoffeeButtons(false);
                updateCoinsButtons(false);
                updateMoneyBacksButton(false);
                break;
        }
    }

    private void updateCoffeeButtons(boolean isEnabled) {
        for (AutomatButton automatButton : mButtons) {
            automatButton.setEnabled(isEnabled);
        }
        mAutomatAdapter.setButtons(mButtons);
        mAutomatAdapter.notifyDataSetChanged();
    }

    private void updateCoinsButtons(boolean isEnabled) {
        UiUtils.highlightButtonParent(this, mAddOneCoinBtn, isEnabled);
        UiUtils.highlightButtonParent(this, mAddFiveCoinsBtn, isEnabled);
        UiUtils.highlightButtonParent(this, mAddTenCoinsBtn, isEnabled);
    }

    private void updateMoneyBacksButton(boolean isEnabled) {
        UiUtils.highlightButtonParent(this, mMoneyBackBtn, isEnabled);
    }

    public void onMoneyBackClicked(View view) {
        mAvailableCredit = 0;
        mAutomatStateMachine.reset();
    }

    public void onAddCreditClicked(View view) {
        if (0 == mAvailableCredit) {
            mAutomatStateMachine.processState();
        }

        switch (view.getId()) {
            case R.id.oneCoinAddBtn:
                mAvailableCredit += 1;
                break;
            case R.id.fiveCoinAddBtn:
                mAvailableCredit += 5;
                break;
            case R.id.tenCoinAddBtn:
                mAvailableCredit += 10;
                break;
        }

        mDisplayText.setText("Available credit " + mAvailableCredit);
    }

    @Override
    public void onCoffeePrepared(String name) {
        mAutomatStateMachine.processState();
        for (AutomatButton automatButton : mButtons) {
            if (automatButton.getTitle().equals(name)) {
                int price = Integer.parseInt(automatButton.getPrice());
                mAvailableCredit -= price;
                break;
            }
        }
    }

    public void onCupClicked(View view) {
        mCupImageView.setVisibility(View.GONE);
        mAutomatStateMachine.processState();
    }

    public void onChangeCoinClicked(View view) {
        mChangeCoinImageView.setVisibility(View.GONE);
        mAvailableCredit = 0;
        mAutomatStateMachine.processState();
    }
}
