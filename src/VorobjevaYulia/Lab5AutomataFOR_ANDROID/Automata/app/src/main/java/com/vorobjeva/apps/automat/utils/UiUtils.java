package com.vorobjeva.apps.automat.utils;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.vorobjeva.apps.automat.R;

public class UiUtils {

    public static void highlightButtonParent(Context context, Button button, boolean isEnabled) {
        View parent = (View) button.getParent();
        parent.setBackgroundColor(isEnabled ? context.getColor(R.color.colorEnabled) : context.getColor(R.color.colorDisabled));
        button.setEnabled(isEnabled);
    }
}
