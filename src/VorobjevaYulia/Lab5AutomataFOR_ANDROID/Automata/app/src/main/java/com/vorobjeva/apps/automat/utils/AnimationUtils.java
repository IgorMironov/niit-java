package com.vorobjeva.apps.automat.utils;

import android.animation.ObjectAnimator;
import android.view.View;

public class AnimationUtils {

    public static void startFadeOutAnimation(View view, int duration) {
        view.setVisibility(View.VISIBLE);
        ObjectAnimator.ofFloat(view, View.ALPHA, 0.1f, 1.0f).setDuration(duration * 1000).start();
    }
}
