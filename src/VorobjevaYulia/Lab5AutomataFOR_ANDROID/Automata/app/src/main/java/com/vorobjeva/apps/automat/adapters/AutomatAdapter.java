package com.vorobjeva.apps.automat.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.vorobjeva.apps.automat.model.AutomatButton;
import com.vorobjeva.apps.automat.R;
import com.vorobjeva.apps.automat.utils.UiUtils;

import java.util.List;

/**
 * Created by yuliavorobjeva on 26.04.17.
 */

public class AutomatAdapter extends RecyclerView.Adapter<AutomatAdapter.MyViewHolder> {

    private final OnAutomatButtonClickListener mListener;

    private List<AutomatButton> mButtons;
    private Context mContext;

    public AutomatAdapter(Context context, List<AutomatButton> buttons, OnAutomatButtonClickListener listener) {
        mButtons = buttons;
        mContext = context;
        mListener = listener;

    }

    public void setButtons(List<AutomatButton> buttons) {
        mButtons = buttons;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coffee_button, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AutomatButton item = mButtons.get(position);
        holder.hButton.setText(item.getTitle());
        holder.hPriceView.setText(item.getPrice());
        final int price = Integer.parseInt(holder.hPriceView.getText().toString());
        UiUtils.highlightButtonParent(mContext, holder.hButton, item.isEnabled());
        holder.hButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAutomatButtonClick((Button) v, price);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mButtons.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        Button hButton;
        TextView hPriceView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.hButton = (Button) itemView.findViewById(R.id.button_text);
            this.hPriceView = (TextView) itemView.findViewById(R.id.textPrice);
        }

    }
}
