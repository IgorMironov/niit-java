package com.vorobjeva.apps.automat.utils;

import android.database.Cursor;
import android.util.Pair;

import com.vorobjeva.apps.automat.model.AutomatButton;

import java.util.ArrayList;

public class CursorParserUtils {

    public static Pair<String,Integer> parseCount(Cursor data) {
        Pair<String, Integer> result = null;
        if (null != data) {
            if (data.moveToFirst()) {
                String name = data.getString(data.getColumnIndex("drinkName"));
                int count = Integer.parseInt(data.getString(data.getColumnIndex("drinkCount")));
                result = new Pair<>(name, count);
            }
            data.close();
        }
        return result;
    }

    public static void parseDrinks(Cursor data, ArrayList<AutomatButton> arrayList) {
        if (null != data) {
            if (data.moveToFirst()) {
                do {
                    String name = data.getString(data.getColumnIndex("drinkName"));
                    String price = data.getString(data.getColumnIndex("drinkPrice"));
                    AutomatButton automatButton = new AutomatButton(name, price);
                    arrayList.add(automatButton);
                } while (data.moveToNext());
            }
            data.close();
        }
    }
}
