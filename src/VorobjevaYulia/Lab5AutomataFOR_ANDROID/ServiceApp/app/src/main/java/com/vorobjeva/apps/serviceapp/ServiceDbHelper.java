package com.vorobjeva.apps.serviceapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by yuliavorobjeva on 30.04.17.
 */

public class ServiceDbHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "DRINKS_DB";
    private Context mContext;

    public static final String TABLE_NAME = "drinks";
    public static final String COLUMN_NAME = "drinkName";
    public static final String COLUMN_COUNT = "drinkCount";
    public static final String COLUMN_PRICE = "drinkPrice";

    public static final String CREATE_TABLE = "create table " + TABLE_NAME + " ( _id integer primary key autoincrement, "
            + COLUMN_NAME + " TEXT, "
            + COLUMN_PRICE + " TEXT, "
            + COLUMN_COUNT + " TEXT)";

    public ServiceDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
        initAll(sqLiteDatabase);
    }

    public void initAll(SQLiteDatabase sqLiteDatabase) {
        ContentValues cv = new ContentValues();

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.espresso));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.milk));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.tea));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.glace));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.latte));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.americano));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.russiano));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);

        cv.put(ServiceDbHelper.COLUMN_NAME, mContext.getString(R.string.chocolate));
        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.insert(ServiceDbHelper.TABLE_NAME, null, cv);
    }

    public void updateCount(SQLiteDatabase sqLiteDatabase) {
        ContentValues cv = new ContentValues();

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.espresso)});

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.milk)});

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.tea)});

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.glace)});

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.latte)});

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.americano)});

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.russiano)});

        cv.put(ServiceDbHelper.COLUMN_COUNT, (int) (Math.random() * 100));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.chocolate)});
    }

    public void updatePrices(SQLiteDatabase sqLiteDatabase) {
        ContentValues cv = new ContentValues();

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.espresso)});

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.milk)});

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.tea)});

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.glace)});

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.latte)});

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.americano)});

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.russiano)});

        cv.put(ServiceDbHelper.COLUMN_PRICE, (int) (Math.random() * 50));
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", new String[]{mContext.getString(R.string.chocolate)});
    }

    public void reduceCount(SQLiteDatabase sqLiteDatabase, String[] selectionArgs) {
        Cursor cursor = sqLiteDatabase.query(TABLE_NAME, new String[] {COLUMN_COUNT}, COLUMN_NAME + " LIKE ?", selectionArgs, null, null, null);
        if (null == cursor) {
            return;
        }
        int count = 0;
        if (cursor.moveToFirst()) {
            count = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_COUNT)));
            count--;
        }
        cursor.close();
        ContentValues cv = new ContentValues();
        cv.put(ServiceDbHelper.COLUMN_COUNT, count);
        sqLiteDatabase.update(ServiceDbHelper.TABLE_NAME, cv, COLUMN_NAME + " LIKE ?", selectionArgs);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

}
