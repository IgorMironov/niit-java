package com.vorobjeva.apps.serviceapp;

public interface UpdateDataBaseListener {
    void onUpdateCompleted();
}
