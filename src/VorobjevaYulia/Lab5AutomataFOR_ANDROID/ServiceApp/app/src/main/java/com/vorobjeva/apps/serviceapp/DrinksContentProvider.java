package com.vorobjeva.apps.serviceapp;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by yuliavorobjeva on 30.04.17.
 */

public class DrinksContentProvider extends ContentProvider {

    static final String PROVIDER_NAME = "com.vorobjeva.apps.serviceapp.drinksprovider";
    static final String URL = "content://" + PROVIDER_NAME + "/drinks";
    static final Uri CONTENT_URI = Uri.parse(URL);

    static final int DRINKS         = 1;
    static final int ESPRESSO       = 2;
    static final int MILK           = 3;
    static final int TEA            = 4;
    static final int GLACE          = 5;
    static final int LATTE          = 6;
    static final int AMERICACANO    = 7;
    static final int RUSSIANO       = 8;
    static final int CHOCOLATE      = 9;
    static final int PRICES         = 10;

    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "drinks", DRINKS);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/espresso", ESPRESSO);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/milk", MILK);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/tea", TEA);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/glace", GLACE);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/latte", LATTE);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/americano", AMERICACANO);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/russiano", RUSSIANO);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/chocolate", CHOCOLATE);
        uriMatcher.addURI(PROVIDER_NAME, "drinks/prices", PRICES);
    }

    private SQLiteDatabase mDb;
    private Context mContext;
    private ServiceDbHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mContext = getContext();
        mDbHelper = new ServiceDbHelper(mContext);
        mDb = mDbHelper.getWritableDatabase();
        return mDb != null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        String[] sColumns = null;
        String sSelection = null;
        String[] sSelectionArgs = null;

        switch (uriMatcher.match(uri)) {
            case DRINKS:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME,
                        ServiceDbHelper.COLUMN_PRICE,
                        ServiceDbHelper.COLUMN_COUNT};
                break;
            case ESPRESSO:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.espresso)};
                break;
            case MILK:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.milk)};
                break;
            case TEA:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.tea)};
                break;
            case GLACE:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.glace)};
                break;
            case LATTE:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.latte)};
                break;
            case AMERICACANO:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.americano)};
                break;
            case RUSSIANO:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.russiano)};
                break;
            case CHOCOLATE:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_COUNT};
                sSelection = ServiceDbHelper.COLUMN_NAME + "=?";
                sSelectionArgs = new String[] {mContext.getString(R.string.chocolate)};
                break;
            case PRICES:
                sColumns = new String[] {ServiceDbHelper.COLUMN_NAME, ServiceDbHelper.COLUMN_PRICE};
                break;
            default:
                break;
        }
        return mDb.query(ServiceDbHelper.TABLE_NAME, sColumns, sSelection, sSelectionArgs, null, null, null);
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        switch (uriMatcher.match(uri)) {
            case DRINKS:
                mDbHelper.updateCount(mDb);
                break;
            case PRICES:
                mDbHelper.updatePrices(mDb);
                break;
            case ESPRESSO:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.espresso)});
                break;
            case MILK:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.milk)});
                break;
            case TEA:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.tea)});
                break;
            case GLACE:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.glace)});
                break;
            case LATTE:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.latte)});
                break;
            case AMERICACANO:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.americano)});
                break;
            case RUSSIANO:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.russiano)});
                break;
            case CHOCOLATE:
                mDbHelper.reduceCount(mDb, new String[] {mContext.getString(R.string.chocolate)});
                break;
            default:
                break;
        }
        return 0;
    }
}
