

/**
 * Created by yuliavorobjeva on 02.04.17.
 */

public class Group {

    private String Title;
     Student[] students;
     int num;
    private Student head;

    Group(String name){
        Title = name;
        students = new Student[20];
    }

    public void deleteBadStudents(int numberOfStudents){
        int count =0;
        for(int i=0;i<num;i++)
            if(students[i].findCum()<2.5) {
                deleteStudent(students[i]);
                count++;
            }
            num-=count;
            numberOfStudents-=count;
    }

    public void changeHead(Student HeadNow, Student NewHead){//смена старосты на конкретного студента
        if(HeadNow.getIsHead()){
            HeadNow.deleteHead();
            NewHead.setHead();
            head = NewHead;
        }
    }


    public Student findStudentByID(int id){
        for(int i=0;i<num;i++){
            if(students[i].getID()==id)
                return students[i];
        }
        System.out.println("cannot find");
        return null;
    }


    public Student findStudentByFIO(String fio){
        for(int i=0;i<num;i++){
            if(students[i].getFio().equals(fio))
                return students[i];
        }
        System.out.println("cannot find");
        return null;
    }

    public double countCumInGroup(){
        double CumInGroup = 0;
        for(int i = 0;i<num;i++){
            CumInGroup+=students[i].findCum();
        }
        CumInGroup/=num;
        return CumInGroup;
    }


    public String getTitle(){
        return Title;
    }


    public void setStudents(int numOfStudents){
        students = new Student[numOfStudents];
    }

    public void setStudent(Student student, int i, int number){
        students[i] = student;
        num+=number;
     }

     public void deleteStudent(Student student){
        for(int i=0;i<num;i++){
            if(students[i].getID()==student.getID()) {
                students[i] = null;
                for (int j = i; j < num - 1; j++) {
                    students[j] = students[j + 1];
                }
                num--;
            }
        }
     }

     public void addStudent(Student student){
         if(num<20){
             students[num+1] = student;
             num++;
         }
     }

     public  int getNum(){
         return  num;
     }

}
