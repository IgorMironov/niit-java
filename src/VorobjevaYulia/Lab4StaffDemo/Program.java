/**
 * Created by yuliavorobjeva on 20.04.17.
 */
import java.util.*;
public class Program {
    public static void main(String[] args) throws Exception{
        String allEmployees = FilesReader.readFile();
        List<Employee> employees = Parser.parse(allEmployees);
        for(Employee employee:employees){
            employee.calculatePayment(EmployeeHelper.calculateSubordinates(employee, employees));
            employee.printInfo();
        }
    }
}

