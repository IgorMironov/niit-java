/**
 * Created by yuliavorobjeva on 11.04.17.
 */

import java.util.List;
import java.util.ArrayList;
public abstract class Engineer extends Employee implements WorkTime,Project {

    protected int rate;//ставка по проекту
    protected double part;//"доля" участия
    protected List<ProjectType> mProjectTypes;

    public int getCountPayment() {
        return payment*worktime;
    }

    public double getProjectPayment() {
        return part*rate;
    }

    protected void calculateTotalPayment() {
        totalPayment = getProjectPayment() + getCountPayment();
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.print("Projects: ");
        for (ProjectType projectType : mProjectTypes) {
            System.out.print(projectType + ",  ");
        }
        System.out.println();
        //System.out.println("Part: " + part);
    }

    @Override
    public void fillWithAdditionalData(List<String> data) {
        mProjectTypes = new ArrayList<>();
        for (String item : data) {
            if (ProjectType.contains(item)) {
                mProjectTypes.add(ProjectType.fromString(item));
            } else {
                part = Double.parseDouble(item);
                //System.out.println(item);
            }
        }
    }

    public List<ProjectType> getProjectTypes(){
        return mProjectTypes;
    }
}
