/**
 * Created by yuliavorobjeva on 11.04.17.
 */


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;


public class Parser {
    public static List<Employee> parse(String value){
        List <Employee> result = new ArrayList<>();
        String[] employees = value.split("\n");
        for(String oneEmployee:employees){
            List<String> params = new ArrayList<>(Arrays.asList(oneEmployee.split(",")));
            int id = Integer.parseInt(params.get(0));
            String name = params.get(1);
            Employee.EmployeeType employeeType = Employee.EmployeeType.fromString(params.get(2));
            int payment = Integer.parseInt(params.get(3));
            Employee employee = Employee.EmployeeFactory.getEmployeeForType(employeeType);
            employee.setId(id);
            employee.setName(name);
            employee.setPayment(payment);
            if (params.size() > 4) {
                employee.fillWithAdditionalData(params.subList(4,params.size()));
            }
            result.add(employee);
        }
        return result;
    }
}
