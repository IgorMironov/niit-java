package New;

import java.util.Scanner;


public class Lab1_4 {
    public static void main(String[] args) {
        int[] str_in = charToInt(new Scanner(System.in).nextLine().toCharArray());
        convers(str_in);
    }

    static void convers(int[] str_in) {
        StringBuffer str_out = new StringBuffer();
        for (int i = 0, j = 0; i < str_in.length; i++) {
            if (str_out.length() == 0)
                str_out.append(str_in[i]);
            else {
                if ((str_in[i] - str_in[i - 1]) == 1) {
                    j++;
                    if (i == str_in.length - 1) {
                        str_out.append('-');
                        str_out.append(str_in[i]);
                    }
                } else {
                    if (j > 0) {
                        str_out.append('-');
                        str_out.append(str_in[i - 1]);
                        j = 0;
                    }
                    str_out.append(',');
                    str_out.append(str_in[i]);
                }
            }
        }
        System.out.println(str_out);
    }

    static int[] charToInt(char[] ch) {
        StringBuffer buf = new StringBuffer();
        int[] out = new int[numOfNumbers(ch)];
        for (int i = 0, j = 0; i < ch.length; i++)
            if (Character.isDigit(ch[i])) {
                buf.append(ch[i]);
                if(i== ch.length-1)
                    out[j] = Integer.parseInt(buf.toString());
            }
            else {
                out[j] = Integer.parseInt(buf.toString());
                j++;
                buf.setLength(0);
            }
        return out;
    }

    static int numOfNumbers(char[] ch) {
        int count = 0;
        for (int i = 0; i < ch.length; i++)
            if (ch[i] == ',')
                count++;
        return count + 1;
    }
}
