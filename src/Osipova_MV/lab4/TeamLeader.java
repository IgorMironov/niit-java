package Staff;

public class TeamLeader extends Programmer implements Heading
{
    private int employeePrice;
    //WorkProject workProject;
    public TeamLeader(int id, String name, int workTime, int payment, int percentages, int priceOfEmp){
        super(id, name, workTime, payment, percentages);
        this.employeePrice = priceOfEmp;
    }

    public int calcHeadingPayment(){
        return workProject.getNumOfEmployees() * employeePrice;
    }

    @Override
    public int calcPayment(){
        return super.calcPayment() + calcHeadingPayment();
    }
}