package Staff;

public class WorkProject
{
    String name;
    int price;
    int percentForManager;
    private Employee employees[];
    public WorkProject(String name, int price, int percentForManager)
    {
        this.name = name;
        this.price = price;
        this.percentForManager = percentForManager;
        this.employees = new Employee[100];
    }

    public void addEmployee(Employee employee)
    {
        for(int i = 0; i < employees.length; i++)
        {
            if(employees[i] == null)
            {
                if(employee instanceof Engineer)
                    ((Engineer) employee).setWorkProject(this);
                else if(employee instanceof Manager)
                    ((Manager) employee).setWorkProject(this);
                employees[i] = employee;
                return;
            }
        }
        throw new RuntimeException("Array with Employees is full!");
    }

    int getNumOfEmployees()
    {
        int i = 0;
        for(Employee e : employees)
        {
            if(e != null)
                i++;
        }
        return i;
    }

    public void printSalary()
    {
        String format = "%s %10s %15s %20s\n";
        System.out.println("Project: " + this.name);
        System.out.printf(format, "Id", "FIO", "Position", "Salary");
        int total = 0;
        for(Employee e : employees)
        {
            if(e != null)
            {
                System.out.printf(format, e.id, e.name, e.getClass().getSimpleName(), e.calcPayment());
                total += e.calcPayment();
            }
        }
        System.out.println("TOTAL: " + total);
    }
}