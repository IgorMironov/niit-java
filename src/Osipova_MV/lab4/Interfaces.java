package Staff;

interface WorkTime
{
    int calcWorkTimePayment();
}

interface Project
{
    int calcProjectPayment();
}

interface Heading
{
    int calcHeadingPayment();
}
