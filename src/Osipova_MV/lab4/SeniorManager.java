package Staff;

public class SeniorManager extends ProjectManager
{
    final static int paymentForProjects = 500;
    public SeniorManager(int id, String name){
        super(id, name);
    }

    @Override
    public int calcPayment(){
        return super.calcPayment() + paymentForProjects;
    }
}
