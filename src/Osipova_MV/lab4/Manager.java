package Staff;

abstract class Manager extends Employee implements Project
{
    WorkProject workProject;
    Manager(int id, String name){
        super(id, name, 0, 0);
    }

    public void setWorkProject(WorkProject workProject){
        this.workProject = workProject;
    }
}
