package Staff;

abstract class Engineer extends Employee implements Project, WorkTime
{

    WorkProject workProject;
    private int percentages;

    Engineer(int id, String name, int workTime, int payment, int percentages){
        super(id, name, workTime, payment);
        this.percentages = percentages;
    }

    public void setWorkProject(WorkProject workProject){
        this.workProject = workProject;
    }

    public int calcWorkTimePayment(){
        return workTime * payment;
    }

    public int calcProjectPayment(){
        return workProject.price * percentages / 100;
    }

    @Override
    public int calcPayment(){
        return calcWorkTimePayment() + calcProjectPayment();
    }
}
