class AutomataDemo1 {
    public static void main(String args[]) {
        Automat automat = new Automat();
        automat.printState();

        automat.on();
        automat.printState();
        automat.coin(50);
        automat.choice(Drinks.TEA);
        automat.off();
        automat.printState();
    }
}
