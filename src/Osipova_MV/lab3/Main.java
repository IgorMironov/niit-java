package Dekanat;

public class Main
{
    public static void main (String [] args) {
        Dekanat dekanat = new Dekanat();
        dekanat.loadStudents("Students.txt");
        dekanat.loadGroups("Groups.txt");
        dekanat.addStudentsToRandomGroups();
        dekanat.setRandomHeads();
        dekanat.print();
        dekanat.addRandomMarks();
        dekanat.printStatistic();
        Student student = dekanat.findStudentByName("Joker");
        dekanat.changeGroup(student.getId(),"Group3");
        dekanat.deleteStupidStudents();
        dekanat.print();
        dekanat.printStatistic();
        dekanat.writeResult("Results.txt");
    }

}