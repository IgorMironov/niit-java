package Dekanat;

/*
Примерный перечень полей:

Title - название группы
Students - массив из ссылок на студентов
Num - количество студентов в группе
Head - ссылка на старосту (из членов группы)
Обеспечить класс следующими методами:

создание группы с указанием названия
добавление студента
избрание старосты
поиск студента по ФИО или ИД
вычисление соеднего балла в группе
исключение студента из группы
*/

public class Group {
    private int maxStudentsNumber = 30;
    private String Title;
    private Student[] Students = new Student[maxStudentsNumber];
    private int Num;
    private Student Head;

    //Создание группы с указанием названия
    Group(String Title) {
        this.Title = Title;
    }

    //Добавление студента
    public void addStudent(Student student){
        Students[Num] = student;
        Students[Num].setGroup(this);
        Num++;
    }

    //исключение студента из группы
    public void delStudent(Student student) {
        for (int i =0; i<Num; i++)
        {
            if (Students[i] == student) {
                Students[i].setGroup(null);
                Students[i] = Students[Num-1];
                Num--;
            }
        }
    }

    //Избрание старосты
    public void setRandomHead(){
        int randomIndex = (int)Math.random() * Num;
        Head = Students[randomIndex];
    }

    //Поиск по имени
    public Student findStudentByName(String name){
        for(int i=0;i<Num;i++){
            if(name.equals(Students[i].getFio())) {
                return Students[i];
            }
        }
        return null;
    }

    //Поиск по ID
    public Student findStudentByID(int id){
        for(int i=0;i<Num;i++){
            if(id == Students[i].getId()) {
                return Students[i];
            }
        }
        return null;
    }

    //вычисление среднего балла в группе
    public double getAverageMark(){
        double sum=0;
        for(int i=0;i<Num;i++){
            sum=sum+Students[i].getAverageMark();
        }
        return sum/Num;
    }

    public String getTitle(){
        return Title;
    }

    //Печать имени группы и студентов группы
    public void print(){
        System.out.println("Group: " + Title);
        System.out.println("Head: " + Head.getFio());
        System.out.println("ID:\tName:");
        for(int i = 0; i<Num; i++) {
            Students[i].print();
        }
    }
}
