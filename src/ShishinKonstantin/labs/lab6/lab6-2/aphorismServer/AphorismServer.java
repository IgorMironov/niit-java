import org.apache.commons.lang3.math.NumberUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

class ServerOne extends Thread {
    private Socket client = null;
    private BufferedReader inStream = null;
    private PrintWriter outStream = null;
    private String strInput;
    private String strOutput = "Для получения нужного афоризма введите число от 1 до 50. " +
            "Для получения случайного афоризма введите \"rnd\". Для выхода введите \"exit\".";

    public ServerOne (Socket client) throws IOException {
        this.client = client;
        inStream = new BufferedReader(new InputStreamReader(client.getInputStream()));
        outStream = new PrintWriter(client.getOutputStream(),true);
        start();
    }

    public void run() {
        outStream.println(strOutput);
        System.out.println("Ожидаем сообщений от клиента c IP " + client.getInetAddress());
        try {
            while ((strInput = inStream.readLine()) != null) {
                if (strInput.equalsIgnoreCase("exit"))
                    break;
                else if (strInput.equalsIgnoreCase("rnd")) {
                    //( Math.random() * (max - min) ) + min
                    int indexRnd = (int) (Math.random() * AphorismServer.getAphIdsSize());  // от 0 до 49
                    outStream.println(AphorismServer.getAphorism(indexRnd));
                    System.out.println("Клиенту c IP " + client.getInetAddress() + " отправлен " + AphorismServer.getAphorism(indexRnd));
                }
                else if (NumberUtils.isNumber(strInput.toString())) {
                    int index = Integer.parseInt(strInput.toString()) - 1;
                    if (index >= 0 && index <= 49) {
                        outStream.println(AphorismServer.getAphorism(index));
                        System.out.println("Клиенту c IP " + client.getInetAddress() + " отправлен " + AphorismServer.getAphorism(index));
                    }
                    else outStream.println(strOutput);
                }
                else outStream.println(strOutput);
            }
            System.out.println("Клиент c IP " + client.getInetAddress() + " отключился");
        } catch (IOException e) {System.err.println("Ошибка чтения/записи");}
        finally {
            try {
                client.close();
            } catch (IOException e) {System.err.println("Сокет не закрыт");}
        }
    }
}

public class AphorismServer {
    //static потому что сервер запускается из main, который может вызывать только статические методы,
    //которые могут обращаться только к статическим переменным
    //альтернатива: создавать в main экземпляр класса сервера, так сделано в лабе с задачами
    private static ArrayList<Integer> aphIds = null;        //список ID афоризмов
    private static ArrayList<String> aphTexts = null;       //список текстов афоризмов
    private static ArrayList<String> aphAuthors = null;     //список авторов афоризмов

    //получение размера списка ID афоризмов
    public static int getAphIdsSize(){
        return aphIds.size();
    }

    //чтение XML файла
    public static void readXMLFile(){
        aphIds = new ArrayList<Integer>();
        aphTexts = new ArrayList<String>();
        aphAuthors = new ArrayList<String>();
        try {
            //создаем построитель документа
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            //создаем дерево DOM документа из файла
            Document document = documentBuilder.parse("aphorisms.xml");
            //получаем корневой элемент
            Node root = document.getDocumentElement();
            //document.getDocumentElement().normalize();
            //просматриваем все подэлементы корневого элемента
            NodeList aphorisms = root.getChildNodes();

            for (int i = 0; i < aphorisms.getLength(); i++) {
                Node aphorism = aphorisms.item(i);
                if (Node.ELEMENT_NODE == aphorism.getNodeType()) {
                    Element element = (Element) aphorism;
                    //заполняем списки
                    aphIds.add(Integer.parseInt(element.getAttribute("id")));
                    aphTexts.add(element.getElementsByTagName("Text").item(0).getTextContent());
                    aphAuthors.add(element.getElementsByTagName("Author").item(0).getTextContent());
                }
            }
        } catch (ParserConfigurationException ex) {ex.printStackTrace(System.out);
        } catch (SAXException ex) {ex.printStackTrace(System.out);
        } catch (IOException ex) {ex.printStackTrace(System.out);
        }
    }

    //получение строки афоризма
    public static String getAphorism (int index){
        String strAphorism = "Афоризм "+aphIds.get(index)+": "+aphTexts.get(index)+" ("+aphAuthors.get(index)+")";
        return strAphorism;
    }

    //запуск приложения
    public static void main(String[] args) throws IOException{
        ServerSocket server = null;
        Socket client = null;
        int port = 1234;

        System.out.println("Мультипоточный сервер стартовал");

        //читаем XML файл
        readXMLFile();

        //создаем серверный сокет
        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Ошибка связывания с портом " + port);
            System.exit(-1);
        }

        //ждем подключения клиента
        try {
            while (true) {
                client = server.accept();
                try {
                    System.out.println("Подключился клиент c IP " + client.getInetAddress());
                    new ServerOne(client);
                } catch (IOException e) {client.close();}
            }
        }
        finally {
            server.close();
        }
    }
}

