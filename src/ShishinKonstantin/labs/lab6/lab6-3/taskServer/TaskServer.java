import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

class SendServer extends Thread {

    private ArrayList<Socket> clients;  //список сокетов клиентов
    private ArrayList<Integer> ids = new ArrayList<Integer>();  //список ID клиентов
    private ArrayList<PrintWriter> outStreams = new ArrayList<PrintWriter>();   //список потоков клиентов
    private TaskServer taskServer = null;   //ссылка на сервер

    public SendServer(TaskServer taskServer, ArrayList<Socket> clients) throws IOException {
        this.taskServer = taskServer;
        this.clients = clients;
        start();
    }
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                Date date = new Date();
                SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                String currentDate = df.format(date);
                System.out.println(currentDate + " Num clients = " + ids.size());
                //находим в списке задачу, которую необходимо передать клиенту с нужным ID
                for (int i = 0; i < ids.size(); i++) {
                    for (int j = 0; j < taskServer.getClientsIdsSize(); j++){
                        if (taskServer.getClientsIds().get(j) == ids.get(i) && taskServer.getTasksTimes().get(j).equals(currentDate)) {
                            outStreams.get(i).println(taskServer.getTask(j));
                            System.out.println("Отправлено сообщение клиенту с ID " + ids.get(i));
                        }
                    }
                }
            }
        } catch (InterruptedException ex){System.err.println("Ошибка потока");}
    }

    //получение ID клиента
    public int getID(int i){
        return ids.get(i);
    }

    //получение размера списка ID клиентов
    public int getIDsSize(){
        return ids.size();
    }

    //добавление ID клиента в список
    public void addID(Integer id){
        ids.add(id);
    }

    //удаление ID клиента из списка
    public void removeID(int i){
        ids.remove(i);
    }

    //добавление сокета клиента в список
    public void addClient(Socket client){
        clients.add(client);
    }

    //удаление сокета клиента из списка
    public void removeClient(int i){
        clients.remove(i);
    }

    //добавление потока клиента в список
    public void addStream(PrintWriter outStream){
        outStreams.add(outStream);
    }

    //удаление потока клиента из списка
    public void removeStream(int i){
        outStreams.remove(i);
    }
}

class ServerOne extends Thread {
    private Socket client = null;
    private BufferedReader inStream = null;
    private PrintWriter outStream = null;
    private SendServer sendServer = null;
    private int clientID = 0;

    public ServerOne (Socket client, SendServer sendServer) throws IOException {
        this.client = client;
        this.sendServer = sendServer;
        inStream = new BufferedReader(new InputStreamReader(client.getInputStream()));
        outStream = new PrintWriter(client.getOutputStream(),true);
        start();
    }

    public void run() {
        try {
            clientID = Integer.parseInt(inStream.readLine());
            sendServer.addID(clientID);
            sendServer.addClient(client);
            sendServer.addStream(outStream);

            outStream.println("Ваш ID: " + clientID);
            while ((inStream.readLine()) != null) {}

            for (int i=0; i<sendServer.getIDsSize(); ){
                if (sendServer.getID(i) == clientID){
                    sendServer.removeID(i);
                    sendServer.removeClient(i);
                    sendServer.removeStream(i);
                }
                i++;
            }
        } catch (IOException ex) {System.out.println("Клиент c IP " + client.getInetAddress() + " отключился");}
        finally {
            try {
                for (int i=0; i<sendServer.getIDsSize(); ){
                    if (sendServer.getID(i) == clientID){
                        sendServer.removeID(i);
                        sendServer.removeClient(i);
                        sendServer.removeStream(i);
                    }
                    i++;
                }
                client.close();
            } catch (IOException e) {System.err.println("Сокет не закрыт");}
        }
    }
}

public class TaskServer {
    private ArrayList<Integer> clientsIds = null;    //список ID клиентов
    private ArrayList<String> tasksTimes = null;     //список времен задач
    private ArrayList<String> tasksTexts = null;     //список текстов задач
    private ArrayList<Socket> clients = new ArrayList<Socket>();     //список сокетов клиентов

    public TaskServer(){
        ServerSocket server = null;
        Socket client = null;
        int port = 1234;
        System.out.println("Мультипоточный сервер стартовал");

        //читаем XML файл
        readXMLFile();

        //запускаем SendServer
        SendServer sendServer = null;
        try {
            sendServer = new SendServer(this, clients);
        } catch (IOException e) {
            System.out.println("Ошибка ввода/вывода");
        }

        //создаем серверный сокет
        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Ошибка связывания с портом " + port);
            System.exit(-1);
        }

        //ждем подключения клиента
        try {
            while (true) {
                try{
                    client = server.accept();
                    System.out.println("Подключился клиент c IP " + client.getInetAddress());
                    clients.add(client);
                    new ServerOne(client, sendServer);
                } catch (IOException e){
                    try {
                        client.close();
                    } catch (IOException ex){System.out.println("Ошибка закрытия сокета клиента");}
                }
            }
        }
        finally {
            try{
                server.close();
            } catch (IOException ex){System.out.println("Ошибка ошибка закрытия сокета сервера");}
        }
    }


    //чтение XML файла
    public void readXMLFile(){
        clientsIds = new ArrayList<Integer>();
        tasksTimes = new ArrayList<String>();
        tasksTexts = new ArrayList<String>();

        try {
            //создаем построитель документа
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            //создаем дерево DOM документа из файла
            Document document = documentBuilder.parse("tasks.xml");
            //получаем корневой элемент
            Node root = document.getDocumentElement();
            //document.getDocumentElement().normalize();
            //просматриваем все подэлементы корневого элемента
            NodeList tasks = root.getChildNodes();

            for (int i = 0; i < tasks.getLength(); i++) {
                Node task = tasks.item(i);
                if (Node.ELEMENT_NODE == task.getNodeType()) {
                    Element element = (Element) task;
                    //заполняем списки
                    clientsIds.add(Integer.parseInt(element.getElementsByTagName("ClientID").item(0).getTextContent()));
                    tasksTimes.add(element.getElementsByTagName("Time").item(0).getTextContent());
                    tasksTexts.add(element.getElementsByTagName("Text").item(0).getTextContent());
                }
            }
        } catch (ParserConfigurationException ex) {ex.printStackTrace(System.out);
        } catch (SAXException ex) {ex.printStackTrace(System.out);
        } catch (IOException ex) {ex.printStackTrace(System.out);
        }
    }

    //получение списка времен задач
    public ArrayList<String> getTasksTimes(){
        return tasksTimes;
    }

    //получение списка ID клиентов
    public ArrayList<Integer> getClientsIds(){
        return clientsIds;
    }

    //получение размера списка ID клиентов
    public int getClientsIdsSize(){
        return clientsIds.size();
    }

    //получение строки задачи
    public String getTask (int index){
        String strTask = "Время: "+tasksTimes.get(index)+". Пора "+tasksTexts.get(index);
        return strTask;
    }

    //запуск приложения
    public static void main(String[] args) throws IOException{
        new TaskServer();
    }
}

