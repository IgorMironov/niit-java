package kostya;

//работник. Основной родительский класс для всех разновидностей работников.
public abstract class Employee {
    private int id = 0;                 //идентификационный номер
    private String name = "";           //ФИО
    private String position = "";       //должность
    private double workTime = 0;        //отработанное время
    private double payment = 0;         //заработная плата

    //конструктор
    public Employee(int id, String name, String position){
        this.id = id;
        this.name = name;
        this.position = position;
    }

    //задание отработанного времени
    public void setWorkTime(double workTime){
        this.workTime = workTime;
    }

    //получение отработанного времени
    public double getWorkTime(){
        return workTime;
    }

    //получение заработной платы
    public double getPayment(){
        return payment;
    }

    //задание заработной платы
    public void setPayment(double payment){
        this.payment = payment;
    }

    //расчет заработной платы
    abstract public double calcPayment();

    //вывод информации о зарплатах сотрудников
    public void printEmployeesInfo() {
        System.out.printf("%-20s%-25s%-20.2f%-10.2f%n", position, name, workTime, payment);
    }

}
