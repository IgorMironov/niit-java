package kostya;

//инженер-программист.
public class Programmer extends Engineer{
    private int numWrittenLinesOfCode = 0;      //количество написанных строк кода

    public Programmer(int id, String name, String position, int base, int projectCost, int numLinesOfCodeInProject, int numWrittenLinesOfCode){
        super(id, name, position, base, projectCost, numLinesOfCodeInProject);
        this.numWrittenLinesOfCode = numWrittenLinesOfCode;
    }

    //расчет оплаты исходя из участия в проекте (35% от стоимости выполняемого проекта делятся между ведущим программистом и программистами пропорционально количеству написанных строк кода)
    @Override
    public double calcPaymentForProject() {
        return (getProjectCost() * 0.35) * ((double) numWrittenLinesOfCode / getNumLinesOfCodeInProject());
    }
}
