package kostya;

//работник по найму с оплатой за фактически отработанное время. Имеет ставку за час.
public abstract class Personal extends Employee implements WorkTime{
    private int base = 0;     //ставка за час

    public Personal(int id, String name, String position, int base){
        super(id, name, position);
        this.base = base;
    }

    //получение ставки
    public int getBase(){
        return base;
    }

    //расчет оплаты исходя из отработанного времени (часы умножаются на ставку)
    @Override
    public double calcPaymentForWorkTime(){
        return getWorkTime() * getBase();
    }

    //расчет заработной платы
    @Override
    public double calcPayment() {
        setPayment(calcPaymentForWorkTime());
        return getPayment();
    }
}
