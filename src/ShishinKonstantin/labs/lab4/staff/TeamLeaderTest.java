package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class TeamLeaderTest extends TestCase {
    public void testCalcPaymentForWorkTime() throws Exception {
        TeamLeader teamLeader = new TeamLeader(1008,"Борисов Б.Б.","TeamLeader",800,1000000,10000,1000,10);
        teamLeader.setWorkTime(8);
        assertEquals(6400.0,teamLeader.calcPaymentForWorkTime());
    }

    public void testCalcPaymentForProject() throws Exception {
        TeamLeader teamLeader = new TeamLeader(1008,"Борисов Б.Б.","TeamLeader",800,1000000,10000,1000,10);
        assertEquals(35000.0,teamLeader.calcPaymentForProject());
    }

    public void testCalcPaymentForHeading() throws Exception {
        TeamLeader teamLeader = new TeamLeader(1008,"Борисов Б.Б.","TeamLeader",800,1000000,10000,1000,10);
        assertEquals(50000.0,teamLeader.calcPaymentForHeading());
    }

    public void testCalcPayment() throws Exception {
        TeamLeader teamLeader = new TeamLeader(1008,"Борисов Б.Б.","TeamLeader",800,1000000,10000,1000,10);
        teamLeader.setWorkTime(8);
        assertEquals(91400.0,teamLeader.calcPayment());
    }

}