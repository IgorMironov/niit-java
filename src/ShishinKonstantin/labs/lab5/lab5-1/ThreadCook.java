//поток, имитирующий процесс приготовления напитка
public class ThreadCook extends Thread{
    private MainForm form = null;   //переменная, содержащая ссылку на форму

    //конструктор потока
    ThreadCook(String name, MainForm form){
        super(name);
        this.form = form;
        start();
    }

    public void run(){
        if(form.automat.getState() == Automat.STATES.COOK){
            form.setTextState(form.automat.getStrCheck());
            form.setTextCash(Integer.toString(form.automat.getCash()));
            try{
                for (int i=1; i<11; i++) {
                    Thread.sleep(1000);
                    form.setValueProgressbar(i*10);
                }
                //завершение приготовления напитка
                form.automat.finish();
                form.setTextState(form.automat.getStrFinish());
                Thread.sleep(2000);
            }catch (InterruptedException iex){System.out.println("Поток прерван!");}
            //восстановление начального состояния автомата
            form.setTextState("Автомат готов к работе!");
            form.setValueProgressbar(0);
        }
    }
}
