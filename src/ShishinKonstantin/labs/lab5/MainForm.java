import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainForm extends JFrame{
    private JPanel panel;
    private JProgressBar progressBar;
    private JButton cash10Button;
    private JButton cash100Button;
    private JButton cash50Button;
    private JButton cash1Button;
    private JButton cash2Button;
    private JButton cash5Button;
    private JButton drink1Button;
    private JButton drink2Button;
    private JButton drink3Button;
    private JButton drink4Button;
    private JButton drink5Button;
    private JButton drink6Button;
    private JTextField textState;
    private JTextField textCash;
    private JCheckBox onOffCheckBox;
    private JButton cancelButton;

    MainForm thisForm = this;           //создание переменной и назначение ей ссылки на форму (необходима для потока)
    Automat automat = new Automat();    //создание экземляра класса Automat

    //конструктор формы
    public MainForm(String title) {
        super(title);

        //добавление на кнопки цен на напитки
        int[] prices = automat.getPrices();
        drink1Button.setText(Integer.toString(prices[0])+" р.");
        drink2Button.setText(Integer.toString(prices[1])+" р.");
        drink3Button.setText(Integer.toString(prices[2])+" р.");
        drink4Button.setText(Integer.toString(prices[3])+" р.");
        drink5Button.setText(Integer.toString(prices[4])+" р.");
        drink6Button.setText(Integer.toString(prices[5])+" р.");

        setSize(410,500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(panel);
        setVisible(true);

        //переключатель включения и выключения автомата
        onOffCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(onOffCheckBox.isSelected()) {
                    if(automat.getState() == Automat.STATES.OFF){
                        automat.on();
                        onOffCheckBox.setText("ON");
                        textState.setText("Автомат готов к работе!");
                        textCash.setText(Integer.toString(automat.getCash()));
                    }
                }
                else {
                    if(automat.getState() == Automat.STATES.WAIT){
                        automat.off();
                        onOffCheckBox.setText("OFF");
                        textState.setText("");
                        textCash.setText("");
                    }
                    else {
                        onOffCheckBox.setSelected(true);
                    }
                }
            }
        });

        //кнопка добавления к сумме денег 1 р.
        cash1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.WAIT || automat.getState() == Automat.STATES.ACCEPT){
                    automat.coin(1);
                    textCash.setText(Integer.toString(automat.getCash()));
                    textState.setText(automat.getStrCoin());
                }

            }
        });

        //кнопка добавления к сумме денег 2 р.
        cash2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.WAIT || automat.getState() == Automat.STATES.ACCEPT){
                    automat.coin(2);
                    textCash.setText(Integer.toString(automat.getCash()));
                    textState.setText(automat.getStrCoin());
                }
            }
        });

        //кнопка добавления к сумме денег 5 р.
        cash5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.WAIT || automat.getState() == Automat.STATES.ACCEPT){
                    automat.coin(5);
                    textCash.setText(Integer.toString(automat.getCash()));
                    textState.setText(automat.getStrCoin());
                }
            }
        });

        //кнопка добавления к сумме денег 10 р.
        cash10Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.WAIT || automat.getState() == Automat.STATES.ACCEPT){
                    automat.coin(10);
                    textCash.setText(Integer.toString(automat.getCash()));
                    textState.setText(automat.getStrCoin());
                }
            }
        });

        //кнопка добавления к сумме денег 50 р.
        cash50Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.WAIT || automat.getState() == Automat.STATES.ACCEPT){
                    automat.coin(50);
                    textCash.setText(Integer.toString(automat.getCash()));
                    textState.setText(automat.getStrCoin());
                }
            }
        });

        //кнопка добавления к сумме денег 100 р.
        cash100Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.WAIT || automat.getState() == Automat.STATES.ACCEPT){
                    automat.coin(100);
                    textCash.setText(Integer.toString(automat.getCash()));
                    textState.setText(automat.getStrCoin());
                }
            }
        });

        //кнопка отмены внесения денег
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.ACCEPT) {
                    automat.cancel();
                    textCash.setText(Integer.toString(automat.getCash()));
                    textState.setText("Возврат денег...");
                }
            }
        });

        //кнопка выбора напитка 1 и запуска потока, имитирующего процесс приготовления напитка
        drink1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.ACCEPT || automat.getState() == Automat.STATES.WAIT) {
                    automat.choice(1);
                    //установление значения поля textState в "Недостаточная сумма денег!" (при нехватке денег на напиток)
                    textState.setText(automat.getStrCheck());
                    ThreadCook threadCook = new ThreadCook("ThreadCook", thisForm);
                    try{
                        threadCook.join();
                    }catch (InterruptedException iex){System.out.println("Поток прерван!");}
                }
            }
        });

        //кнопка выбора напитка 2 и запуска потока, имитирующего процесс приготовления напитка
        drink2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.ACCEPT || automat.getState() == Automat.STATES.WAIT) {
                    automat.choice(2);
                    //установление значения поля textState в "Недостаточная сумма денег!" (при нехватке денег на напиток)
                    textState.setText(automat.getStrCheck());
                    ThreadCook threadCook = new ThreadCook("ThreadCook", thisForm);
                    try{
                        threadCook.join();
                    }catch (InterruptedException iex){System.out.println("Поток прерван!");}
                }
            }
        });

        //кнопка выбора напитка 3 и запуска потока, имитирующего процесс приготовления напитка
        drink3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.ACCEPT || automat.getState() == Automat.STATES.WAIT) {
                    automat.choice(3);
                    //установление значения поля textState в "Недостаточная сумма денег!" (при нехватке денег на напиток)
                    textState.setText(automat.getStrCheck());
                    ThreadCook threadCook = new ThreadCook("ThreadCook", thisForm);
                    try{
                        threadCook.join();
                    }catch (InterruptedException iex){System.out.println("Поток прерван!");}
                }
            }
        });

        //кнопка выбора напитка 4 и запуска потока, имитирующего процесс приготовления напитка
        drink4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.ACCEPT || automat.getState() == Automat.STATES.WAIT) {
                    automat.choice(4);
                    //установление значения поля textState в "Недостаточная сумма денег!" (при нехватке денег на напиток)
                    textState.setText(automat.getStrCheck());
                    ThreadCook threadCook = new ThreadCook("ThreadCook", thisForm);
                    try{
                        threadCook.join();
                    }catch (InterruptedException iex){System.out.println("Поток прерван!");}
                }
            }
        });

        //кнопка выбора напитка 5 и запуска потока, имитирующего процесс приготовления напитка
        drink5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.ACCEPT || automat.getState() == Automat.STATES.WAIT) {
                    automat.choice(5);
                    //установление значения поля textState в "Недостаточная сумма денег!" (при нехватке денег на напиток)
                    textState.setText(automat.getStrCheck());
                    ThreadCook threadCook = new ThreadCook("ThreadCook", thisForm);
                    try{
                        threadCook.join();
                    }catch (InterruptedException iex){System.out.println("Поток прерван!");}
                }
            }
        });

        //кнопка выбора напитка 6 и запуска потока, имитирующего процесс приготовления напитка
        drink6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(automat.getState() == Automat.STATES.ACCEPT || automat.getState() == Automat.STATES.WAIT) {
                    automat.choice(6);
                    //установление значения поля textState в "Недостаточная сумма денег!" (при нехватке денег на напиток)
                    textState.setText(automat.getStrCheck());
                    ThreadCook threadCook = new ThreadCook("ThreadCook", thisForm);
                    try{
                        threadCook.join();
                    }catch (InterruptedException iex){System.out.println("Поток прерван!");}
                }
            }
        });
    }

    //установление значения и обновление графики поля textState
    public void setTextState(String state){
        textState.setText(state);
        textState.update(textState.getGraphics());
    }

    //установление значения и обновление графики поля textCash
    public void setTextCash (String cash){
        textCash.setText(cash);
        textCash.update(textCash.getGraphics());
    }

    //установление значения и обновление графики progressBar'а
    public void setValueProgressbar(int value){
        progressBar.setValue(value);
        progressBar.update(progressBar.getGraphics());
    }

    //запуск приложения
    public static void main(String[] args) {
        MainForm form = new MainForm("AUTOMAT");     //создание экземляра класса MainForm
    }
}


