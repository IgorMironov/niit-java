import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;

public class AnalogClock extends JComponent implements Runnable
{
    private String typeOfDigits = "Arabic";
    private String[] digits = null;
    private String[] arabicDigits = {"1","2","3","4","5","6","7","8","9","10","11","12"};
    private String[] romanDigits = {"I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII"};
    private String fontOfDigits = "Times New Roman";
    private int sizeOfDigits = 60;
    private int offset = 50;    //смещение относительно радиуса (необходимо при увеличении цифр)
    private Color colorOfLines = Color.black;
    private Color colorOfDigits = Color.black;
    private Color colorOfLogo = Color.black;
    private Color colorOfCalendarDigits = Color.black;
    private Color colorOfCalendar = Color.white;
    private Color colorOfHands = Color.red;
    private String logo = "JAnalogClock";
    private boolean drawLogo = true;
    private boolean drawCalendar = true;

    private static Stroke SEC_STROKE = new BasicStroke(3F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    private static Stroke HOUR_STROKE = new BasicStroke(4F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    private static Stroke ROUND = new BasicStroke(6F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);

    private Dimension size = null;

    //конструктор
    public AnalogClock()  {
        setBackground(Color.white);
        (new Thread(this)).start();
    }

    //запуск часов
    public void run() {
        try {
            for(;;) {
                Thread.sleep(1000);
                repaint();
            }
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    //задание типа цифр
    public void setTypeOfDigits(String typeOfDigits){
        this.typeOfDigits = typeOfDigits;
    }
    //задание шрифта цифр
    public void setFontOfDigits(String fontOfDigits){
        this.fontOfDigits = fontOfDigits;
    }
    //задание размера цифр и смещения
    public void setSizeOfDigits(int sizeOfDigits, int offset){
        this.sizeOfDigits = sizeOfDigits;
        this.offset = offset;
    }
    //задание шрифта секундных штрихов циферблата
    public void setColorOfLines(Color colorOfLines){
        this.colorOfLines = colorOfLines;
    }
    //задание цвета цифр
    public void setColorOfDigits(Color colorOfDigits){
        this.colorOfDigits = colorOfDigits;
    }
    //задание цвета стрелок
    public void setColorOfHands(Color colorOfHands){
        this.colorOfHands = colorOfHands;
    }
    //получение цвета стрелок
    public Color getColorOfHands(){
        return this.colorOfHands;
    }
    //задание цвета логотипа
    public void setColorOfLogo(Color colorOfLogo){
        this.colorOfLogo = colorOfLogo;
    }
    //задание цвета цифр календаря
    public void setColorOfCalendarDigits(Color colorOfCalendarDigits){
        this.colorOfCalendarDigits = colorOfCalendarDigits;
    }
    //задание цвета календаря
    public void setColorOfCalendar(Color colorOfCalendar){
        this.colorOfCalendar = colorOfCalendar;
    }
    //задание необходимости рисования логотипа
    public void setDrawLogo(boolean drawLogo){
        this.drawLogo = drawLogo;
    }
    //задание необходимости рисования календаря
    public void setDrawCalendar(boolean drawCalendar){
        this.drawCalendar = drawCalendar;
    }


    //рисование часов
    public void paint(Graphics graphics) {
        super.paint(graphics);

        LocalTime lt = LocalTime.now();

        long minute = lt.getMinute();
        long sec = lt.getSecond();
        long hour = lt.getHour();

        //углы поворота стрелок
        double sec_angle = -Math.PI/2+sec*6.0*Math.PI/180;
        double min_angle = -Math.PI/2+6.0*Math.PI/180*(minute+sec/60.0);
        double hour_angle = -Math.PI/2+30.0*Math.PI/180*(hour+minute/60.0);

        //задание параметров графической области
        Graphics2D g = (Graphics2D) graphics;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        size = getSize(size);
        //insets = getInsets(insets);
        int radius = Math.min(size.width,size.height) / 2 - 5;
        g.translate((double)size.width/2D,(double)size.height/2D);

        //задание шрифта и его метрик для рисования цифр
        Font fDigits = new Font(fontOfDigits, Font.PLAIN, sizeOfDigits);
        g.setFont(fDigits);
        FontMetrics fmDigits = g.getFontMetrics(fDigits);
        //задание типа цифр
        if(typeOfDigits.equals("Arabic")) digits = arabicDigits;
        else if(typeOfDigits.equals("Roman"))digits = romanDigits;
        //задание параметров, используемых для задания координат цифр
        int radiusToDigit = radius-offset;
        double sin30 = Math.sin(Math.toRadians(30.0));
        double cos30 = Math.cos(Math.toRadians(30.0));
        //центр цифры по оси х
        float[] xCenterOfDigit = new float[12];
        for(int i=0; i<12; i++) xCenterOfDigit[i] = fmDigits.stringWidth(digits[i])/2;
        //центр цифры по оси у
        float yCenterOfDigit = fmDigits.getAscent() - fmDigits.getHeight()/2;
        //координаты х и у цифр
        float[] xDigit = new float[12];
        float[] yDigit = new float[12];
        //задание координат цифр
        xDigit[0] = ((float)sin30 * radiusToDigit) - xCenterOfDigit[0];
        yDigit[0] = -((float)cos30 * radiusToDigit) + yCenterOfDigit;
        xDigit[1] = ((float)cos30 * radiusToDigit) - xCenterOfDigit[1];
        yDigit[1] = -((float)sin30 * radiusToDigit) + yCenterOfDigit;
        xDigit[2] = radiusToDigit - xCenterOfDigit[2];
        yDigit[2] = yCenterOfDigit;
        xDigit[3] = ((float)cos30 * radiusToDigit) - xCenterOfDigit[3];
        yDigit[3] = ((float)sin30 * radiusToDigit) + yCenterOfDigit;
        xDigit[4] = ((float)sin30 * radiusToDigit) - xCenterOfDigit[4];
        yDigit[4] = ((float)cos30 * radiusToDigit) + yCenterOfDigit;
        xDigit[5] = -xCenterOfDigit[5];
        yDigit[5] = radiusToDigit + yCenterOfDigit;
        xDigit[6] = -((float)sin30 * radiusToDigit) - xCenterOfDigit[6];
        yDigit[6] = ((float)cos30 * radiusToDigit) + yCenterOfDigit;
        xDigit[7] = -((float)cos30 * radiusToDigit) - xCenterOfDigit[7];
        yDigit[7] = ((float)sin30 * radiusToDigit) + yCenterOfDigit;
        xDigit[8] = -radiusToDigit - xCenterOfDigit[8];
        yDigit[8] = yCenterOfDigit;
        xDigit[9] = -((float)cos30 * radiusToDigit) - xCenterOfDigit[9];
        yDigit[9] = -((float)sin30 * radiusToDigit) + yCenterOfDigit;
        xDigit[10] = -((float)sin30 * radiusToDigit) - xCenterOfDigit[10];
        yDigit[10] = -((float)cos30 * radiusToDigit) + yCenterOfDigit;
        xDigit[11] = -xCenterOfDigit[11];
        yDigit[11] = -radiusToDigit + yCenterOfDigit;
        //рисование цифр
        g.setColor(colorOfDigits);
        for(int i=0; i<12; i++) g.drawString(digits[i], xDigit[i], yDigit[i]);

        //рисование циферблата
        g.setColor(Color.black);
        g.setStroke(ROUND);
        g.drawOval(-radius + 2, -radius + 2, 2 * radius - 4, 2 * radius - 4);
        g.setStroke(HOUR_STROKE);
        for (int i = 0; i < 60; ++i) {
            if ((i % 5) == 0) {
                g.setColor(colorOfHands);
                g.fillOval(radius -15, -7, 14, 14);
                g.setColor(Color.black);
                g.drawOval(radius -15, -7, 14, 14);
            }
            else {
                g.setColor(colorOfLines);
                g.drawLine(radius-10, 0, radius-3, 0);
            }
            g.rotate(6.0*Math.PI/180);
        }

        //задание шрифта и его метрик для рисования логотипа
        Font fLogo = new Font("Times New Roman", Font.ITALIC, sizeOfDigits-38);
        g.setFont(fLogo);
        FontMetrics fmLogo = g.getFontMetrics(fLogo);
        //рисование логотипа
        if (drawLogo == true){
            g.setColor(colorOfLogo);
            g.drawString(logo, -fmLogo.stringWidth(logo)/2, -(float)(radius*0.4));
        }

        //задание формата даты
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM");
        Date date = new Date();
        String calendar = dateFormat.format(date);
        //задание шрифта и его метрик для рисования календаря
        Font fCalendar = new Font("Arial", Font.BOLD, sizeOfDigits-30);
        g.setFont(fCalendar);
        FontMetrics fmCalendar = g.getFontMetrics(fCalendar);
        //задание параметров, используемых для задания координат календаря
        int radiusToCalendar = (int)(radius*0.3);
        int xCenterOfCalendar = fmCalendar.stringWidth(calendar)/2;
        int yCenterOfCalendar = fmCalendar.getAscent();
        //рисование календаря
        if (drawCalendar == true){
            g.setStroke(SEC_STROKE);
            g.setColor(colorOfCalendar);
            g.fillRoundRect(-xCenterOfCalendar-10, radiusToCalendar,fmCalendar.stringWidth(calendar)+20,
                    fmCalendar.getHeight(),15,15);
            g.setColor(colorOfLines);
            g.drawRoundRect(-xCenterOfCalendar-10, radiusToCalendar,fmCalendar.stringWidth(calendar)+20,
                    fmCalendar.getHeight(),15,15);
            g.setColor(colorOfCalendarDigits);
            g.drawString(calendar, -fmCalendar.stringWidth(calendar)/2, radiusToCalendar + yCenterOfCalendar);
        }

        //рисование часовой стрелки
        g.rotate(hour_angle);
        g.setStroke(HOUR_STROKE);
        g.setColor(colorOfHands);
        g.fillRoundRect(0,-5, radius-90, 10,10,10);
        g.setColor(Color.black);
        g.drawRoundRect(0,-5, radius-90, 10,10,10);
        g.rotate(-hour_angle);

        //рисование минутной стрелки
        g.rotate(min_angle);
        g.setStroke(HOUR_STROKE);
        g.setColor(colorOfHands);
        g.fillRoundRect(0,-5, radius-40, 10,10,10);
        g.setColor(Color.black);
        g.drawRoundRect(0,-5, radius-40, 10,10,10);
        g.rotate(-min_angle);

        //рисование центрального круга у минутной стрелки
        g.setStroke(HOUR_STROKE);
        g.setColor(Color.black);
        g.fillOval(-11, -11, 22, 22);

        //рисование секундной стрелки
        g.rotate(sec_angle);
        g.setStroke(SEC_STROKE);
        g.setColor(colorOfHands);
        g.drawLine(-30, 0, radius - 20, 0);
        g.rotate(-sec_angle);

        //рисование центрального круга у секундной стрелки
        g.fillOval(-7, -7, 14, 14);
    }
}


