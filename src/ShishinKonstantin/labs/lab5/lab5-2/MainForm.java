import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by shishin on 23.05.2017.
 */
public class MainForm extends JFrame{
    private JPanel panel;
    private JButton defaultSettingsButton;
    private JComboBox colorOfHandsComboBox;
    private JComboBox colorOfDigitsComboBox;
    private JCheckBox nightModeCheckBox;
    private JComboBox fontOfDigitsComboBox;
    private JComboBox typeOfDigitsComboBox;
    private JCheckBox calendarCheckBox;
    private JComboBox sizeOfDigitsComboBox;
    private JCheckBox logoCheckBox;
    private JComboBox colorOfLogoComboBox;
    private JComboBox colorOfCalendarComboBox;
    private JComboBox colorOfCalendarDigitsComboBox;

    String[] typesOfDigits = new String[]{"Arabic","Roman"};
    String[] fontsOfDigits = new String[]{"Times New Roman","Arial","Tahoma","Impact"};
    int[] sizesOfDigits = new int[]{60,70,80,90,100};
    int[] offsets = new int[]{50,60,70,75,80};
    String[] colorsNames = new String[]{"Black","Red","Green","Orange","Cyan","Magenta","White"};
    Color[] colors = new Color[]{Color.black, Color.red, Color.green, Color.orange, Color.cyan, Color.magenta, Color.white};

    //создание экземпляра класса AnalogClock
    AnalogClock clock = new AnalogClock();

    //конструктор формы
    public MainForm(String title) {
        super(title);
        setSize(600,580);
        panel.setBackground(Color.white);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(panel);
        add(clock);
        setVisible(true);

        //заполнение ComboBox'ов
        for (int i=0; i<typesOfDigits.length; i++) typeOfDigitsComboBox.addItem(typesOfDigits[i]);
        for (int i=0; i<fontsOfDigits.length; i++) fontOfDigitsComboBox.addItem(fontsOfDigits[i]);
        for (int i=0; i<sizesOfDigits.length; i++) sizeOfDigitsComboBox.addItem(sizesOfDigits[i]);
        for (int i=0; i<colors.length; i++){
            //пропускаем цвет Black для colorOfHandsComboBox
            if(i == 0) {
                colorOfDigitsComboBox.addItem(colors[i]);
                colorOfLogoComboBox.addItem(colors[i]);
                colorOfCalendarDigitsComboBox.addItem(colors[i]);
                colorOfCalendarComboBox.addItem(colors[i]);
            }
            else{
                colorOfDigitsComboBox.addItem(colors[i]);
                colorOfHandsComboBox.addItem(colors[i]);
                colorOfLogoComboBox.addItem(colors[i]);
                colorOfCalendarDigitsComboBox.addItem(colors[i]);
                colorOfCalendarComboBox.addItem(colors[i]);
            }
        }

        //ComboBox выбора типа цифр
        typeOfDigitsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i=0; i<typesOfDigits.length; i++){
                    if (i == typeOfDigitsComboBox.getSelectedIndex()){
                        clock.setTypeOfDigits(typesOfDigits[i]);
                        break;
                    }
                }
            }
        });

        //ComboBox выбора шрифта цифр
        fontOfDigitsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i=0; i<fontsOfDigits.length; i++){
                    if (i == fontOfDigitsComboBox.getSelectedIndex()){
                        clock.setFontOfDigits(fontsOfDigits[i]);
                        break;
                    }
                }
            }
        });

        //ComboBox выбора размера цифр
        sizeOfDigitsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i=0; i<sizesOfDigits.length; i++){
                    if (i == sizeOfDigitsComboBox.getSelectedIndex()){
                        clock.setSizeOfDigits(sizesOfDigits[i], offsets[i]);
                        break;
                    }
                }
            }
        });

        //задание цвета фона и цвета шрифта выбранного элемента
        setColors(colorOfDigitsComboBox, colors[0], Color.white);
        //задание renderer'а
        colorOfDigitsComboBox.setRenderer(new ComboBoxRenderer());
        //ComboBox выбора цвета цифр
        colorOfDigitsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i=0; i<colors.length; i++){
                    if (i == colorOfDigitsComboBox.getSelectedIndex()) {
                        if (i == 0) {
                            clock.setColorOfDigits(colors[i]);
                            setColors(colorOfDigitsComboBox, colors[i], Color.white);
                            break;
                        } else {
                            clock.setColorOfDigits(colors[i]);
                            setColors(colorOfDigitsComboBox, colors[i], Color.black);
                            break;
                        }
                    }
                }
            }
        });

        //задание цвета фона и цвета шрифта выбранного элемента
        setColors(colorOfHandsComboBox, colors[1], colors[0]);
        //задание renderer'а
        colorOfHandsComboBox.setRenderer(new ComboBoxRenderer());
        //ComboBox выбора цвета стрелок
        colorOfHandsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i=0; i<colors.length-1; i++){
                    if (i == colorOfHandsComboBox.getSelectedIndex()){
                        clock.setColorOfHands(colors[i+1]);
                        if (nightModeCheckBox.isSelected()) clock.setColorOfLines(colors[i+1]);
                        setColors(colorOfHandsComboBox, colors[i+1], Color.black);
                        break;
                    }
                }
            }
        });

        //задание цвета фона и цвета шрифта выбранного элемента
        setColors(colorOfLogoComboBox, colors[0], Color.white);
        //задание renderer'а
        colorOfLogoComboBox.setRenderer(new ComboBoxRenderer());
        //ComboBox выбора цвета логотипа
        colorOfLogoComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < colors.length; i++) {
                    if (i == colorOfLogoComboBox.getSelectedIndex()) {
                        if (i == 0) {
                            clock.setColorOfLogo(colors[i]);
                            colorOfLogoComboBox.setSelectedItem(colors[i]);
                            setColors(colorOfLogoComboBox, colors[i], Color.white);
                            break;
                        } else {
                            clock.setColorOfLogo(colors[i]);
                            colorOfLogoComboBox.setSelectedItem(colors[i]);
                            setColors(colorOfLogoComboBox, colors[i], Color.black);
                            break;
                        }
                    }
                }
            }
        });

        //задание выбранного элемента
        colorOfCalendarComboBox.setSelectedItem(colors[6]);
        //задание цвета фона и цвета шрифта выбранного элемента
        setColors(colorOfCalendarComboBox, colors[6], Color.black);
        //задание renderer'а
        colorOfCalendarComboBox.setRenderer(new ComboBoxRenderer());
        //ComboBox выбора цвета календаря
        colorOfCalendarComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedColorOfCalendar = colorOfCalendarComboBox.getSelectedIndex();
                for (int i = 0; i < colors.length; i++) {
                    if (i == selectedColorOfCalendar) {
                        if (i == 0) {
                            clock.setColorOfCalendar(colors[i]);
                            colorOfCalendarComboBox.setSelectedItem(colors[i]);
                            setColors(colorOfCalendarComboBox, colors[i], Color.white);
                            break;
                        } else {
                            clock.setColorOfCalendar(colors[i]);
                            colorOfCalendarComboBox.setSelectedItem(colors[i]);
                            setColors(colorOfCalendarComboBox, colors[i], Color.black);
                            break;
                        }
                    }
                }
            }
        });

        //задание цвета фона и цвета шрифта выбранного элемента
        setColors(colorOfCalendarDigitsComboBox, colors[0], Color.white);
        //задание renderer'а
        colorOfCalendarDigitsComboBox.setRenderer(new ComboBoxRenderer());
        //ComboBox выбора цвета цифр календаря
        colorOfCalendarDigitsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < colors.length; i++) {
                    if (i == colorOfCalendarDigitsComboBox.getSelectedIndex()) {
                        if (i == 0) {
                            clock.setColorOfCalendarDigits(colors[i]);
                            colorOfCalendarDigitsComboBox.setSelectedItem(colors[i]);
                            setColors(colorOfCalendarDigitsComboBox, colors[i], Color.white);
                            break;
                        } else {
                            clock.setColorOfCalendarDigits(colors[i]);
                            colorOfCalendarDigitsComboBox.setSelectedItem(colors[i]);
                            setColors(colorOfCalendarDigitsComboBox, colors[i], Color.black);
                            break;
                        }
                    }
                }
            }
        });

        //CheckBox включения/выключения логотипа
        logoCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(logoCheckBox.isSelected()) {
                    clock.setDrawLogo(true);
                }
                else {
                    clock.setDrawLogo(false);
                }
            }
        });

        //CheckBox включения/выключения календаря
        calendarCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(calendarCheckBox.isSelected()) {
                    clock.setDrawCalendar(true);
                }
                else {
                    clock.setDrawCalendar(false);
                }
            }
        });

        //CheckBox включения/выключения ночного режима
        nightModeCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(nightModeCheckBox.isSelected()) {
                    panel.setBackground(Color.black);
                    clock.setColorOfHands(clock.getColorOfHands());
                    clock.setColorOfLines(clock.getColorOfHands());
                }
                else {
                    panel.setBackground(Color.white);
                    clock.setColorOfLines(Color.black);
                }
            }
        });

        //кнопка сброса настроек
        defaultSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clock.setTypeOfDigits(typesOfDigits[0]);
                clock.setFontOfDigits(fontsOfDigits[0]);
                clock.setSizeOfDigits(sizesOfDigits[0], offsets[0]);
                clock.setColorOfDigits(colors[0]);
                clock.setColorOfHands(colors[1]);
                typeOfDigitsComboBox.setSelectedItem(typesOfDigits[0]);
                fontOfDigitsComboBox.setSelectedItem(fontsOfDigits[0]);
                sizeOfDigitsComboBox.setSelectedItem(sizesOfDigits[0]);
                colorOfDigitsComboBox.setSelectedItem(colors[0]);
                colorOfHandsComboBox.setSelectedItem(colors[1]);
                setColors(colorOfLogoComboBox, colors[0], Color.white);
                colorOfCalendarComboBox.setSelectedItem(colors[6]);
                colorOfCalendarDigitsComboBox.setSelectedItem(colors[0]);
                if (!(logoCheckBox.isSelected())) {
                    logoCheckBox.setSelected(true);
                    clock.setDrawLogo(true);
                }
                if (!(calendarCheckBox.isSelected())) {
                    calendarCheckBox.setSelected(true);
                    clock.setDrawCalendar(true);
                }
                if (nightModeCheckBox.isSelected()) {
                    nightModeCheckBox.setSelected(false);
                    clock.setColorOfLines(Color.black);
                    panel.setBackground(Color.white);
                }
            }
        });
    }

    //задание цвета фона и цвета шрифта выбранного элемента ComboBox'a
    public void setColors(JComboBox comboBox, Color background, Color foreground){
        comboBox.setBackground(background);
        comboBox.setForeground(foreground);
        comboBox.getComponents()[0].setBackground(new JComboBox().getBackground());
    }

    //реализация renderer'а для изменения цвета элементов ComboBox
    class ComboBoxRenderer extends JLabel implements ListCellRenderer {
        public ComboBoxRenderer()
        {
            setOpaque(true);    //прозрачный фон
        }
        public Component getListCellRendererComponent(JList list, Object value, int index,
                                                      boolean isSelected, boolean hasFocus) {
            Color color = (Color) value;
            //цвет фона
            setBackground(color);
            //цвет шрифта
            if (value == Color.black) setForeground(Color.white);
            else setForeground(Color.black);
            //установка названия цвета
            for (int i=0; i<colors.length; i++){
                if (value == colors[i]) setText(colorsNames[i]);
            }
            return this;
        }
    }

    //запуск приложения
    public static void main(String[] args) {
        MainForm form = new MainForm("AnalogClock");     //создание экземляра класса MainForm
    }
}
