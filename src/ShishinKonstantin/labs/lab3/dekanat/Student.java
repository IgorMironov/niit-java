package kostya;

//класс Студент
public class Student
{
    private int id;							//идентификационный номер
	private String fio;						//фамилия и инициалы
	private Group group;					//ссылка на группу (объект Group)
	private int[] marks = new int[10];		//массив оценок
	private int numMarks=0;					//количество оценок

	//создание студента с указанием ИД и ФИО
	public Student(int id, String fio){
		this.id = id;
		this.fio = fio;
	}
	//получение ID студента
	public int getId(){
		return id;
	}

	//получение ФИО студента
	public String getFio(){
		return fio;
	}

	//получение ссылки на группу
	public Group getGroup(){
		return group;
	}

	//зачисление в группу
	public void setGroup(Group group) {
		this.group = group;
	}

	//добавление случайной оценки
	public void addMark() throws ArrayIndexOutOfBoundsException {
		//( Math.random() * (max+1 - min) ) + min
		marks[numMarks]=(int) (Math.random() * (6-2) + 2);	//генерирование случайной оценки от 2 до 5
		numMarks++;
	}

	//добавление заданной оценки (для теста)
	public void setMark(int mark) throws ArrayIndexOutOfBoundsException {
		marks[numMarks]=mark;
		numMarks++;
	}

	//вычисление средней оценки студента
	public double calcAverageMarkStudent() {
		double averageMarkStudent=0.0;
		double sumMarksStudent=0.0;
		try{
			for(int i=0; i<numMarks; i++){
				sumMarksStudent+=marks[i];
			}
			averageMarkStudent = sumMarksStudent/numMarks;
		}
		catch(ArrayIndexOutOfBoundsException aioex){System.out.println("У каждого студента должно быть не больше 10 оценок!");}
		catch(NumberFormatException nfex){System.out.println("У каждого студента должно быть не меньше 1 оценки!");}
		return averageMarkStudent;
	}
}
